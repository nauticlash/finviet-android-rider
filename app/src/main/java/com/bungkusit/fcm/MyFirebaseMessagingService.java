package com.bungkusit.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.view.View;

import com.bungkusit.R;
import com.bungkusit.activity.SplashActivity;
import com.bungkusit.activity.chatconversation.ChatConversationActivity;
import com.bungkusit.activity.main.MainActivity;
import com.bungkusit.activity.orderDetail.OrderDetailActivity;
import com.bungkusit.model.CommonResp;
import com.bungkusit.model.UserInfo;
import com.bungkusit.service.LocationService;
import com.bungkusit.utils.AppConstant;
import com.bungkusit.utils.AppGlobal;
import com.bungkusit.utils.AppPreferences;
import com.bungkusit.utils.Debug;
import com.bungkusit.utils.MyApplication;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import retrofit2.Call;

import static com.bungkusit.utils.AppConstant.REQUEST_CODE_4;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "Tag";

    private String messagetitle = "";

    private AppPreferences appPreferences;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        appPreferences = MyApplication.getInstance().getAppPreferences();

        Debug.e(TAG, "Data getFrom:" + remoteMessage.getData().toString());

        appPreferences.setNotificationBedgeInc();

        if (appPreferences.isLogin()) {

            if (remoteMessage.getData().size() > 0) {

                String msg = remoteMessage.getData().toString();

                String data = remoteMessage.getData().get("data");
                String mtype = remoteMessage.getData().get("mtype");
                String message = remoteMessage.getData().get("message");


                JSONObject jsonResponse = new JSONObject();

                try {
                    jsonResponse.put("data", new JSONObject(data));
                    jsonResponse.put("mtype", mtype);
                    jsonResponse.put("message", message);

                } catch (Exception e) {

                    // catch in case data or mtype anyone is not present in payload data
                    // admin msg to all rider and msg to rider doesnot have data and mtype
                    e.printStackTrace();
                    Debug.e("error", ":::" + e.getMessage());
                    jsonResponse = null;
                    if (!message.equalsIgnoreCase("")) {

                        Debug.e("received", ":::" + message);
                        sendNotification(message);
                    }

                }

                if (jsonResponse != null) {

                    try {

                        if (jsonResponse.has("mtype")) {

                            String mType = jsonResponse.getString("mtype");

                            // for chat messages
                            if (mType.equals(getString(R.string.type_message))) {

                                if (jsonResponse.has("data")) {
                                    try {
                                        UserInfo userInfo = appPreferences.getUserInfo();
                                        int iCount = Integer.parseInt(userInfo.totalunread_msgs);
                                        iCount++;
                                        userInfo.totalunread_msgs = String.valueOf(iCount);
                                        appPreferences.setUserInfo(userInfo);

                                        JSONObject notificationdata = jsonResponse.getJSONObject("data");

                                        Bundle bundle = new Bundle();
                                        bundle.putString("toId", notificationdata.getString("toId"));
                                        bundle.putString("fromId", notificationdata.getString("fromId"));
                                        bundle.putString("firstname", notificationdata.getString("firstname"));
                                        bundle.putString("order_id", notificationdata.getString("orderid"));

                                        Intent intent = new Intent(this, ChatConversationActivity.class);
                                        intent.putExtras(bundle);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                                        sendBroadcast(jsonResponse.toString());

                                        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                                                PendingIntent.FLAG_UPDATE_CURRENT);

                                        String titlemsg = notificationdata.getString("firstname") + ": " + notificationdata.getString("message");
                                        sendNotification(titlemsg, pendingIntent, true);

                                    } catch (Exception e) {
                                        Debug.e(TAG, "Exception: " + e.getMessage());
                                    }
                                    MyApplication.getInstance().getBus().post(jsonResponse.getString("data"));
                                }
                            } else if (mType.equals(getString(R.string.type_neworder)) || mType.equalsIgnoreCase(getString(R.string.type_assign_by_user)) || mType.equalsIgnoreCase(getString(R.string.type_cancelorder)) || mType.equals("complete_by_rider")) {

                                if (mType.equalsIgnoreCase(getString(R.string.type_assign_by_user))) {
                                    appPreferences.setAssigned(true);
                                }

                                if (jsonResponse.has("data")) {

                                    try {

                                        messagetitle = (String) jsonResponse.getJSONObject("data").get("message");
//                                        appPreferences.setNotificationBedgeInc();

                                        String orderid = (jsonResponse.getJSONObject("data").getString("order_id"));

                                        Bundle bundle = new Bundle();
                                        bundle.putString(OrderDetailActivity.EXTRA_ORDER_ID, orderid);

                                        Intent intent = new Intent(this, OrderDetailActivity.class);
                                        intent.putExtras(bundle);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                                                PendingIntent.FLAG_CANCEL_CURRENT);

                                        sendBroadcast(jsonResponse.toString());

                                        sendNotification(message, pendingIntent);


                                      /*  Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                                                .setSmallIcon(R.mipmap.ic_launcher)
                                                .setContentTitle("Bungkusit Rider")
                                                .setStyle(new NotificationCompat.BigTextStyle()
                                                        .bigText(Html.fromHtml(message)))
                                                .setContentText(Html.fromHtml(message))
                                                .setAutoCancel(true)
                                                .setSound(defaultSoundUri)
                                                .setVibrate(new long[]{1000, 1000})
                                                .setLights(Color.RED, 3000, 3000)
                                                .setContentIntent(pendingIntent);

                                        NotificationManager notificationManager =
                                                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                                        notificationManager.notify(Config.NEWORDER_ID *//* ID of notification *//*, notificationBuilder.build());*/

                                    } catch (Exception e) {
                                        Debug.e(TAG, "Exception: " + e.getMessage());
                                    }
                                }

                            } else if (mType.equals(getString(R.string.type_subscription))) {

                                if (jsonResponse.has("data")) {
                                    try {

                                        messagetitle = (String) jsonResponse.getJSONObject("data").get("message");
                                        sendBroadcast(jsonResponse.toString());
                                        handleNotification(jsonResponse.toString());

                                    } catch (Exception e) {
                                        Debug.e(TAG, "Exception: " + e.getMessage());
                                    }
                                }
                            } else if (mType.equals(getString(R.string.type_cancelorder))) {

                                if (jsonResponse.has("data")) {
                                    try {
                                        messagetitle = (String) jsonResponse.getJSONObject("data").get("message");
                                        sendBroadcast(jsonResponse.toString());
                                        handleNotification(jsonResponse.toString());

                                    } catch (Exception e) {
                                        Debug.e(TAG, "Exception: " + e.getMessage());
                                    }
                                }
                            } else if (mType.equalsIgnoreCase("admin_disable")) {
                                handleNotification(jsonResponse.toString());
                            } else {

                                if (jsonResponse.has("data")) {

                                    try {
                                        messagetitle = (String) jsonResponse.getJSONObject("data").get("message");
//                                        appPreferences.setNotificationBedgeInc();
                                        sendBroadcast(jsonResponse.toString());
                                        handleNotification(jsonResponse.toString());

                                    } catch (Exception e) {
                                        Debug.e(TAG, "Exception: " + e.getMessage());
                                        handleNotification(msg);
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }


    }

    private void handleNotification(String message) {
        sendNotification(message);
    }

    private void sendBroadcast(String message) {
        Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
        pushNotification.putExtra("message", message);
        LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
    }


    private void sendNotification(String message, PendingIntent pendingIntent, boolean... incremental) {

        Bitmap image = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.icon_mini_notification)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(message))
                .setContentTitle(getString(R.string.notification_title))
                .setContentText(Html.fromHtml(message))
                .setAutoCancel(true)
                .setLargeIcon(image)
                .setSound(defaultSoundUri)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setLights(Color.RED, 3000, 3000)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channel_id = "NOTIFICATION_CHANNEL";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(channel_id, "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationBuilder.setChannelId(channel_id);
            notificationManager.createNotificationChannel(notificationChannel);
        }


        Notification notification = notificationBuilder.build();
        int smallIconId = getResources().getIdentifier("right_icon", "id", android.R.class.getPackage().getName());
        if (smallIconId != 0) {
            if (notification.contentView != null) {
                notification.contentView.setViewVisibility(smallIconId, View.INVISIBLE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    notification.bigContentView.setViewVisibility(smallIconId, View.INVISIBLE);
                }
            }

        }


        if (incremental != null && incremental.length > 0) {
            notificationManager.notify(Config.CHATINCREMENT_ID++ /* ID of notification */, notification);
        } else {
            notificationManager.notify(Config.NOTIFICATION_ID /* ID of notification */, notification);
        }

    }

    private void sendNotification(String message) {

        /*
         message contains json object when admin msg to all rider
         else message will contain text only without obj when msg to rider only from backend */

        JSONObject jsonObject;
        String orderid = "";
        String mOrderType = "";
        String mType = "";
        String mMessage = "";

        try {

            jsonObject = new JSONObject(message);

            if (jsonObject.has("data")) {



                if (jsonObject.has("mtype")) {

                    mType = jsonObject.getString("mtype");
                    mMessage = jsonObject.getString("message");

                    if (mType.equalsIgnoreCase("admin")) {

                        //admin msg comes here
                        Bundle newBundle = new Bundle();
                        newBundle.putString("adminmsg", mMessage);
                        Intent intent = new Intent(this, MainActivity.class);
                        intent.putExtras(newBundle);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


                        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                                PendingIntent.FLAG_UPDATE_CURRENT);
                        sendNotification(mMessage, pendingIntent, true);

                        sendBroadcast(mMessage);

                        Debug.e("received", "admin:::all rider" + message);
                    } else if (mType.equalsIgnoreCase("admin_disable")) {
                        Bundle newBundle = new Bundle();
                        newBundle.putString("admin_disable", mMessage);
                        Intent intent = new Intent(this, MainActivity.class);
                        intent.putExtras(newBundle);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                                PendingIntent.FLAG_UPDATE_CURRENT);
                        sendNotification(mMessage, pendingIntent, true);
//                        sendBroadcast(mMessage);
                        MainActivity.main.kickoutAPI();
                    }

                }


            }

        } catch (JSONException e) {
            e.printStackTrace();

            if (!message.equalsIgnoreCase("")) {

                // to display normal message without object
                Bundle newBundle = new Bundle();
                newBundle.putString("adminmsg", message);
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtras(newBundle);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
                sendNotification(message, pendingIntent, true);
                sendBroadcast(message);
                Debug.e("received", "admin rideronly:::" + message);

            }
        }


    }

}
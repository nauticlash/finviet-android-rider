package com.bungkusit.service;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.view.View;

import com.bungkusit.R;
import com.bungkusit.activity.SplashActivity;
import com.bungkusit.activity.main.MainActivity;
import com.bungkusit.fcm.Config;
import com.bungkusit.utils.AppConstant;
import com.bungkusit.utils.AppPreferences;
import com.bungkusit.utils.Debug;
import com.bungkusit.utils.MyApplication;

import io.nlopez.smartlocation.SmartLocation;

import static com.bungkusit.utils.AppConstant.isGpsEnabled;
import static com.bungkusit.utils.AppConstant.isNetworkGPSEnabled;
import static com.bungkusit.utils.AppGlobal.isGPSAvailable;
import static com.bungkusit.utils.AppGlobal.locationServicesEnabled;


/**
 * Created by cears on 13/12/17.
 */

public class GpsLocationReceiver extends BroadcastReceiver {


    private LocationService mLocationService;
    private AppPreferences appPreferences;
    private Intent iIntent;

    @Override
    public void onReceive(Context context, Intent intent) {

        mLocationService = new LocationService(MyApplication.getInstance());
        appPreferences = MyApplication.getInstance().getAppPreferences();

        Debug.e("PROVIDERS_CHANGED", "PROVIDERS_CHANGED");

        if (appPreferences.getUserInfo() != null && appPreferences.isLogin()) {
            iIntent = new Intent(MyApplication.getInstance(), MainActivity.class);
        } else {
            iIntent = new Intent(MyApplication.getInstance(), SplashActivity.class);
        }

        if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {

            isGPSAvailable = locationServicesEnabled(MyApplication.getInstance());

            if (!SmartLocation.with(MyApplication.getInstance()).location().state().locationServicesEnabled() || !locationServicesEnabled(context)) {

                Bitmap image = BitmapFactory.decodeResource(MyApplication.getInstance().getResources(), R.mipmap.ic_launcher);

                //   Intent iIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);

                PendingIntent pendingIntent = PendingIntent.getActivity(MyApplication.getInstance(), 0 /* Request code */, iIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(MyApplication.getInstance())
                        .setSmallIcon(R.drawable.icon_mini_notification)
                        .setLargeIcon(image)
                        .setContentTitle(MyApplication.getInstance().getString(R.string.notification_title))
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText("Please enable location."))
                        .setContentText("Please enable location.")
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setVibrate(new long[]{1000, 1000})
                        .setLights(Color.RED, 3000, 3000)
                        .setContentIntent(pendingIntent);

                NotificationManager notificationManager =
                        (NotificationManager) MyApplication.getInstance().getSystemService(Context.NOTIFICATION_SERVICE);

                Notification notification = notificationBuilder.build();
                int smallIconId = MyApplication.getInstance().getResources().getIdentifier("right_icon", "id", android.R.class.getPackage().getName());
                if (smallIconId != 0) {
                    if (notification.contentView != null) {
                        notification.contentView.setViewVisibility(smallIconId, View.INVISIBLE);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            notification.bigContentView.setViewVisibility(smallIconId, View.INVISIBLE);
                        }
                    }

                }

                notificationManager.notify(1/* ID of notification */, notification);

            } else if (!isMyServiceRunning(mLocationService.getClass())) {
                Intent pushIntent = new Intent(context, LocationService.class);
                pushIntent.setAction(AppConstant.ACTION.STARTFOREGROUND_ACTION);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(pushIntent);
                } else {
                    context.startService(pushIntent);
                }
            }


            Debug.e("location", "isGPS = >" + isGpsEnabled);
        }
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {

        ActivityManager manager = (ActivityManager) MyApplication.getInstance().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
package com.bungkusit.service;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.bungkusit.R;
import com.bungkusit.activity.SplashActivity;
import com.bungkusit.activity.main.MainActivity;
import com.bungkusit.utils.AppConstant;
import com.bungkusit.utils.AppGlobal;
import com.bungkusit.utils.AppPreferences;
import com.bungkusit.utils.Debug;
import com.bungkusit.utils.MyApplication;
import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesProvider;
import io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesWithFallbackProvider;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by cears on 13/12/17.
 */

public class LocationService extends Service {

    private LocationGooglePlayServicesProvider provider;

    private AppPreferences appPreferences;

    private CountDownTimer countDownTimer;

    private static final String LOG_TAG = "ForegroundService";
    public static boolean IS_SERVICE_RUNNING = false;

    public LocationService(Context applicationContext) {
        super();
    }

    public LocationService() {
    }

    public Timer mTimer;
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        if (intent != null && intent.getAction().equals(AppConstant.ACTION.STARTFOREGROUND_ACTION)) {

//            showNotification();
//            Toast.makeText(this, "Service Started!", Toast.LENGTH_SHORT).show();
            startTimer();
        } else if (intent != null && intent.getAction().equals(AppConstant.ACTION.STOPFOREGROUND_ACTION)) {

            if (countDownTimer != null)
                countDownTimer.cancel();
            stopForeground(true);
            stopSelf();
        } else {
            startTimer();
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Intent broadcastIntent = new Intent("com.bungkusit.rider.RestartLocation");
        sendBroadcast(broadcastIntent);
        if (countDownTimer != null)
            countDownTimer.cancel();
    }

    long oldTime = 0;

    public void startTimer() {
        this.mTimer = new Timer();
        //Set the schedule function
        this.mTimer.scheduleAtFixedRate(new TimerTask() {
                                            @Override
                                            public void run() {
                                                // Magic here
                                                sendLocationtoApi();
                                            }
                                        },
                0, 10000);
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void sendLocationtoApi() {

        appPreferences = MyApplication.getInstance().getAppPreferences();
        provider = new LocationGooglePlayServicesProvider();

        if (SmartLocation.with(this).location().state().locationServicesEnabled()) {

            LocationGooglePlayServicesWithFallbackProvider provider = new LocationGooglePlayServicesWithFallbackProvider(this);

            Location location = SmartLocation.with(this).location(provider).getLastLocation();

            if (location != null) {
                double lat = location.getLatitude();
                double lng = location.getLongitude();

                if (lat != 0.0 && lng != 0.0) {
                    callSendLocationAPI(lat + "", lng + "");
                }
            }

            SmartLocation smartLocation = new SmartLocation.Builder(MyApplication.getInstance())
                    .logging(true).build();
            smartLocation.location(provider).continuous().start(new OnLocationUpdatedListener() {
                @Override
                public void onLocationUpdated(Location location) {

                    double lat = location.getLatitude();
                    double lng = location.getLongitude();

                    if (lat != 0.0 && lng != 0.0) {
                        callSendLocationAPI(location.getLatitude() + "", location.getLongitude() + "");
                    }
                }
            });


        }
    }


    private void callSendLocationAPI(String clat, String clng) {

        appPreferences = MyApplication.getInstance().getAppPreferences();

        if (AppGlobal.isNetwork(getApplicationContext())) {

            if (appPreferences.isLogin() && appPreferences.isOnline()) {

                HashMap<String, Object> request = new HashMap<>();
                request.put("userid", appPreferences.getUserInfo().userid);
                request.put("clat", clat);
                request.put("clng", clng);
                appPreferences.setLastLocation(new LatLng(Double.parseDouble(clat), Double.parseDouble(clng)));

                HashMap<String, Object> auth = new HashMap<>();
                auth.put("id", appPreferences.getUserInfo().userid);
                auth.put("token", appPreferences.getToken());

                HashMap<String, Object> map = new HashMap<>();
                map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_update_location));
                map.put(getResources().getString(R.string.request), request);
                map.put("auth", auth);

                Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

                requestService.enqueue(new Callback<String>() {

                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        Debug.v("Tag", "Response=>" + response.body());

                        if (response.code() == 400) {

                        } else if (response.code() == 200) {

                            if (response.isSuccessful()) {
                                if (AppConstant.pos != AppConstant.dLat.length - 1)
                                    AppConstant.pos++;

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable throwable) {
                        if (!call.isCanceled()) {
                            call.cancel();
                        }
                    }
                });


            }

        }
    }


    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent restartServiceTask = new Intent(getApplicationContext(), LocationService.class);
        restartServiceTask.setAction(AppConstant.ACTION.STARTFOREGROUND_ACTION);
        restartServiceTask.setPackage(getPackageName());
        PendingIntent restartPendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceTask, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager myAlarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        myAlarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 1000,
                restartPendingIntent);

        super.onTaskRemoved(rootIntent);
    }


    private void showNotification() {

        appPreferences = MyApplication.getInstance().getAppPreferences();

        Intent notificationIntent;

        if (appPreferences.getUserInfo() != null && appPreferences.isLogin()) {
            notificationIntent = new Intent(this, MainActivity.class);
        } else {
            notificationIntent = new Intent(this, SplashActivity.class);
        }

        notificationIntent.setAction(AppConstant.ACTION.MAIN_ACTION);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);

        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.mipmap.ic_launcher);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setContentTitle("ECO Delivery")
                .setTicker("ECO Delivery cập nhật vị trí")
                .setContentText("Cập nhật vị trí")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentIntent(pendingIntent)
                .setOngoing(true);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channel_id = "NOTIFICATION_CHANNEL";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(channel_id, "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationBuilder.setChannelId(channel_id);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        Notification notification = notificationBuilder.build();

        startForeground(AppConstant.NOTIFICATION_ID.FOREGROUND_SERVICE,
                notification);

    }

}


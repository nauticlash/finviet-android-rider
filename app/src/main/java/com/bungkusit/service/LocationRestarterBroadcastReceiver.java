package com.bungkusit.service;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.bungkusit.utils.AppConstant;
import com.bungkusit.utils.Debug;
import com.bungkusit.utils.MyApplication;

/**
 * Created by cears on 13/12/17.
 */

public class LocationRestarterBroadcastReceiver extends BroadcastReceiver {

    private LocationService mLocationService;

    @Override
    public void onReceive(Context context, Intent intent) {

        mLocationService = new LocationService(MyApplication.getInstance());


        Debug.e(LocationRestarterBroadcastReceiver.class.getSimpleName(), "Service Stops!!!!");

        if (!isMyServiceRunning(mLocationService.getClass())) {
            Intent pushIntent = new Intent(context, LocationService.class);
            pushIntent.setAction(AppConstant.ACTION.STARTFOREGROUND_ACTION);
            context.startService(pushIntent);
        }

    }



    private boolean isMyServiceRunning(Class<?> serviceClass) {

        ActivityManager manager = (ActivityManager) MyApplication.getInstance().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Debug.e("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Debug.e("isMyServiceRunning?", false + "");
        return false;
    }
}
package com.bungkusit.service;

import android.Manifest;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.bungkusit.R;
import com.bungkusit.utils.AppConstant;
import com.bungkusit.utils.AppGlobal;
import com.bungkusit.utils.AppPreferences;
import com.bungkusit.utils.Debug;
import com.bungkusit.utils.MyApplication;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyLocation extends Service implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = MyLocation.class.getSimpleName();
    private Context mContext;

    private Location mLastLocation;

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;

    // boolean flag to toggle periodic location updates
    private boolean mRequestingLocationUpdates = false;

    private LocationRequest mLocationRequest;
    public static int UPDATE_INTERVAL = 10000; // 10 sec
    public static int FATEST_INTERVAL = 5000; // 5 sec
    public static int DISPLACEMENT = 10; // 10 meters
    private AppPreferences appPreferences;

    public static Socket mSocket;
//    private DriverModel mDriver;
    public MyLocation() {
    }

    public Timer mTimer;
    @Override
    public void onCreate() {
        super.onCreate();
//      Building the GoogleApi client
        buildGoogleApiClient();

        createLocationRequest();
        appPreferences = MyApplication.getInstance().getAppPreferences();

        this.mTimer = new Timer();
        //Set the schedule function
        this.mTimer.scheduleAtFixedRate(new TimerTask() {

                                      @Override
                                      public void run() {
                                          // Magic here
                                          if (mLastLocation != null)
                                            callSendLocationAPI(mLastLocation.getLatitude() + "", mLastLocation.getLongitude() + "");
                                      }
                                  },
                0, 10000);   // 1000 Millisecond  = 1 second

//        if (checkIsTracking()) {
//            startTracking();
//        } else {
//            stopTracking();
//        }

        try {
            mSocket = IO.socket(AppConstant.SOCKET_URL + "/?token=" + appPreferences.getToken());
            mSocket.connect();
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "Destroy");
        if (mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
            stopLocationUpdates();
        super.onDestroy();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d(TAG, "Location update service started Start");
        mGoogleApiClient.connect();
        Log.d(TAG, mGoogleApiClient.isConnected() + "");

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {
//        broadcastOnLocationChanged(location);
        Log.d(TAG, location.toString());
        mLastLocation = location;

        try {
            callSendLocationAPI(mLastLocation.getLatitude() + "", mLastLocation.getLongitude() + "");
        } catch (Exception e) {
            Log.d(TAG, "Error send location " + e.getMessage());
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        } else {
            mGoogleApiClient.connect();
        }
    }

    /**
     * Starting the location updates
     * */
    protected void startLocationUpdates() {

        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates( mGoogleApiClient, mLocationRequest, this);
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }

//        broadcastOnLocationChanged(mLastLocation);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed");
    }

    /**
     * Creating google api client object
     * */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();

        Log.d(TAG, "buildGoogleApiClient");
    }

    /**
     * Creating location request object
     * */
    protected void createLocationRequest() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(this.UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(this.FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(this.DISPLACEMENT);
        Log.d(TAG, "createLocationRequest");
    }

    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates() {
        if (mGoogleApiClient.isConnected())
            LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    private void callSendLocationAPI(String clat, String clng) {

        if (appPreferences.isLogin() && appPreferences.isOnline()) {

            if (mSocket.connected()) {
                try {
                    JSONObject request = new JSONObject();
                    request.put("user_id", appPreferences.getUserInfo().userid);
                    request.put("latitude", clat);
                    request.put("longitude", clng);
                    request.put("speed", mLastLocation.getSpeed());
                    request.put("heading", mLastLocation.getBearing());
                    request.put("timestamp", System.currentTimeMillis());
                    mSocket.emit("updateLocation", request);
                } catch (Exception e) {
                    System.out.println(e);
                }
            } else
            if (AppGlobal.isNetwork(getApplicationContext())) {
                mSocket.connect();
                HashMap<String, Object> request = new HashMap<>();
                request.put("userid", appPreferences.getUserInfo().userid);
                request.put("clat", clat);
                request.put("clng", clng);
                appPreferences.setLastLocation(new LatLng(Double.parseDouble(clat), Double.parseDouble(clng)));

                HashMap<String, Object> auth = new HashMap<>();
                auth.put("id", appPreferences.getUserInfo().userid);
                auth.put("token", appPreferences.getToken());

                HashMap<String, Object> map = new HashMap<>();
                map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_update_location));
                map.put(getResources().getString(R.string.request), request);
                map.put("auth", auth);

                Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

                requestService.enqueue(new Callback<String>() {

                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        Log.d(TAG, " Response=>" + response.body());

                        if (response.code() == 400) {

                        } else if (response.code() == 200) {

                            if (response.isSuccessful()) {
                                if (AppConstant.pos != AppConstant.dLat.length - 1)
                                    AppConstant.pos++;

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable throwable) {
                        if (!call.isCanceled()) {
                            call.cancel();
                        }
                    }
                });
            }

        }
    }
}

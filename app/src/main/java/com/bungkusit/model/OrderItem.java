package com.bungkusit.model;

import android.support.annotation.Keep;

import com.google.gson.annotations.SerializedName;

@Keep
public class OrderItem {

    @SerializedName("name")
    public String name;

    @SerializedName("quantity")
    public Integer quantity;

    @SerializedName("amount")
    public Integer amount;

    @SerializedName("position")
    public Integer position;
}

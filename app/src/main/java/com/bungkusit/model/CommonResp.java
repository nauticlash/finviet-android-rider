package com.bungkusit.model;

import com.google.gson.annotations.SerializedName;

public class CommonResp {

    @SerializedName("success")
    public int success;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public String data;

    @SerializedName("orderid")
    public String orderid;

    @SerializedName("riderapp")
    public String riderapp;

}

package com.bungkusit.model;

import android.util.Log;

import com.bungkusit.utils.AppGlobal;
import com.bungkusit.utils.Debug;
import com.google.gson.annotations.SerializedName;

/**
 * Created by cears on 21/11/17.
 */

public class PaymentInfo {

    @SerializedName("sub_id")
    public String sub_id;

    @SerializedName("riderid")
    public String riderid;

    @SerializedName("sub_start_date")
    public String sub_start_date;

    @SerializedName("sub_end_date")
    public String sub_end_date;

    @SerializedName("subamount")
    public String subamount;

    @SerializedName("recimage")
    public String recimage;

    @SerializedName("payvia")
    public String payvia;

    @SerializedName("status")
    public String status;

    @SerializedName("is_deleted")
    public String isDeleted;

    @SerializedName("create_date")
    public String createDate;

    @SerializedName("modify_date")
    public String modifyDate;

    @SerializedName("delete_date")
    public String deleteDate;

    public String getModifyDate() {

        String[] dateFormat = modifyDate.split(" ");
        Debug.e("modifieddate",dateFormat[0]);

        if (dateFormat[0].equalsIgnoreCase("0000-00-00")) {
            return "";
        } else
            return "" + dateFormat[0];
    }

}
package com.bungkusit.model;

import android.support.annotation.Keep;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@Keep
public class ChatConversionResp {

    @SerializedName("success")
    public int success;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public List<ChatConversationInfo> data;


}

package com.bungkusit.model;

/**
 * Created by cears on 25/11/17.
 */

public class Notification {
    public String id;
    public String toId;
    public String fromId;
    public String message;
    public String firstname;
    public String lastname;
    public String fromprofile;
    public String toprofilepic;
    public String attachmenturl;
    public String attachment;
    public String create_date;
}
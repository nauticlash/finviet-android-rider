package com.bungkusit.model;

import android.support.annotation.Keep;
import com.google.gson.annotations.SerializedName;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

@Keep
public class OrderDetailInfo {
    @SerializedName("orderid")
    public String orderid;

    @SerializedName("userid")
    public String userid;

    @SerializedName("rider_id")
    public String riderId;

    @SerializedName("ordertype")
    public String ordertype;

    @SerializedName("user_cash_payment")
    public String userCashPayment;

    @SerializedName("other_amount")
    public String otherAmount;

    @SerializedName("billamount")
    public String billamount;

    @SerializedName("fare_amount")
    public String fare_amount;


    @SerializedName("waiting_charge")
    public String waitingCharge;

    @SerializedName("cash_payment")
    public String cashPayment;

    @SerializedName("change_received")
    public String changeReceived;

    @SerializedName("changestatus")
    public String changestatus;

    @SerializedName("tips")
    public String tips;

    @SerializedName("total_amount")
    public String totalAmount;

    @SerializedName("pickup_address")
    public String pickupAddress;

    @SerializedName("plat")
    public String plat;

    @SerializedName("plng")
    public String plng;

    @SerializedName("destination_address")
    public String destinationAddress;

    @SerializedName("dlat")
    public String dlat;

    @SerializedName("dlng")
    public String dlng;

    @SerializedName("delivery_address")
    public String deliveryAddress;

    @SerializedName("dllat")
    public String dllat;

    @SerializedName("dllng")
    public String dllng;

    @SerializedName("delivery_type")
    public String deliveryType;

    @SerializedName("description")
    public String description;

    @SerializedName("images")
    public ArrayList<String> images;

    @SerializedName("promocode")
    public String promocode;

    @SerializedName("sender_name")
    public String senderName;

    @SerializedName("recipient_name")
    public String recipientName;

    @SerializedName("courier_service_name")
    public String courierServiceName;

    @SerializedName("status")
    public String status;

    @SerializedName("is_rain")
    public String isRain;

    @SerializedName("no_of_km")
    public String noOfKm;

    @SerializedName("orderdate")
    public String orderdate;

    @SerializedName("delivery_time")
    public String delivery_time;

    @SerializedName("modifieddate")
    public String modifieddate;

    @SerializedName("firstname")
    public String firstname;

    @SerializedName("lastname")
    public String lastname;

    @SerializedName("email")
    public String email;

    @SerializedName("contactno")
    public String contactno;

    @SerializedName("profilepic")
    public String profilepic;

    @SerializedName("isbided")
    public String isbided;

    @SerializedName("is_pickup")
    public String isPickup;

    @SerializedName("is_reached_destination")
    public String isReachedDestination;

    @SerializedName("pickup_time")
    public String pickuptime;


    @SerializedName("final_amount")
    public String final_amount;


    @SerializedName("reached_destination_time")
    public String reachedDestinationTime;

    @SerializedName("is_paid")
    public String isPaid;

    @SerializedName("at_destination")
    public String atDestination;

    @SerializedName("is_at_destination")
    public String isAtDestination;

    @SerializedName("is_at_pickup")
    public String isAtPickup;

    @SerializedName("is_dropoff")
    public String isDropoff;


    @SerializedName("is_at_dropoff")
    public String isAtDropoff;

    @SerializedName("receipt")
    public String receipt;

    @SerializedName("riderstatus")
    public String riderstatus;

    @SerializedName("wallet_amount")
    public String wallet_amount;

    @SerializedName("is_assigned")
    public String isAssigned;

    @SerializedName("userwallet")
    public String userwallet;

    @SerializedName("order_total")
    public String order_total;

    @SerializedName("order_items")
    public String order_items;

    @SerializedName("cancel_reason")
    public String cancel_reason;

    @SerializedName("cancel_time")
    public String cancel_time;

    @SerializedName("rider_accept_time")
    public String rider_accept_time;

    @SerializedName("merchant_contact")
    public String merchant_contact;

    @SerializedName("customer_contact")
    public String customer_contact;

    @SerializedName("dropoff_time")
    public String dropoff_time;

    @SerializedName("merchant_order_id")
    public String merchant_order_id;

    @SerializedName("need_image_verify")
    public String need_image_verify;

    @SerializedName("image_verify_type")
    public String image_verify_type;

    @SerializedName("shipping_amount")
    public String shippingAmount;

    @SerializedName("pickup_amount")
    public String pickupAmount;

    @SerializedName("dropoff_amount")
    public String dropoffAmount;

    public String getFinalamount() {
        double finalamount = Double.parseDouble(final_amount);
        return "Rm" + String.format("%.2f", finalamount);
    }

    public String getTotalAmount() {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);;
        formatter.applyPattern("#,###,###");
        double totalAmount = Double.parseDouble(order_total);
        return formatter.format(totalAmount) + " đ";
    }

    public String getFare_amount() {

        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);;
        formatter.applyPattern("#,###,###");
        double fareAmount = Double.parseDouble(fare_amount);
        return formatter.format(fareAmount) + " đ";
    }

    public String getOther_amount() {
        double otheramount = Double.parseDouble(otherAmount);
        return String.format("%.2f", otheramount);
    }

    public String getWallet_amount() {
        double walletamount = Double.parseDouble(wallet_amount);
        return String.format("%.2f", walletamount);
    }


    public String getBillamount() {
        double billAmount = Double.parseDouble(billamount);
        return String.format("%.2f", billAmount);
    }

    public String getChangeReceived() {
        double changereceived = Double.parseDouble(changeReceived);
        return String.format("%.2f", changereceived);
    }

    public String getCancelTime() {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date newDate = null;
            try {
                newDate = format.parse(cancel_time);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            format = new SimpleDateFormat("HH:mm:ss, ngày dd/MM/yyyy");
            String dateF = format.format(newDate);

            return dateF;
        } catch (Exception e) {
            return cancel_time;
        }
    }

    public String getPickupTime() {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date newDate = null;
            try {
                newDate = format.parse(pickuptime);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            format = new SimpleDateFormat("HH:mm:ss, ngày dd/MM/yyyy");
            String dateF = format.format(newDate);

            return dateF;
        } catch (Exception e) {
            return pickuptime;
        }
    }

    public String getDroppOffTime() {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date newDate = null;
            try {
                newDate = format.parse(dropoff_time);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            format = new SimpleDateFormat("HH:mm:ss, ngày dd/MM/yyyy");
            String dateF = format.format(newDate);

            return dateF;
        } catch (Exception e) {
            return dropoff_time;
        }
    }

    public boolean checkChangeReceived() {

        String str = changeReceived;

        if (str.startsWith("-")) { //checks for negative values
            return true;
        }

        return false;
    }

    public String getPickupAmount() {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        formatter.applyPattern("#,###,###");
        double pickup_amount = Double.parseDouble(pickupAmount);
        return formatter.format(pickup_amount) + " đ";
    }

    public String getDropoffAmount() {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        formatter.applyPattern("#,###,###");
        double dropoff_amount = Double.parseDouble(dropoffAmount);
        return formatter.format(dropoff_amount) + " đ";
    }

    public String getShippingAmount() {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        formatter.applyPattern("#,###,###");
        double shipping_amount = Double.parseDouble(shippingAmount);
        return formatter.format(shipping_amount) + " đ";
    }
}

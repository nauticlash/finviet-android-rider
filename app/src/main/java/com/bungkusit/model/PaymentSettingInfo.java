package com.bungkusit.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cears on 24/11/17.
 */

public class PaymentSettingInfo {


    @SerializedName("setting_id")
    public String settingId;

    @SerializedName("terms_link")
    public String termsLink;

    @SerializedName("referral_price")
    public String referralPrice;

    @SerializedName("bank_details")
    public String bankDetails;

    @SerializedName("createdate")
    public String createdate;
}

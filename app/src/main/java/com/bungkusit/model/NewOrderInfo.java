package com.bungkusit.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.Keep;

import com.bungkusit.BR;
import com.google.gson.annotations.SerializedName;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

@Keep
public class NewOrderInfo extends BaseObservable {
    @SerializedName("orderid")
    public String orderid;

    @SerializedName("userid")
    public String userid;

    @SerializedName("rider_id")
    public String riderId;

    @SerializedName("ordertype")
    public String ordertype;

    @SerializedName("user_cash_payment")
    public String userCashPayment;


    @SerializedName("fare_amount")
    public String fare_amount;

    @SerializedName("cash_payment")
    public String cashPayment;

    @SerializedName("change_received")
    public String changeReceived;

    @SerializedName("tips")
    public String tips;

    @SerializedName("total_amount")
    public String totalAmount;

    @SerializedName("pickup_address")
    public String pickupAddress;

    @SerializedName("plat")
    public String plat;

    @SerializedName("plng")
    public String plng;

    @SerializedName("destination_address")
    public String destinationAddress;

    @SerializedName("dlat")
    public String dlat;

    @SerializedName("dlng")
    public String dlng;

    @SerializedName("delivery_address")
    public String deliveryAddress;

    @SerializedName("dllat")
    public String dllat;

    @SerializedName("dllng")
    public String dllng;

    @SerializedName("delivery_type")
    public String deliveryType;

    @SerializedName("description")
    public String description;

    @SerializedName("images")
    public ArrayList<String> images;

    @SerializedName("promocode")
    public String promocode;

    @SerializedName("sender_name")
    public String senderName;

    @SerializedName("recipient_name")
    public String recipientName;

    @SerializedName("courier_service_name")
    public String courierServiceName;

    @SerializedName("status")
    public String status;

    @SerializedName("riderstatus")
    public String riderstatus;

    @SerializedName("no_of_km")
    public String noOfKm;

    @SerializedName("orderdate")
    public String orderdate;

    @SerializedName("deliverdate")
    public String deliverdate;

    @SerializedName("modifieddate")
    public String modifieddate;

    @SerializedName("firstname")
    public String firstname;

    @SerializedName("lastname")
    public String lastname;

    @SerializedName("email")
    public String email;

    @SerializedName("contactno")
    public String contactno;

    @SerializedName("isbided")
    public String isbided;


    @SerializedName("is_assigned")
    public String isAssigned;

    @SerializedName("is_pickup")
    public String ispickup;

    @SerializedName("userwallet")
        public String userwallet;

    @SerializedName("order_total")
    public String order_total;

    @SerializedName("order_items")
    public String order_items;

    @SerializedName("merchant_order_id")
    public String merchant_order_id;

    @SerializedName("shipping_amount")
    public String shippingAmount;

    @SerializedName("pickup_amount")
    public String pickupAmount;

    @SerializedName("dropoff_amount")
    public String dropoffAmount;

    @Bindable
    public String getIsbided() {
        return isbided;
    }

    public void setIsbided(String isbided) {
        this.isbided = isbided;
        notifyPropertyChanged(BR.isbided);
    }

    public String getFare_amount(){
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);;
        formatter.applyPattern("#,###,###");
        double fareAmount = Double.parseDouble(fare_amount);
        return formatter.format(fareAmount) + " đ";
    }

    public String getOrderTotalAmount() {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);;
        formatter.applyPattern("#,###,###");
        double totalAmount = Double.parseDouble(order_total);
        return formatter.format(totalAmount) + " đ";
    }

    public String getPickupAmount() {

        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);;
        formatter.applyPattern("#,###,###");
        double totalAmount = Double.parseDouble(pickupAmount);
        return formatter.format(totalAmount) + " đ";
    }

    public String getDropoffAmount() {

        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);;
        formatter.applyPattern("#,###,###");
        double totalAmount = Double.parseDouble(dropoffAmount);
        return formatter.format(totalAmount) + " đ";
    }

    public String getShippingAmount() {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);;
        formatter.applyPattern("#,###,###");
        double totalAmount = Double.parseDouble(shippingAmount);
        return formatter.format(totalAmount) + " đ";
    }

    public String getDistanceKm() {
        return noOfKm + " Km";
    }

}

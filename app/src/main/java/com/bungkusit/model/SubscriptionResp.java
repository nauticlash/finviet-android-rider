package com.bungkusit.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cears on 21/11/17.
 */

public class SubscriptionResp {

    @SerializedName("success")
    public int success;

    @SerializedName("subscriptionid")
    public int subscriptionid;

    @SerializedName("message")
    public String message;


}

package com.bungkusit.model;

import android.support.annotation.Keep;

import com.bungkusit.utils.AppGlobal;
import com.google.gson.annotations.SerializedName;

@Keep
public class ChatInfo {


    @SerializedName("id")
    public String id;

    @SerializedName("fromId")
    public String fromId;

    @SerializedName("toId")
    public String toId;

    @SerializedName("order_id")
    public String order_id;

    @SerializedName("message")
    public String message;

  @SerializedName("totalunread")
    public String totalunread;

    @SerializedName("attachment")
    public String attachment;

    @SerializedName("isViewed")
    public String isViewed;

    @SerializedName("is_deleted")
    public String isDeleted;

    @SerializedName("create_date")
    public String createDate;

    @SerializedName("modify_date")
    public String modifyDate;

    @SerializedName("delete_date")
    public String deleteDate;

    @SerializedName("userid")
    public String userid;

    @SerializedName("firstname")
    public String firstname;

    @SerializedName("lastname")
    public String lastname;

    @SerializedName("usertype")
    public String usertype;

    @SerializedName("profilepic")
    public String profilepic;

    public String daycount()
    {
        String daycount= AppGlobal.getTime(createDate);
        return daycount;
    }
}

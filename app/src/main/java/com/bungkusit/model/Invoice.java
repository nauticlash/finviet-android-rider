package com.bungkusit.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cears on 25/11/17.
 */

public class Invoice {

    @SerializedName("sub_id")
    public String sub_id;

    @SerializedName("riderid")
    public String riderid;

    @SerializedName("sub_start_date")
    public String sub_start_date;

    @SerializedName("sub_end_date")
    public String sub_end_date;

    @SerializedName("subamount")
    public String subamount;

    @SerializedName("recimage")
    public String recimage;

    @SerializedName("payvia")
    public String payvia;

    @SerializedName("status")
    public String status;

    @SerializedName("is_deleted")
    public String is_deleted;

    @SerializedName("create_date")
    public String create_date;

    @SerializedName("modify_date")
    public String modify_date;

    @SerializedName("delete_date")
    public String delete_date;

    @SerializedName("firstname")
    public String firstname;

    @SerializedName("lastname")
    public String lastname;

    @SerializedName("address")
    public String address;

    @SerializedName("email")
    public String email;

    @SerializedName("contactno")
    public String contactno;

    @SerializedName("wallet")
    public String wallet;
    }

package com.bungkusit.model;

import android.support.annotation.Keep;

import com.google.gson.annotations.SerializedName;

@Keep
public class OrderDetailResp {

    @SerializedName("success")
    public int success;

    @SerializedName("message")
    public String message;

    @SerializedName("data")
    public OrderDetailInfo data;
}

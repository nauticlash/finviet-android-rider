package com.bungkusit.model;

import android.support.annotation.Keep;

import com.google.gson.annotations.SerializedName;

@Keep
public class NotificationInfo {

    @SerializedName("id")
    public String id;

    @SerializedName("userid")
    public String userid;

    @SerializedName("riderid")
    public String riderid;

    @SerializedName("message")
    public String message;

    @SerializedName("order_id")
    public String order_id;

    @SerializedName("type")
    public String type;

    @SerializedName("status")
    public String status;

    @SerializedName("isview")
    public String isview;

    @SerializedName("createdate")
    public String createdate;


}

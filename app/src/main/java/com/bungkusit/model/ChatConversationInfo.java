package com.bungkusit.model;

import android.support.annotation.Keep;

import com.bungkusit.utils.AppGlobal;
import com.google.gson.annotations.SerializedName;

@Keep
public class ChatConversationInfo {

    @SerializedName("id")
    public String id;
    
    @SerializedName("fromId")
    public String fromId;
    
    @SerializedName("toId")
    public String toId;
    
    @SerializedName("message")
    public String message;
    
    @SerializedName("attachment")
    public String attachment;
    
    @SerializedName("isViewed")
    public String isViewed;
    
    @SerializedName("is_deleted")
    public String isDeleted;
    
    @SerializedName("create_date")
    public String createDate;
    
    @SerializedName("modify_date")
    public String modifyDate;
    
    @SerializedName("delete_date")
    public String deleteDate;
    
    @SerializedName("attachmenturl")
    public String attachmenturl;
    
    @SerializedName("fromprofile")
    public String fromprofile;
    
    @SerializedName("toprofile")
    public String toprofile;

    public String daycount()
    {
        String daycount= AppGlobal.getTime(createDate);
        return daycount;
    }
}

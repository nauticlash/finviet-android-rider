package com.bungkusit.model;

/**
 * Created by cears on 21/11/17.
 */

import com.google.gson.annotations.SerializedName;

public class PaymentResp {

    @SerializedName("message")
    public String message;

    @SerializedName("success")
    public String success;

    @SerializedName("bank_details")
    public String bank_details;

    @SerializedName("wallet")
    public String wallet;

    @SerializedName("carry_forward")
    public String carry_forward;

    @SerializedName("status")
    public String status;

    @SerializedName("remaindays")
    public String remaindays;

    @SerializedName("recimage")
    public String recimage;

    @SerializedName("invoice")
    public PaymentInfo paymentInfo;


}
package com.bungkusit.model;

import android.support.annotation.Keep;

import com.google.gson.annotations.SerializedName;

@Keep
public class SettingInfo {

    @SerializedName("pricing_id")
    public String pricing_id;

    @SerializedName("holiday_date")
    public String holiday_date;

    @SerializedName("min_km")
    public String min_km;

    @SerializedName("min_price_per_km")
    public String min_price_per_km;

    @SerializedName("min_up_km")
    public String min_up_km;

    @SerializedName("min_up_price")
    public String min_up_price;

    @SerializedName("flag")
    public String flag;

}

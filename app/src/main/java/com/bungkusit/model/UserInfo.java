package com.bungkusit.model;

import android.databinding.BaseObservable;
import android.support.annotation.Keep;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Keep
public class UserInfo extends BaseObservable implements Serializable {
    @SerializedName("userid")
    public String userid;

    @SerializedName("fbid")
    public String fbid;

    @SerializedName("usertype")
    public String usertype;

    @SerializedName("firstname")
    public String firstname;

    @SerializedName("lastname")
    public String lastname;

    @SerializedName("totalunread_msgs")
    public String totalunread_msgs;

    @SerializedName("dob")
    public String dob;

    @SerializedName("address")
    public String address;

    @SerializedName("email")
    public String email;

    @SerializedName("password")
    public String password;

    @SerializedName("contactno")
    public String contactno;

    @SerializedName("clat")
    public String clat;

    @SerializedName("clng")
    public String clng;

    @SerializedName("lat")
    public String lat;

    @SerializedName("lng")
    public String lng;

    @SerializedName("profilepic")
    public String profilepic;

    @SerializedName("license_image")
    public String licenseImage;

    @SerializedName("front_photo")
    public String frontPhoto;

    @SerializedName("back_photo")
    public String backPhoto;

    @SerializedName("ic_front_photo")
    public String icFrontPhoto;

    @SerializedName("ic_back_photo")
    public String icBackPhoto;

    @SerializedName("bike_number_plate")
    public String bikeNumberPlate;

    @SerializedName("bike_pic")
    public String bikePic;

    @SerializedName("insurance_latter_pic")
    public String insuranceLatterPic;

    @SerializedName("road_tex_pic")
    public String roadTexPic;

    @SerializedName("isfree")
    public String isfree;

    @SerializedName("subscription_plan")
    public String subscriptionPlan;

    @SerializedName("sub_enddate")
    public String subEnddate;

    @SerializedName("online_status")
    public String onlineStatus;

    @SerializedName("code")
    public String code;

    @SerializedName("is_verify")
    public String isVerify;

    @SerializedName("platform")
    public String platform;

    @SerializedName("deviceid")
    public String deviceid;

    @SerializedName("status")
    public String status;

    @SerializedName("createdate")
    public String createdate;


    @SerializedName("newnotification")
    public String newnotification;


    @SerializedName("satisfaction_rate")
    public float satisfaction_rate;

    @SerializedName("communication_rate")
    public float communication_rate;

    @SerializedName("ontime_rate")
    public float ontime_rate;

//    @SerializedName("notes")
//    public float notes;


    @SerializedName("state")
    public String state;

    public String getEmailString() {
        return email.equals("") ? "Chưa có thông tin" : email;
    }

}
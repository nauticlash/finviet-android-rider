package com.bungkusit.model;

import android.support.annotation.Keep;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

@Keep
public class NewOrderResp {

    @SerializedName("success")
    public int success;

    @SerializedName("message")
    public String message;

    @SerializedName("is_assigned")
    public String is_assigned;

    @SerializedName("data")
    public List<NewOrderInfo> data = new ArrayList<>();
}

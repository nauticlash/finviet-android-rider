package com.bungkusit.model;

import android.support.annotation.Keep;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@Keep
public class NotificationResp {

    @SerializedName("success")
    public int success;

    @SerializedName("message")
    public String message;

    @SerializedName("count")
    public String count;

    @SerializedName("data")
    public List<NotificationInfo> data;


}

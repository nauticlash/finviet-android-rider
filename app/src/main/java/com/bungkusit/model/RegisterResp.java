package com.bungkusit.model;

import android.support.annotation.Keep;

import com.google.gson.annotations.SerializedName;

@Keep
public class RegisterResp {

    @SerializedName("success")
    public int success;

    @SerializedName("message")
    public String message;

    @SerializedName("token")
    public String token;

    @SerializedName("code")
    public String code;

    @SerializedName("userid")
    public String userid;



}

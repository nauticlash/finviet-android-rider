package com.bungkusit.utils;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.bungkusit.R;


public class PermissionUtils {

    public static final int REQUEST_CODE_CAMERA_GALLERY_PERMISSION = 101;
    public static final int REQUEST_CODE_LOCATION_PERMISSION = 102;
    public static final int REQUEST_CODE_EXTERNAL_STORAGE = 103;

    public static final String[] REQUEST_CAMERA_GALLERY_PERMISSION = new String[]{"android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE"};
    public static final String[] REQUEST_LOCATION_PERMISSION = new String[]{"android.permission.ACCESS_COARSE_LOCATION"};
    public static final String[] REQUEST_EXTERNAL_STORAGE = new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"};


    private Activity activity;
    private Fragment fragment;

    private OnPermissionResponse onPermissionResponse;
    private AlertDialog.Builder alertDialogBuilder;
    private AlertDialog dialog;

    public PermissionUtils(Activity activity) {
        this.activity = activity;
        onPermissionResponse = (OnPermissionResponse) activity;
    }

    public PermissionUtils(Fragment fragment) {
        this.activity = fragment.getActivity();
        this.fragment = fragment;
        onPermissionResponse = (OnPermissionResponse) fragment;
    }

    /**
     * @param permissions    string list of permission you want to ask for
     * @param requestCode    int code for requesting permission
     * @param onForceDenyMsg message for show alert dialog when user force fully deny(On never ask) permission
     */
    public void requestPermissions(String[] permissions, int requestCode, String... onForceDenyMsg) {


        if (checkPermission(permissions)) {
            if (onPermissionResponse != null) {
                onPermissionResponse.onPermissionGranted(requestCode);
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (fragment != null) {
                    fragment.requestPermissions(permissions, requestCode);
                } else {
                    activity.requestPermissions(permissions, requestCode);
                }
            }
        }

    }

    /**
     * @param permissions string list of permission you want to ask for
     * @return boolean  returns true if permission already granted
     */

    public boolean checkPermission(String[] permissions) {
        if (Build.VERSION.SDK_INT < 23) {
            return true;
        } else {
            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(activity, permission) != 0) {
                    return false;
                }
            }
            return true;
        }
    }


    public interface OnPermissionResponse {
        void onPermissionGranted(int requestCode);

        void onPermissionDenied(int requestCode);
    }

    /**
     * @param requestCode  requestCode received from onRequestPermissionsResult()
     * @param permissions  permissions received from onRequestPermissionsResult()
     * @param grantResults grantResults received from onRequestPermissionsResult()
     */
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults, String... onForceDenyMsg) {
        if (verifyPermissionResults(grantResults)) {
            if (onPermissionResponse != null) {
                onPermissionResponse.onPermissionGranted(requestCode);
            }
        } else {
            if (onPermissionResponse != null) {
                isPermissionRationale(permissions, requestCode, onForceDenyMsg);
//                onPermissionResponse.onPermissionDenied(requestCode);
            }
        }
    }

    private void isPermissionRationale(String[] permissions, int requestCode, String[] onForceDenyMsg) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Boolean isPermissionRationale = false;

            if (fragment != null && fragment.getActivity() != null) {
                for (String permission : permissions) {
                    if (!fragment.shouldShowRequestPermissionRationale(permission)) {
                        isPermissionRationale = true;
                        break;
                    }
                }
                if (isPermissionRationale) {
                    final Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", fragment.getActivity().getPackageName(), null);
                    intent.setData(uri);

                    alertDialogBuilder = new AlertDialog.Builder(fragment.getActivity())
                            .setTitle("Cần cấp quyền")
                            .setPositiveButton("Đồng ý", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    fragment.startActivity(intent);
                                }
                            });

                    if (onForceDenyMsg.length > 0) {
                        alertDialogBuilder.setMessage(fragment.getString(R.string.permission, onForceDenyMsg[0]));
                    } else {
                        alertDialogBuilder.setMessage(fragment.getString(R.string.permission, ""));
                    }

                    dialog = alertDialogBuilder.create();

                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    dialog.show();

                } else {
                    onPermissionResponse.onPermissionDenied(requestCode);
                }
            } else {
                for (String permission : permissions) {
                    if (!activity.shouldShowRequestPermissionRationale(permission)) {
                        isPermissionRationale = true;
                        break;
                    }
                }

                Debug.e("isPermissionRationale", "" + isPermissionRationale);

                if (isPermissionRationale) {
                    final Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
                    intent.setData(uri);


                    alertDialogBuilder = new AlertDialog.Builder(activity)
                            .setTitle("Vui lòng cấp quyền")
                            .setPositiveButton("Đồng ý", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    activity.startActivity(intent);
                                }
                            });
                    if (onForceDenyMsg.length > 0) {
                        alertDialogBuilder.setMessage(activity.getString(R.string.permission));
                    } else {
                        alertDialogBuilder.setMessage(activity.getString(R.string.permission));
                    }

                    dialog = alertDialogBuilder.create();

                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }

                    dialog.show();
                } else {
                    onPermissionResponse.onPermissionDenied(requestCode);
                }
            }
        }
    }

    public boolean verifyPermissionResults(int[] grantResults) {
        // At least one result must be checked.
        if (grantResults.length < 1) {
            return false;
        }
        // Verify that each required permission has been granted, otherwise return false.
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }
}

package com.bungkusit.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.bungkusit.model.SettingInfo;
import com.bungkusit.model.UserInfo;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class AppPreferences {

    private final static String PREFS_FILE = "BungkusitRider";

    private final static String KEY_TOKEN = "token";
    private final static String KEY_USER_INFO = "user_info";
    private final static String KEY_SETTING_INFO = "setting_info";
    private final static String KEY_LOCATION_HISTORY = "loc_history";

    private final static String KEY_NOTIFICATION_BEDGE = "notification_bedge";
    private final static String KEY_ISLOGIN = "is_login";
    private final static String KEY_SECRET_LOG_ID = "secret_log_id";

    private final static String KEY_DEVICE_TOKEN = "device_token";

    private final static String KEY_ISONLINE = "is_online";

    private final static String KEY_ISVERIFY = "is_verify";

    private final static String KEY_ISASSIGNED = "is_assigned";

    private final static String KEY_LAST_LOCATION = "last_location";

    private SharedPreferences prefs;

    public AppPreferences(Context context) {
        prefs = context.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
    }

    public void setNotificationBedgeInc() {
        prefs.edit().putInt(KEY_NOTIFICATION_BEDGE, getNotificationBedge() + 1).apply();
    }

    public void setNotificationBedgeInc(int count) {
        prefs.edit().putInt(KEY_NOTIFICATION_BEDGE, count).apply();
        Debug.e("notificationsharedpf", getNotificationBedge().toString());
    }

    public void setNotificationBedgeDec() {
        if (getNotificationBedge() > 0) {
            prefs.edit().putInt(KEY_NOTIFICATION_BEDGE, getNotificationBedge() - 1).apply();
        }
    }

    public void setLastLocation(LatLng location) {
        String info = new Gson().toJson(location);
        prefs.edit().putString(KEY_LAST_LOCATION, info).apply();
    }

    public LatLng getLastLocation() {
        LatLng location;
        try {
            location = new Gson().fromJson(prefs.getString(KEY_LAST_LOCATION, ""), LatLng.class);
        } catch (Exception e) {
            location = new LatLng(0,0);
        }

        return location;
    }

    public Integer getNotificationBedge() {
        return prefs.getInt(KEY_NOTIFICATION_BEDGE, 0);
    }

    public boolean isLogin() {
        return prefs.getBoolean(KEY_ISLOGIN, false);
    }

    public void setLogin(boolean islogin) {
        prefs.edit().putBoolean(KEY_ISLOGIN, islogin).apply();
    }


    public boolean isVerify() {
        return prefs.getBoolean(KEY_ISVERIFY, false);
    }

    public void setVerify(boolean isVerify) {
        prefs.edit().putBoolean(KEY_ISVERIFY, isVerify).apply();
    }


    public void setAssigned(boolean isAssigned) {
        prefs.edit().putBoolean(KEY_ISASSIGNED, isAssigned).apply();
    }

    public boolean isAssigned() {
        return prefs.getBoolean(KEY_ISASSIGNED, false);
    }

    public boolean isOnline() {
        return prefs.getBoolean(KEY_ISONLINE, true);
    }

    public String getOnlineString() {
        return prefs.getBoolean(KEY_ISONLINE, true) ? "is_online" : "is_offline";
    }

    public void setOnline(boolean isOnline) {
        prefs.edit().putBoolean(KEY_ISONLINE, isOnline).apply();
    }

    public void setToken(String token) {
        prefs.edit().putString(KEY_TOKEN, token).apply();
    }

    public String getToken() {
        return prefs.getString(KEY_TOKEN, "");
    }


    public void setDeviceToken(String deviceToken) {
        prefs.edit().putString(KEY_DEVICE_TOKEN, deviceToken).apply();
    }

    public String getDeviceToken() {
        return prefs.getString(KEY_DEVICE_TOKEN, "");
    }

    public void setSecretLogId(String token) {
        prefs.edit().putString(KEY_SECRET_LOG_ID, token).apply();
    }

    public String getSecretLogId() {
        return prefs.getString(KEY_SECRET_LOG_ID, "");
    }


    public void setUserInfo(UserInfo userInfo) {
        String info = new Gson().toJson(userInfo);
        prefs.edit().putString(KEY_USER_INFO, info).apply();
    }

    public UserInfo getUserInfo() {
        UserInfo loginInfo;
        try {
            loginInfo = new Gson().fromJson(prefs.getString(KEY_USER_INFO, ""), UserInfo.class);
        } catch (Exception e) {
            loginInfo = new UserInfo();
        }

        return loginInfo;
    }

    public void setSetting(SettingInfo userInfo) {
        String info = new Gson().toJson(userInfo);
        prefs.edit().putString(KEY_SETTING_INFO, info).apply();
    }

    public SettingInfo getSetting() {
        SettingInfo settingInfo;
        try {
            settingInfo = new Gson().fromJson(prefs.getString(KEY_SETTING_INFO, ""), SettingInfo.class);
        } catch (Exception e) {
            settingInfo = new SettingInfo();
        }

        return settingInfo;
    }


    public void clearAllPrefValues() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();

    }

}
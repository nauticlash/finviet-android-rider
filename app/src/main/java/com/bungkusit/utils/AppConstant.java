package com.bungkusit.utils;

public class AppConstant {
    public static boolean IS_PRODUCTION = false;
    //    public static String BASE_URL = IS_PRODUCTION ? "http://jommalasa.cearsinfotech.in/api" : "http://jommalasa.cearsinfotech.in/api";
    public static String BASE_URL = "";
    public static String SOCKET_URL = "";
    //    public static String URL = BASE_URL + "/";
    public static int TIME_OUT = 60;

    public static String currentFragment;

    // API Constants

    public static int REQUEST_CODE_1 = 101;
    public static int REQUEST_CODE_2 = 102;
    public static int REQUEST_CODE_3 = 103;
    public static int REQUEST_CODE_4 = 104;
    public static int REQUEST_CODE_5 = 105;
    public static int REQUEST_CODE_6 = 106;
    public static int REQUEST_CODE_7 = 107;
    public static int REQUEST_CODE_8 = 108;
    public static int REQUEST_CODE_9 = 109;
    public static int REQUEST_CODE_10 = 110;


    public static int REQUEST_CAMERA_GALLERY_PERMISSION_CODE = 201;
    public static int ACTION_SETTING_CODE = 0;

    // SHared preference keys
    public static String SP_TOKEN = "token";
    public static String FIRSTNAME = "firstname";
    public static int REQUEST_BACK = 100;
    public static String LASTNAME = "lastname";
    public static String EMAIL = "email";
    public static String PASSWORD = "password";
    public static String CONTACTNO = "contactno";
    public static String PLATFORM = "platform";
    public static String USERTYPE = "usertype";
    public static String LAT = "lat";
    public static String LNG = "lng";
    public static String DOB = "dob";
    public static String ADDRESS = "address";
    public static String DEVICEID = "deviceid";
    public static String COUNTRY = "country";
    public static String STATE = "state";
    public static String CITY = "city";

    public static String PLATFORM_AND = "android";
    public static String USER_TYPE_DRIVER = "driver";

    //public static String COUNTRY_CODE = "IN";
    public static String COUNTRY_CODE = "MY";

    public static int pos = 0;
//    public static double[] dLat = {21.2332079, 21.234235, 21.2341175, 21.2339725, 21.2335782, 21.2334632, 21.2333232, 21.2332032, 21.2330402, 21.2328752, 21.2326772, 21.2321756, 21.2321652, 21.2318452, 21.2316072, 21.2311915, 21.2313292,21.2266182,21.2263032,21.2257732,21.2250632,21.223529,21.2216705};
//    public static double[] dLong = {72.8337968, 72.8289643, 72.8303747, 72.8311467, 72.8330158, 72.8335308, 72.8340328, 72.8345318, 72.8350198, 72.8356258, 72.8361838, 72.837138, 72.8371678, 72.8375648, 72.8378628, 72.8383803, 72.8383038,72.8374048,72.8372708,72.8372818,72.8372978,72.8365626,72.8372393};
    public static double[] dLat = {21.2332079, 21.234235, 21.2333232, 21.2332032, 21.2330402, 21.2328752, 21.2326772, 21.2321756, 21.2321652, 21.2318452, 21.2316072, 21.2311915, 21.2313292,21.2266182,21.2263032,21.2257732,21.2250632,21.223529,21.2216705};
    public static double[] dLong = {72.8337968, 72.8289643, 72.8340328, 72.8345318, 72.8350198, 72.8356258, 72.8361838, 72.837138, 72.8371678, 72.8375648, 72.8378628, 72.8383803, 72.8383038,72.8374048,72.8372708,72.8372818,72.8372978,72.8365626,72.8372393};


    public static boolean isGpsEnabled;
    public static boolean isNetworkGPSEnabled;

    public static String NEW_ORDER_SERVER_URL = "";


    public interface ACTION {
        public static String MAIN_ACTION = "com.bungkusit.service.locationservice.action.main";
        public static String STARTFOREGROUND_ACTION = "com.bungkusit.service.locationservice.action.startforeground";
        public static String STOPFOREGROUND_ACTION = "com.bungkusit.service.locationservice.action.stopforeground";
    }

    public interface NOTIFICATION_ID {
        public static int FOREGROUND_SERVICE = 100000;
    }


}
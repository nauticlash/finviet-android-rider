/**
 * @author c61
 * All the global Methods LocatorApp
 */

package com.bungkusit.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.bungkusit.R;
import com.bungkusit.activity.main.MainPresenter;
import com.bungkusit.activity.main.MainPresenterImpl;
import com.bungkusit.model.CommonResp;
import com.bungkusit.views.textview.TextViewRegular;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;

import static com.bungkusit.utils.AppConstant.REQUEST_CODE_8;
import static com.bungkusit.utils.AppConstant.isGpsEnabled;
import static com.bungkusit.utils.AppConstant.isNetworkGPSEnabled;


@SuppressWarnings("ALL")
@SuppressLint("InflateParams")
public class AppGlobal {


    public static final String DATEFORMAT = "yyyy-MM-dd HH:mm:ss";

    // Email Pattern
    public final static Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9+._%-+]{1,256}" +
                    "@" +
                    "[a-zA-Z0-9][a-zA-Z0-9-]{0,64}" +
                    "(" +
                    "." +
                    "[a-zA-Z0-9][a-zA-Z0-9-]{0,25}" +
                    ")+"
    );

    public final static Pattern PASSWORD_NUMBER_PATTERN = Pattern
            .compile("^(?=.*[A-Za-z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{6,}$");


    // Phone Pattern
    public final static Pattern PHONE_NUMBER_PATTERN = Pattern
            .compile("^[7-9][0-9]{9}$");
    private static SimpleDateFormat localFormat;
    private static AlertDialog.Builder builder1;
    private static AlertDialog alertDialog;

    public static boolean isGPSAvailable = locationServicesEnabled(MyApplication.getInstance());


    /**
     * Email validation
     *
     * @param email
     * @return
     */
   /* public static boolean checkEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }*/
    public static boolean checkEmail(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean checkPhoneNumber(String phone) {
        return PHONE_NUMBER_PATTERN.matcher(phone).matches();
    }

    public static boolean checkPassword(String password) {
        return PASSWORD_NUMBER_PATTERN.matcher(password).matches();
    }


    public static void displayAlertDilog(final Context mContext, String msg) {


        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
        builder1.setMessage(Html.fromHtml(msg));
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Đồng ý",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    /**
     * check Network Connection
     *
     * @param context
     * @return
     */
    public static boolean isNetwork(Context context) {

        try {
            ConnectivityManager cm = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }

    }

    /**
     * Hide Keyboard
     *
     * @param mContext
     */
    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }


    public static void showSnakeBar(Context context, View view, String message, int... duration) {
        try {

            if (duration != null && duration.length > 0) {
                Snackbar snackbar1 = Snackbar.make(view, message, duration[0]);
                snackbar1.show();
            } else {
                Snackbar snackbar1 = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
                snackbar1.show();
            }


        } catch (Exception e) {
        }
    }

    /**
     * Check Edittext value null or not
     *
     * @param text        String of the EditText
     * @param focusView   if text blank then Scroll on this view
     * @param editText    if text blank then focus on editext
     * @param inputLayout if text blank then set error on TextInputLayout
     * @param error       if text blank then message error
     * @return
     */

    public static boolean isNull(String text, View focusView, View editText, TextInputLayout inputLayout, String error) {

        if (text.equals("")) {
            requestFocus(focusView, editText);
            inputLayout.setError(error);
            return false;
        }
        return true;
    }

    public static boolean isEmpty(String s) {
        if (s == null)
            return true;
        if (s.trim().length() == 0)
            return true;
        return false;
    }

    public static void requestFocus(final View scrollView, final View editText) {
        editText.requestFocus();

        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, editText.getTop());
            }
        });
    }

    public static void errorDisable(final TextInputLayout inputLayout) {
        inputLayout.setErrorEnabled(false);
        inputLayout.setError("");
    }

    public static Map<String, String> getHeaderData() {
        AppPreferences preferences = MyApplication.getInstance().getAppPreferences();

        Map<String, String> map = new HashMap<>();
//        map.put("Authorization", preferences.getTokenType() + " " + preferences.getToken());

        return map;
    }


    public static String getDate(String date) {
        try {


            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date newDate = null;
            try {
                newDate = format.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            format = new SimpleDateFormat("dd/MM/yyyy");
            String dateF = format.format(newDate);

            return dateF;
        } catch (Exception e) {
            return date;
        }
    }

    public static String getFormatedDate(String date) {
        try {


            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date newDate = null;
            try {
                newDate = format.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            format = new SimpleDateFormat("HH:mm:ss, ngày dd/MM/yyyy");
            String dateF = format.format(newDate);

            return dateF;
        } catch (Exception e) {
            return date;
        }
    }

    public static String getDateNotification(String date) {
        try {


            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date newDate = null;
            try {
                newDate = format.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            format = new SimpleDateFormat("dd/MM/yyyy, HH:mm:ss");
            String dateF = format.format(newDate);

            return dateF;
        } catch (Exception e) {
            return date;
        }
    }

    public static int getDay() {
        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("dd");
        String formattedDate = df.format(calendar.getTime());

        return Integer.parseInt(formattedDate);
    }

    public static int getMonth() {
        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("MM");
        String formattedDate = df.format(calendar.getTime());

        return Integer.parseInt(formattedDate);
    }

    public static int getYear() {
        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy");
        String formattedDate = df.format(calendar.getTime());

        return Integer.parseInt(formattedDate);
    }

//    public static String getTime(String date) {
//        try {
//            date = date.replace("T", " ");
//            date = date.replace("+", " ");
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            Date dat = null;
//            PrettyTime p = new PrettyTime();
//            p.setLocale(Locale.ENGLISH);
//            String time = "";
//            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
////            format.setTimeZone(TimeZone.getTimeZone("UTC"));
//            dat = format.parse("" + date.toString()
//                    .split("\\.")[0].toString()
//                    .replace("T", " "));
//
//
//            time = "" + p.format(dat);
//            return time;
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return "";
//    }


    public static String getTime(String date) {

//        2017-09-21T07:36:19+0000
        date = date.replace("T", " ");
        date = date.replace("+", " ");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dat = null;
        PrettyTime p = new PrettyTime();
        p.setLocale(Locale.ENGLISH);
        String time = "";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
//            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            dat = format.parse("" + date.toString()
                    .split("\\.")[0].toString()
                    .replace("T", " "));


            time = "" + p.format(dat);


        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time;
    }


    /*

    Waze App redirection for Navigation

    @param mContext
    @param latitude
    @param longitude
     */
    public static void reditectToWaze(Activity activity, String latitude, String longitude) {
        try {
            String url = "waze://?ll=" + latitude + "," + longitude + "&navigate=yes";
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            activity.startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            // If Waze is not installed, open it in Google Play:
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.waze"));
            activity.startActivity(intent);
        }
    }

    public static boolean isForeground(String myPackage) {
        ActivityManager manager = (ActivityManager) MyApplication.getInstance()
                .getSystemService(Activity.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfo = manager
                .getRunningTasks(1);

        ComponentName componentInfo = runningTaskInfo.get(0).topActivity;
        if (componentInfo.getPackageName().equals(myPackage))
            return true;
        return false;
    }


    public static String utcToLocalDateFormat(String dateString) {

        String formattedDate = null;
        Date date;

        if (dateString != null) {

            DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            try {

                date = utcFormat.parse(dateString);

                DateFormat localFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                localFormat.setTimeZone(TimeZone.getDefault());

                formattedDate = localFormat.format(date);

            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        return formattedDate;
    }

    public static String getCurrentDateTime() {
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentDateTime = dateFormat.format(new Date()); // Find todays date

            return currentDateTime;
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }

    public static long getTimeDifference(String start, String currentTime) {

        Date startDate;
        Date currentendDate;
        Long diffInSeconds, diffInMinutes, diffInHours;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getDefault());

        try {

            startDate = sdf.parse(start);
            currentendDate = sdf.parse(currentTime);

            long duration = currentendDate.getTime() - startDate.getTime();

            diffInSeconds = TimeUnit.MILLISECONDS.toSeconds(duration);
            diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
            diffInHours = TimeUnit.MILLISECONDS.toHours(duration);

            return diffInSeconds;

        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }


    }

    public static void openLocationDialog(final Context context, String... msg) {


        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        } else {

            builder1 = new AlertDialog.Builder(context);
            builder1.setTitle(context.getString(R.string.location_title));
            if (msg.length > 0 && msg != null) {
                builder1.setMessage(msg[0]);
            } else {
                builder1.setMessage(context.getString(R.string.location_msg));
            }
            builder1.setCancelable(false);
            builder1.setPositiveButton(
                    "Đồng ý", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            try {
                                context.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                alertDialog.dismiss();
                            } catch (Exception e) {
                            }

                        }
                    });

            alertDialog = builder1.create();
            alertDialog.show();
        }


    }

    public static String convertDateFormatToDashed(String dateToFormat) {
        String[] data = dateToFormat.split("/");
        return data[2] + "-" + data[1] + "-" + data[0];
    }

    public static String convertDateToSlashFormat(String dateToFormat) {

        String[] data = dateToFormat.split("-");
        return data[2] + "/" + data[1] + "/" + data[0];
    }

    public static void updateDevice(Activity context) {

        String appVersion="";
        try {
            PackageInfo pInfo = MyApplication.getInstance().getPackageManager().getPackageInfo( MyApplication.getInstance().getPackageName(), 0);
            appVersion=String.valueOf(pInfo.versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        AppPreferences appPreferencesHelper = MyApplication.getInstance().getAppPreferences();

        if (appPreferencesHelper.getUserInfo() != null && appPreferencesHelper.getUserInfo().userid != null) {

            if (AppGlobal.isNetwork(context)) {

                MainPresenter presenter = new MainPresenterImpl(context);

                HashMap<String, Object> request = new HashMap<>();

                request.put("platform", "android");
                request.put("deviceid", appPreferencesHelper.getDeviceToken());
                request.put("devicename", Build.MODEL);
                request.put("deviceversion", Build.VERSION.RELEASE);
                request.put("appversion", appVersion);

                HashMap<String, Object> auth = new HashMap<>();
                auth.put("id", appPreferencesHelper.getUserInfo().userid);
                auth.put("token", appPreferencesHelper.getToken());

                HashMap<String, Object> map = new HashMap<>();
                map.put(MyApplication.getInstance().getString(R.string.service), MyApplication.getInstance().getResources().getString(R.string.update_device));
                map.put(MyApplication.getInstance().getString(R.string.request), request);
                map.put("auth", auth);

                Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

                presenter.editProfile(requestService, new CommonResp(), REQUEST_CODE_8);

            }
        }


    }


    public static boolean locationServicesEnabled(Context context) {

        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        isGpsEnabled = false;
        isNetworkGPSEnabled = false;
        isGPSAvailable = false;

        try {
            isGpsEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            Debug.e("isGpsEnabled", "isGpsEnabled=>" + isGpsEnabled);
        } catch (Exception ex) {
            isGpsEnabled = false;
        }

        try {
            isNetworkGPSEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            Debug.e("isNetworkGPSEnabled", "isNetworkGPSEnabled=>" + isNetworkGPSEnabled);
        } catch (Exception ex) {
            isNetworkGPSEnabled = false;
        }

        return isGpsEnabled || isNetworkGPSEnabled;
    }


    public static final int TOAST_LENGTH_SHORT = 0;
    public static final int TOAST_LENGTH_LONG = 1;
    public static void showToast(Context context, LayoutInflater inflater, int messageId, int duration) {
        View layout = inflater.inflate(R.layout.custom_toast_success,
                (ViewGroup) ((Activity) context).findViewById(R.id.llMainToast));

        TextViewRegular text = (TextViewRegular) layout.findViewById(R.id.tvAlert);
        text.setText(messageId);

        Toast toast = new Toast(context);
        toast.setGravity(Gravity.TOP | Gravity.FILL_HORIZONTAL, 0, 0);
        toast.setDuration(duration);
        toast.setView(layout);
        toast.show();
    }


    public static void showKeyboardWithFocus(View v, Activity a) {
        try {
            v.requestFocus();
            InputMethodManager imm = (InputMethodManager) a.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
            a.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
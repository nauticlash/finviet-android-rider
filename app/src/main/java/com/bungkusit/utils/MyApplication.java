package com.bungkusit.utils;

import android.support.multidex.MultiDexApplication;
import android.util.Base64;

import com.bungkusit.webservices.api.RestClient;
import com.bungkusit.webservices.interfaces.APIListService;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.google.firebase.messaging.FirebaseMessaging;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import java.net.URISyntaxException;

import io.fabric.sdk.android.Fabric;
import io.socket.client.IO;
import io.socket.client.Socket;


public class MyApplication extends MultiDexApplication {

    private static MyApplication mInstance;
    private Socket mSocket;
    private AppPreferences appPreferences;
    private RestClient retrofitUtil;

    private Bus bus;

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    static {
        System.loadLibrary("native-lib");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());

        FirebaseMessaging.getInstance().subscribeToTopic("rider");
        FirebaseMessaging.getInstance().subscribeToTopic("AllUser");

        mInstance = this;
        try {
            mSocket = IO.socket(AppConstant.NEW_ORDER_SERVER_URL);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        String key = new String(Base64.decode(Base64.decode(stringFromJNI(), Base64.DEFAULT), Base64.DEFAULT));
//        AppConstant.BASE_URL = key;
        AppConstant.BASE_URL = "http://finviet-api.7tech.ai/";
        AppConstant.SOCKET_URL = "http://209.97.163.153:7778";
//        AppConstant.BASE_URL = "http://test.finviet-api.7tech.ai";
//        AppConstant.SOCKET_URL = "http://103.63.212.135:7778";
//        AppConstant.BASE_URL = "http://api.bungkusit.thinhtriumph.com";
//        AppConstant.SOCKET_URL = "http://159.89.201.21:7778";

        retrofitUtil = new RestClient();
        appPreferences = new AppPreferences(MyApplication.this);
        bus = new Bus(ThreadEnforcer.ANY);

    }

    public RestClient getRestClient() {
        return retrofitUtil;
    }

    public APIListService getAPIListService() {
        return retrofitUtil.getAPIListService();
    }

    public AppPreferences getAppPreferences() {
        return appPreferences;
    }

    public Bus getBus() {
        return bus;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    public Socket getSocket() {
        return mSocket;
    }

    public native String stringFromJNI();
}
package com.bungkusit.utils;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public abstract class Timer {
    public static final int DURATION_INFINITY = -1;
    private volatile boolean isRunning = false;
    private long interval;
    private long elapsedTime;
    private long duration;
    private ScheduledExecutorService execService = Executors
            .newSingleThreadScheduledExecutor();
    private Future<?> future = null;

    /**
     * Default constructor which sets the interval to 1000 ms (1s) and the
     * duration to {@link Timer#DURATION_INFINITY}
     */
    public Timer() {
        this(1000, -1);
    }

    /**
     * @param interval The time gap between each tick in millis.
     * @param duration The period in millis for which the timer should run. Set it to {@code Timer#DURATION_INFINITY} if the timer has to run indefinitely.
     */
    public Timer(long interval, long duration) {
        this.interval = interval;
        this.duration = duration;
        this.elapsedTime = 0;
    }

    /**
     * Starts the timer. If the timer was already running, this call is ignored.
     */
    public void start() {
        if (isRunning)
            return;
        isRunning = true;
        future = execService.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                onTick();
                elapsedTime += Timer.this.interval;
                if (duration > 0) {
                    if (elapsedTime >= duration) {
                        onFinish();
                        future.cancel(false);
                    }
                }
            }
        }, 0, this.interval, TimeUnit.MILLISECONDS);
    }

    protected abstract void onFinish();

    /**
     * Paused the timer. If the timer is not running, this call is ignored.
     */
    public void pause() {
        if (!isRunning) return;
        future.cancel(false);
        isRunning = false;
    }
    public void cancel() {
        if (!isRunning) return;
        future.cancel(false);
        isRunning = false;
    }

    /**
     * Resumes the timer if it was paused, else starts the timer.
     */
    public void resume() {
        this.start();
    }

    /**
     * This method is called periodically with the interval set as the delay between subsequent calls.
     */
    protected abstract void onTick();

}
package com.bungkusit.utils;


public class TimeManager extends Timer {
    OnTimeChanger onTimeChanger;
    boolean isPause = false;
    boolean isCancel = false;
    int Type;

    // Time time;
    public TimeManager() {
        super();
    }

    public TimeManager(long interval, long duration, int type) {
        super(interval, duration);
        this.Type = type;
    }

    public void setTimerListner(OnTimeChanger interfaceTimer) {
        this.onTimeChanger = interfaceTimer;
    }

    @Override
    protected void onTick() {
        System.out.println("onTick called!" + Type);
        if (onTimeChanger != null)
            onTimeChanger.onTimerTick(Type);
    }

    @Override
    protected void onFinish() {
        System.out.println("onFinish called! " + Type);
        if (onTimeChanger != null)
            onTimeChanger.onTimerFinish(Type);
    }

    public void pause() {
// if (isPause == false)
        {
// Debug.e("pause inside TimeManager", "called");
            isPause = true;
            super.pause();
            if (onTimeChanger != null)
                onTimeChanger.onTimerPause(Type);
        }
    }

    public void resume() {
//if (isPause)
        {
//Debug.e("resume inside TimeManager", "called");
            isPause = false;
            super.resume();
            if (onTimeChanger != null)
                onTimeChanger.onTimerResume(Type);
        }
    }

    public void cancel() {
        isCancel = true;
        super.cancel();
// cancel();

    }


    public interface OnTimeChanger {
        public void onTimerResume(int type);

        public void onTimerPause(int type);

        public void onTimerFinish(int type);

        public void onTimerTick(int type);
    }

}
package com.bungkusit.utils;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.model.ShareVideo;
import com.facebook.share.model.ShareVideoContent;
import com.facebook.share.widget.ShareDialog;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Set;

public class FacebookManager {

    private Activity activity;
    private CallbackManager callbackManager;

    public FacebookManager(Activity activity) {
        this.activity = activity;
        initFacebook();
    }

    private void initFacebook() {
        FacebookSdk.sdkInitialize(activity.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
    }

    public void login(final FacebookCallback<LoginResult> onLoginListener) {

//        Set<String> permissions = AccessToken.getCurrentAccessToken().getPermissions();


        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("public_profile", "user_friends", "email"));
//        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("public_profile", "user_friends", "email"));
//        LoginManager.getInstance().logInWithPublishPermissions(
//                activity,
//                Arrays.asList("publish_actions"));
        LoginManager.getInstance().registerCallback(callbackManager, onLoginListener);

    }

    public void loginWithPublishPermission(final FacebookCallback<LoginResult> onLoginListener) {

        Set<String> permissions = AccessToken.getCurrentAccessToken().getPermissions();


        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("public_profile", "user_friends", "email"));
//        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("public_profile", "user_friends", "email"));
//        LoginManager.getInstance().logInWithPublishPermissions(
//                activity,
//                Arrays.asList("publish_actions"));
        LoginManager.getInstance().registerCallback(callbackManager, onLoginListener);

    }

    public void login_share() {

        Set<String> permissions = AccessToken.getCurrentAccessToken().getPermissions();
        if (permissions.contains("publish_actions")) {
            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    shareDialog("SnapTask", "Desciption", "www.snaptask.com");
                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onError(FacebookException e) {

                }
            });
        } else {

            LoginManager.getInstance().logInWithPublishPermissions(
                    activity,
                    Arrays.asList("publish_actions"));


        }

//        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("public_profile", "user_friends"));

//        if(AccessToken.getCurrentAccessToken().ha)

//        LoginManager.getInstance().logInWithPublishPermissions(
//                activity_discover,
//                Arrays.asList("publish_actions"));


    }

    public void logout() {
        LoginManager.getInstance().logOut();
    }

    public void shareDialog(String title, String description, String contentURL) {
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle(title)
                    .setContentDescription(description)
                    .setContentUrl(Uri.parse(contentURL))
                    .build();
            ShareDialog shareDialog = new ShareDialog(activity);
            shareDialog.show(linkContent);

        }

    }


    public void shareLinks(final String title, final String imgUrl, final String url, final FacebookCallback<Sharer.Result> onShareListener) {
        Set<String> permissions = AccessToken.getCurrentAccessToken().getPermissions();
        Log.e("FB Pemissions", permissions + "-");
        if (permissions.contains("publish_actions")) {
            Log.e("FB Pemissions", "has publish_permissions-");
            ShareLinkContent content = new ShareLinkContent.Builder()
                    .setContentTitle(title)
                    .setImageUrl(Uri.parse(imgUrl))
                    .setContentUrl(Uri.parse(url))
                    .build();
            ShareApi.share(content, onShareListener);
        } else {
            Log.e("FB Pemissions", "no has publish_permissions-");
            LoginManager.getInstance().logInWithPublishPermissions(
                    activity,
                    Arrays.asList("publish_actions"));
            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {


                    Log.e("on success fb login", loginResult + "-");
                    shareLinks(title, imgUrl, url, onShareListener);
                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onError(FacebookException error) {
                    Log.e("on fail fb login", error + "-");
                    error.printStackTrace();
                }
            });

        }


    }

    public void sharePhoto(String title, Bitmap image, FacebookCallback<Sharer.Result> onShareListener) {


        Set<String> permissions = AccessToken.getCurrentAccessToken().getPermissions();
        if (permissions.contains("publish_actions")) {
            Log.e("permission", "true " + permissions);

            LoginManager.getInstance().logInWithPublishPermissions(
                    activity,
                    Arrays.asList("publish_actions"));
            SharePhoto photo = new SharePhoto.Builder()
                    .setBitmap(image).setCaption(title)
                    .build();
            SharePhotoContent content = new SharePhotoContent.Builder()
                    .addPhoto(photo)
                    .build();
            ShareApi.share(content, onShareListener);
        } else {
            Log.e("permission", "false " + permissions);
        }

    }

    public void appInvite() {


        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("public_profile", "user_friends"));

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

//                        if (loginResult != null && !AppGlobal.isEmpty(loginResult.getAccessToken().getToken())) {
//                            String appLinkUrl, previewImageUrl;
//                            appLinkUrl = "https://fb.me/792182127552013"; /*"https://play.google.com/store/apps/details?id=net.blipped.blipd";*/
//                            previewImageUrl = "https://gust-production.s3.amazonaws.com/uploads/startup/logo_image/601575/SnapTask-Profile-700x700.png";
////                            Toast.makeText(activity, "Login Done.", Toast.LENGTH_LONG).show();
//                            if (AppInviteDialog.canShow()) {
//                                AppInviteContent content = new AppInviteContent.Builder()
//                                        .setApplinkUrl(appLinkUrl)
//                                        .setPreviewImageUrl(previewImageUrl)
//                                        .build();
//                                AppInviteDialog.show(activity, content);
//                            }
//                        } else {
//                            Toast.makeText(activity, "Something went wrong, Please try again.", Toast.LENGTH_LONG).show();
//                        }
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(activity, "on Cancel.", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException e) {
                        Toast.makeText(activity, "On Error." + e.getMessage(), Toast.LENGTH_LONG).show();
                    }

                }

        );


    }

    public void shareVideo(String title, File videoFile, FacebookCallback<Sharer.Result> onShareListener) {
        Uri videoFileUri = Uri.fromFile(videoFile);
        ShareVideo video = new ShareVideo.Builder()
                .setLocalUrl(videoFileUri)
                .build();
        ShareVideoContent content = new ShareVideoContent.Builder()
                .setVideo(video).setContentTitle(title)
                .build();
        ShareApi.share(content, onShareListener);
    }


    public AccessToken getFacebookSession() {
        if (AccessToken.getCurrentAccessToken() == null)
            return null;
        else {
            return AccessToken.getCurrentAccessToken();


        }

    }

    public void resetAccessToken() {
        AccessToken.setCurrentAccessToken(null);
    }

    public boolean hasAccessToken() {

        if (AccessToken.getCurrentAccessToken() == null)
            return false;
        else {
            return !AccessToken.getCurrentAccessToken().isExpired();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    public void getKeyHash() {
        // Add code to print out the key hash
        try {
            PackageInfo info = activity.getPackageManager().getPackageInfo(
                    activity.getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

}
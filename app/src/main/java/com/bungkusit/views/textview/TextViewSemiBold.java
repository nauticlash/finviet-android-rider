package com.bungkusit.views.textview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class TextViewSemiBold extends com.rey.material.widget.TextView {
    public TextViewSemiBold(Context context) {
        super(context);
        setStyle(context);
    }

    public TextViewSemiBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        setStyle(context);
    }

    public TextViewSemiBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setStyle(context);
    }


    private void setStyle(Context context) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/SF-Pro-Display-Semibold.otf");
        setTypeface(typeface);
    }
}

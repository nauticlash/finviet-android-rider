package com.bungkusit.views.textview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class TextViewRegular extends com.rey.material.widget.TextView {
    public TextViewRegular(Context context) {
        super(context);
        setStyle(context);
    }

    public TextViewRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        setStyle(context);
    }

    public TextViewRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setStyle(context);
    }


    private void setStyle(Context context) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/SF-Pro-Display-Regular.otf");
        setTypeface(typeface);
    }
}

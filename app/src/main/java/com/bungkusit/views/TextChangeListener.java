package com.bungkusit.views;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;

import com.bungkusit.utils.AppGlobal;

public class TextChangeListener implements TextWatcher {

    private TextInputLayout inputLayout;

    public TextChangeListener(TextInputLayout inputLayout) {
        this.inputLayout = inputLayout;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        AppGlobal.errorDisable(inputLayout);
    }

    @Override
    public void afterTextChanged(Editable editable) {
        inputLayout.setError("");
    }
}

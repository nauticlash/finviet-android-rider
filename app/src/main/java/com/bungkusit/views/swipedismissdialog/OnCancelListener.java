package com.bungkusit.views.swipedismissdialog;

import android.view.View;

public interface OnCancelListener {
    void onCancel(View view);
}

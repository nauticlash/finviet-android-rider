package com.bungkusit.views.swipedismissdialog;

public enum SwipeDismissDirection {
    LEFT, RIGHT, TOP, BOTTOM
}

package com.bungkusit.views.swipedismissdialog;

import android.view.View;


public interface OnSwipeDismissListener {
    void onSwipeDismiss(View view, SwipeDismissDirection direction);
}

package com.bungkusit.fragment.all_order;


import android.app.Dialog;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.bungkusit.R;
import com.bungkusit.activity.OrderLocationActivity;
import com.bungkusit.activity.main.MainActivity;
import com.bungkusit.activity.orderDetail.OrderDetailActivity;
import com.bungkusit.adapter.NewOrderAdapter;
import com.bungkusit.databinding.FrgHomeBinding;
import com.bungkusit.model.CommonResp;
import com.bungkusit.model.NewOrderInfo;
import com.bungkusit.model.NewOrderResp;
import com.bungkusit.model.NotificationResp;
import com.bungkusit.utils.AppGlobal;
import com.bungkusit.utils.AppPreferences;
import com.bungkusit.utils.MyApplication;
import com.bungkusit.views.textview.TextViewRegular;
import com.bungkusit.webservices.interfaces.OnApiCompleteListener;

import java.util.HashMap;

import io.nlopez.smartlocation.SmartLocation;
import retrofit2.Call;

import static com.bungkusit.utils.AppConstant.REQUEST_CODE_1;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_2;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_3;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_4;

public class NewOrderFragment extends Fragment implements View.OnClickListener, OnApiCompleteListener {

    private FrgHomeBinding binding;
    private MainActivity main;
    private OrderPresenter presenter;
    private static final String ARG_TYPE = "type";
    private String type = "";
    private NewOrderResp orderResp;
    private int position = -1;
    NewOrderAdapter newOrderAdapter;
    private AppPreferences appPreferences;
    public String order_id;

    public static NewOrderFragment newInstance(String type) {
        NewOrderFragment fragment = new NewOrderFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TYPE, type);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type = getArguments().getString(ARG_TYPE);
        appPreferences = MyApplication.getInstance().getAppPreferences();
        MyApplication.getInstance().getBus().register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        main = (MainActivity) getActivity();
        main.toggleActionBarIcon(1);
        main.setTitle(getResources().getString(R.string.home));

        presenter = new OrderPresenterImpl(this);
        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.frg_home, container, false);
        } else {
            main.toggleActionBarIcon(1);
            main.setTitle(getResources().getString(R.string.home));
        }
        return binding.getRoot();
    }

    public void updateLayout() {
        if (!type.equalsIgnoreCase(getString(R.string.completed)) && binding != null) {
            if (appPreferences.isOnline()) {
                if (appPreferences.isAssigned() && type.equalsIgnoreCase(getString(R.string.newf))) {
                    if (binding.rvNewOrder != null) {
                        binding.rvNewOrder.setVisibility(View.GONE);
                    }
                    if (binding.tvEmpty != null) {
                        binding.tvEmpty.setVisibility(View.VISIBLE);
                    }
                    if (binding.tvEmptyOrder != null) {
                        binding.tvEmptyOrder.setText(R.string.no_order_is_assigned);
                    }
                    binding.swipeRefreshLayout.setRefreshing(false);
                } else {
                    if (binding.tvEmptyOrder != null) {
                        binding.tvEmptyOrder.setText(R.string.no_order);
                    }
                    if (binding.rvNewOrder != null) {
                        binding.rvNewOrder.setVisibility(View.VISIBLE);
                    }
                }
                binding.ivEmpty.setImageResource(R.drawable.idea);
            } else {
                binding.ivEmpty.setImageResource(R.drawable.eraser);
                if (binding.tvEmptyOrder != null) {
                    binding.tvEmptyOrder.setText(R.string.is_offline_message);
                }
                if (binding.rvNewOrder != null) {
                    binding.rvNewOrder.setVisibility(View.GONE);
                }
            }
        }
    }

    public void getOrderApi() {
        updateLayout();
        if (!appPreferences.isOnline()) {
            if (!type.equalsIgnoreCase(getString(R.string.completed))) {
                return;
            }
        } else {
            if (appPreferences.isAssigned() && type.equalsIgnoreCase(getString(R.string.newf))) {
                return;
            }
        }
        if (AppGlobal.isNetwork(getActivity())) {

            if (appPreferences.getUserInfo() == null || appPreferences.getUserInfo().userid == null || appPreferences.getToken() == null) {
                return;
            }

            appPreferences = MyApplication.getInstance().getAppPreferences();

            HashMap<String, Object> request = new HashMap<>();

            HashMap<String, Object> auth = new HashMap<>();
            auth.put("id", appPreferences.getUserInfo().userid);
            auth.put("token", appPreferences.getToken());

            HashMap<String, Object> map = new HashMap<>();
            if (type.equalsIgnoreCase(getString(R.string.newf))) {
                map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_get_new_orders));
            } else if (type.equalsIgnoreCase(getString(R.string.assigned))) {
                map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_get_rider_assign_orders));
            } else if (type.equalsIgnoreCase(getString(R.string.completed))) {
                map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_get_complete_orders));
            }

            map.put(getResources().getString(R.string.request), request);
            map.put("auth", auth);

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

            presenter.getNewOrder(requestService, new NewOrderResp(), REQUEST_CODE_1);
        } else if (isAdded())
            AppGlobal.showSnakeBar(getActivity(), binding.rlMain, getString(R.string.msg_no_internet));

    }

    private void callPlaceBid(String orderId) {
        if (AppGlobal.isNetwork(getActivity())) {
            appPreferences = MyApplication.getInstance().getAppPreferences();

            HashMap<String, Object> request = new HashMap<>();
            request.put("orderid", orderId);

            HashMap<String, Object> auth = new HashMap<>();
            auth.put("id", appPreferences.getUserInfo().userid);
            auth.put("token", appPreferences.getToken());

            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_postbid));
            map.put(getResources().getString(R.string.request), request);
            map.put("auth", auth);

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

            presenter.postBid(requestService, new CommonResp(), REQUEST_CODE_2);
        } else
            AppGlobal.showSnakeBar(getActivity(), binding.rlMain, getString(R.string.msg_no_internet));
    }

    private void init() {

        if (appPreferences.isAssigned()) {

        } else {
            binding.llcompleteAssignedOrder.setVisibility(View.GONE);
        }

        binding.swipeRefreshLayout.setOnRefreshListener(
            new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getOrderApi();
                }
            }
        );
        getOrderApi();
    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()) {
            case R.id.tvViewDetail:

                position = Integer.parseInt(view.getTag().toString());

                NewOrderInfo newOrderInfo = newOrderAdapter.getItem(position);

                if (newOrderInfo.riderstatus.equalsIgnoreCase(getActivity().getString(R.string.Complete))) {
                    Bundle bundle = new Bundle();
                    bundle.putString(OrderDetailActivity.EXTRA_ORDER_ID, newOrderInfo.orderid);
                    OrderDetailActivity.startActivity(getActivity(), bundle);
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString(OrderLocationActivity.EXTRA_ORDER_ID, newOrderInfo.orderid);
                    OrderLocationActivity.startActivity(getActivity(), bundle);
                }
                break;

            case R.id.tvPlaceBid:

                if (appPreferences.isOnline()) {
                    position = Integer.parseInt(view.getTag().toString());
                    final String status = orderResp.data.get(position).status;

                    if (status.equalsIgnoreCase("Pending")) {
                        order_id = orderResp.data.get(position).orderid;
                        callPlaceBid(orderResp.data.get(position).orderid);

                    } else {
                        getOrderApi();
                    }
                }
                break;

            case R.id.tvStartOrder:
                position = Integer.parseInt(view.getTag().toString());
                order_id = orderResp.data.get(position).orderid;
                callStartToPickup();
                break;
            case R.id.tvDescription:
                TextViewRegular tvr = (TextViewRegular) view;
                if (tvr.getText().length() > 30) {
                    if (tvr.getLineCount() == 1) {
                        tvr.setSingleLine(false);
                        tvr.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_collapse_small, 0);
                    } else {
                        tvr.setSingleLine(true);
                        tvr.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expanse_small, 0);
                    }
                }
                break;
        }
    }

    @Override
    public void onRequestCall() {

    }

    @Override
    public void onResponseSuccess(Object success, int requestCode) {
        if (requestCode == REQUEST_CODE_1 && success instanceof NewOrderResp) {
            binding.swipeRefreshLayout.setRefreshing(false);
            orderResp = (NewOrderResp) success;
            setAdapter(orderResp);
            if (orderResp.is_assigned.equalsIgnoreCase("1")) {
                binding.llcompleteAssignedOrder.setVisibility(View.VISIBLE);
                appPreferences.setAssigned(true);
            } else {
                binding.llcompleteAssignedOrder.setVisibility(View.GONE);
                appPreferences.setAssigned(false);
                main.setDrawerEnabled(true);
            }
        } else if (requestCode == REQUEST_CODE_2 && success instanceof CommonResp) {
            orderResp.data.get(position).setIsbided("1");
        } else if (requestCode == REQUEST_CODE_3 && success instanceof CommonResp) {
            getOrderApi();
        } else if (requestCode == REQUEST_CODE_4) {
            getOrderApi();
            final Dialog dialog = new Dialog(getActivity());
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.dialog_start_order);
            dialog.setCanceledOnTouchOutside(false);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            lp.copyFrom(window.getAttributes());

            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
            TextView tvSubmit = (TextView) dialog.findViewById(R.id.tvSubmit);
            tvSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    Bundle bundle = new Bundle();
                    bundle.putString(OrderDetailActivity.EXTRA_ORDER_ID, order_id);
                    OrderDetailActivity.startActivity(getActivity(), bundle);
                }
            });
            TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
            tvClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }


    @Override
    public void onResponseFailure(Object error, int requestCode) {
        if (requestCode == REQUEST_CODE_1 && error instanceof NewOrderResp) {
            binding.rvNewOrder.setVisibility(View.GONE);
            binding.tvEmpty.setVisibility(View.VISIBLE);

            if (((NewOrderResp) error).is_assigned.equalsIgnoreCase("1")) {
                binding.llcompleteAssignedOrder.setVisibility(View.VISIBLE);
                appPreferences.setAssigned(true);

            } else {
                binding.llcompleteAssignedOrder.setVisibility(View.GONE);
                appPreferences.setAssigned(false);
                main.setDrawerEnabled(true);
            }
        } else if (error instanceof String) {
            showNoticeDialog(error.toString());
        } else if (error instanceof NotificationResp) {
            AppGlobal.showSnakeBar(getActivity(), binding.rlMain, ((NotificationResp) error).message);
        } else if (error instanceof CommonResp) {
            showNoticeDialog(((CommonResp) error).message);
        }

        binding.swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onRequestCallBack(boolean isAuthenticationFailed) {
        if (isAuthenticationFailed)
            main.setAuthenticationFailed();
    }

    private void setAdapter(NewOrderResp orderResp) {
        if (orderResp != null && orderResp.data != null && orderResp.data.size() != 0) {
            binding.rvNewOrder.setVisibility(View.VISIBLE);
            binding.tvEmpty.setVisibility(View.GONE);
            binding.rvNewOrder.setLayoutManager(new LinearLayoutManager(getActivity()));
            newOrderAdapter = new NewOrderAdapter(orderResp.data, this, type);
            binding.rvNewOrder.setAdapter(newOrderAdapter);
        } else {
            binding.rvNewOrder.setVisibility(View.GONE);
            binding.tvEmpty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (this.isVisible() && isVisibleToUser) {
            init();
        } else if (isVisibleToUser && !this.isVisible()) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (isAdded()) {
                        init();
                    }
                }
            }, 100);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        MyApplication.getInstance().getBus().unregister(this);
    }

    private void callStartToPickup() {
        appPreferences = MyApplication.getInstance().getAppPreferences();

        if (AppGlobal.isNetwork(getActivity())) {

            HashMap<String, Object> request = new HashMap<>();
            request.put("riderid", appPreferences.getUserInfo().userid);
            request.put("orderid", order_id);


            HashMap<String, Object> auth = new HashMap<>();
            auth.put("id", appPreferences.getUserInfo().userid);
            auth.put("token", appPreferences.getToken());

            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_start_pickup));
            map.put(getResources().getString(R.string.request), request);
            map.put("auth", auth);

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

            presenter.postBid(requestService, new CommonResp(), REQUEST_CODE_4);
        } else
            AppGlobal.showSnakeBar(getActivity(), binding.rlMain, getString(R.string.msg_no_internet));

    }
    public void showNoticeDialog(String message) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_notice);
        dialog.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        TextView tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);
        tvMessage.setText(message);
        TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getOrderApi();
            }
        });
        dialog.show();
    }

    public void showAcceptedDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_accept_order);
        dialog.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        TextView tvSubmit = (TextView) dialog.findViewById(R.id.tvSubmit);
        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callStartToPickup();
                dialog.dismiss();
            }
        });
        TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}

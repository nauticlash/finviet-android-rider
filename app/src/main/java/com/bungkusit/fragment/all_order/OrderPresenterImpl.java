package com.bungkusit.fragment.all_order;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.bungkusit.webservices.interfaces.AppInteractor;
import com.bungkusit.webservices.interfaces.AppInteractorImpl;
import com.bungkusit.webservices.interfaces.OnApiCompleteListener;

import retrofit2.Call;

public class OrderPresenterImpl implements OrderPresenter {

    private OnApiCompleteListener onApiCompleteListener;
    private AppInteractor appInteractor;
    private Context mContext;

    public OrderPresenterImpl(Fragment fragment) {
        this.mContext = fragment.getActivity();
        this.onApiCompleteListener = (OnApiCompleteListener) fragment;
        this.appInteractor = new AppInteractorImpl();
    }

    @Override
    public void getNewOrder(Call<String> requestService, Object respObject, int requestCode) {
        appInteractor.doCallAPI(mContext, requestService, respObject, requestCode, onApiCompleteListener);
    }

    @Override
    public void postBid(Call<String> requestService, Object respObject, int requestCode) {
        appInteractor.doCallAPI(mContext, requestService, respObject, requestCode, onApiCompleteListener);
    }


}

package com.bungkusit.fragment.all_order;

import retrofit2.Call;

public interface OrderPresenter {

    void getNewOrder(Call<String> requestService, Object respObject, int requestCode);
    void postBid(Call<String> requestService, Object respObject, int requestCode);
}

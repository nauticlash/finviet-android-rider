package com.bungkusit.activity.orderDetail;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.net.Uri;

import com.bungkusit.BR;
import com.bungkusit.R;
import com.bungkusit.activity.CaptureReceipt;
import com.bungkusit.activity.CaptureSignature;
import com.bungkusit.activity.main.MainActivity;
import com.bungkusit.activity.rating.RatingPresenter;
import com.bungkusit.activity.rating.RatingPresenterImpl;
import com.bungkusit.adapter.OrderItemAdapter;
import com.bungkusit.databinding.ActivityOrderDetailBinding;
import com.bungkusit.fcm.Config;
import com.bungkusit.model.CommonResp;
import com.bungkusit.model.OrderDetailResp;
import com.bungkusit.model.OrderItem;
import com.bungkusit.utils.AppConstant;
import com.bungkusit.utils.AppGlobal;
import com.bungkusit.utils.AppPreferences;
import com.bungkusit.utils.MyApplication;
import com.bungkusit.utils.PermissionUtils;
import com.bungkusit.webservices.interfaces.OnApiCompleteListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import io.nlopez.smartlocation.SmartLocation;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

import static com.bungkusit.activity.main.MainActivity.main;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_1;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_2;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_3;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_4;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_5;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_6;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_7;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_8;
import static com.bungkusit.utils.AppGlobal.isGPSAvailable;
import static com.bungkusit.utils.AppGlobal.locationServicesEnabled;

public class OrderDetailActivity extends AppCompatActivity implements View.OnClickListener,
        OnApiCompleteListener, PermissionUtils.OnPermissionResponse {

    private ActivityOrderDetailBinding binding;

    private OrderDetailPresenter presenter;
    private String orderId = "";
    public static final String EXTRA_ORDER_ID = "orderId";
    RatingPresenter ratingPresenter;

    OrderDetailResp orderDetailResp;
    private AppPreferences appPreferences;
    private PermissionUtils permissionUtils;
    private String signatureUrl;
    String reason = "";
    Integer isReturn = 0;

    public static void startActivity(Activity mContext, Bundle mBundle) {
        Intent intent = new Intent(mContext, OrderDetailActivity.class);
        intent.putExtras(mBundle);
        mContext.startActivity(intent);
        mContext.overridePendingTransition(R.anim.right2middle, R.anim.middle2left);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            if (requestCode == REQUEST_CODE_1) {
                String signaturePath = data.getExtras().getString("signaturePath");

                if (ratingPresenter == null)
                    ratingPresenter = new RatingPresenterImpl(this);

                MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[1];


                if (!signaturePath.contains("http")) {
                    File file = new File(signaturePath);
                    RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);
                    surveyImagesParts[0] = MultipartBody.Part.createFormData("image", file.getName(), surveyBody);
                }


                Call<String> requestService = MyApplication.getInstance().getAPIListService().callUploadProfileAPI(surveyImagesParts[0]);

                ratingPresenter.uploadImages(requestService, new CommonResp(), REQUEST_CODE_7);

            } else if (requestCode == REQUEST_CODE_6) {
                String message = data.getStringExtra("MESSAGE");
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradiant(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(R.drawable.bg_blue);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setStatusBarGradiant(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_order_detail);
        isGPSAvailable = locationServicesEnabled(MyApplication.getInstance());

        if (getIntent().hasExtra(EXTRA_ORDER_ID))
            orderId = getIntent().getStringExtra(EXTRA_ORDER_ID);
        permissionUtils = new PermissionUtils(this);

        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getOrderDetails();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void init() {

        appPreferences = MyApplication.getInstance().getAppPreferences();
        binding.tvPlaceBid.setOnClickListener(this);
        binding.tvStartToPickup.setOnClickListener(this);
        binding.tvCancel.setOnClickListener(this);
        binding.ivClose.setOnClickListener(this);
        binding.tvCancelPickup.setOnClickListener(this);
        binding.tvPickup.setOnClickListener(this);
        binding.tvCancelDropOff.setOnClickListener(this);
        binding.tvDropOff.setOnClickListener(this);
        binding.tvCompleted.setOnClickListener(this);
        binding.ivPickupContact.setOnClickListener(this);
        binding.ivDropOffContact.setOnClickListener(this);
        binding.ivMap.setOnClickListener(this);
        binding.ivDistanceMap.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        isGPSAvailable = locationServicesEnabled(MyApplication.getInstance());
        if (SmartLocation.with(OrderDetailActivity.this).location().state().locationServicesEnabled() || isGPSAvailable) {
            if (v.getId() == R.id.tvPlaceBid) {
                if (SmartLocation.with(getApplicationContext()).location().state().locationServicesEnabled() || isGPSAvailable) {
                    callPlaceBid();
                } else
                    AppGlobal.openLocationDialog(OrderDetailActivity.this);

            } else if (v.getId() == R.id.tvStartToPickup) {
                if (SmartLocation.with(getApplicationContext()).location().state().locationServicesEnabled()|| isGPSAvailable) {
                    callStartToPickup();
                } else
                    AppGlobal.openLocationDialog(OrderDetailActivity.this);
            } else if (v.getId() == R.id.tvCancel) {
                final Dialog dialog = new Dialog(this);
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.dialog_cancel);
                dialog.setCanceledOnTouchOutside(false);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = dialog.getWindow();
                lp.copyFrom(window.getAttributes());

                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
                final TextView tvNotice = (TextView) dialog.findViewById(R.id.tvNotice);
                TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
                tvClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                TextView tvSubmit = (TextView) dialog.findViewById(R.id.tvSubmit);
                tvSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        EditText edit = (EditText) dialog.findViewById(R.id.etCancel);
                        String reason = edit.getText().toString();
                        if (reason.equals("")) {
                            tvNotice.setText("Vui lòng nhập lí do hủy!");
                        } else {
                            callCancelAPI(reason);
                            dialog.dismiss();
                        }
                    }
                });
                dialog.show();
            } else if (v.getId() == R.id.ivClose) {
                onBackPressed();
            } else if (v.getId() == R.id.tvCancelPickup) {
                final Dialog dialog = new Dialog(this);
                reason = "";
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.dialog_cancel_pickup);
                dialog.setCanceledOnTouchOutside(false);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = dialog.getWindow();
                lp.copyFrom(window.getAttributes());

                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
                final InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                final TextView tvNotice = (TextView) dialog.findViewById(R.id.tvNotice);
                final EditText edit = (EditText) dialog.findViewById(R.id.etCancel);
                TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
                tvClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                TextView tvSubmit = (TextView) dialog.findViewById(R.id.tvSubmit);
                tvSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (reason.equalsIgnoreCase("")) {
                            reason = edit.getText().toString();
                        }
                        if (reason.equals("")) {
                            tvNotice.setText("Vui lòng chọn lí do hủy!");
                        } else {
                            dialog.dismiss();
                            callCancelAPI(reason);
                        }
                    }
                });
                final TextView rbStoreNotOpen = (TextView) dialog.findViewById(R.id.rbStoreNotOpen);
                final TextView rbStoreNotFound = (TextView) dialog.findViewById(R.id.rbStoreNotFound);
                final TextView rbOther = (TextView) dialog.findViewById(R.id.rbOther);
                rbStoreNotOpen.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        reason = rbStoreNotOpen.getText().toString();
                        edit.setVisibility(View.GONE);
                        imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
                        rbStoreNotOpen.setTextColor(getResources().getColor(R.color.text_blue));
                        rbStoreNotFound.setTextColor(getResources().getColor(R.color.text));
                        rbOther.setTextColor(getResources().getColor(R.color.text));
                        rbStoreNotOpen.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_check), null, null, null);
                        rbStoreNotFound.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_uncheck), null, null, null);
                        rbOther.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_uncheck), null, null, null);
                    }
                });

                rbStoreNotFound.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        reason = rbStoreNotFound.getText().toString();
                        edit.setVisibility(View.GONE);
                        imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
                        rbStoreNotFound.setTextColor(getResources().getColor(R.color.text_blue));
                        rbStoreNotOpen.setTextColor(getResources().getColor(R.color.text));
                        rbOther.setTextColor(getResources().getColor(R.color.text));
                        rbStoreNotFound.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_check), null, null, null);
                        rbStoreNotOpen.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_uncheck), null, null, null);
                        rbOther.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_uncheck), null, null, null);
                    }
                });

                rbOther.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        reason = "";
                        edit.setVisibility(View.VISIBLE);
                        rbOther.setTextColor(getResources().getColor(R.color.text_blue));
                        rbStoreNotFound.setTextColor(getResources().getColor(R.color.text));
                        rbStoreNotOpen.setTextColor(getResources().getColor(R.color.text));
                        rbOther.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check, 0, 0, 0);
                        rbStoreNotFound.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                        rbStoreNotOpen.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                    }
                });
                dialog.show();
            } else if (v.getId() == R.id.tvPickup) {
                acceptPaymentAPI("repickupdone", "");
            } else if (v.getId() == R.id.tvCancelDropOff) {
                final Dialog dialog = new Dialog(this);
                reason = "";
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.dialog_cancel_dropoff);
                dialog.setCanceledOnTouchOutside(false);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = dialog.getWindow();
                lp.copyFrom(window.getAttributes());

                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
                final InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                final TextView tvNotice = (TextView) dialog.findViewById(R.id.tvNotice);
                TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
                tvClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                TextView tvSubmit = (TextView) dialog.findViewById(R.id.tvSubmit);
                final EditText edit = (EditText) dialog.findViewById(R.id.etCancel);
                tvSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (reason.equalsIgnoreCase("")) {
                            reason = edit.getText().toString();
                        }
                        if (reason.equals("")) {
                            tvNotice.setText("Vui lòng nhập lí do hủy!");
                        } else {
                            dialog.dismiss();
                            callCancelAPI(reason);
                        }
                    }
                });
                /*final TextView rbChangeItem = (TextView) dialog.findViewById(R.id.rbChangeItem);
                final TextView rbCustomerOffline = (TextView) dialog.findViewById(R.id.rbCustomerOffline);
                final TextView rbCustomerAway = (TextView) dialog.findViewById(R.id.rbCustomerAway);
                final TextView rbProductInvalid = (TextView) dialog.findViewById(R.id.rbProductInvalid);
                final TextView rbOther = (TextView) dialog.findViewById(R.id.rbOther);

                rbChangeItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        reason = rbChangeItem.getText().toString();
                        edit.setVisibility(View.GONE);
                        imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
                        rbChangeItem.setTextColor(getResources().getColor(R.color.text_blue));
                        rbOther.setTextColor(getResources().getColor(R.color.text));
                        rbCustomerOffline.setTextColor(getResources().getColor(R.color.text));
                        rbCustomerAway.setTextColor(getResources().getColor(R.color.text));
                        rbProductInvalid.setTextColor(getResources().getColor(R.color.text));
                        rbChangeItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check, 0, 0, 0);
                        rbOther.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                        rbCustomerOffline.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                        rbCustomerAway.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                        rbProductInvalid.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                    }
                });

                rbCustomerOffline.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        reason = rbCustomerOffline.getText().toString();
                        edit.setVisibility(View.GONE);
                        imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
                        rbOther.setTextColor(getResources().getColor(R.color.text));
                        rbChangeItem.setTextColor(getResources().getColor(R.color.text));
                        rbCustomerOffline.setTextColor(getResources().getColor(R.color.text_blue));
                        rbCustomerAway.setTextColor(getResources().getColor(R.color.text));
                        rbProductInvalid.setTextColor(getResources().getColor(R.color.text));
                        rbOther.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                        rbChangeItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                        rbCustomerOffline.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check, 0, 0, 0);
                        rbCustomerAway.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                        rbProductInvalid.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                    }
                });

                rbCustomerAway.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        reason = rbCustomerAway.getText().toString();
                        edit.setVisibility(View.GONE);
                        imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
                        rbOther.setTextColor(getResources().getColor(R.color.text));
                        rbChangeItem.setTextColor(getResources().getColor(R.color.text));
                        rbCustomerOffline.setTextColor(getResources().getColor(R.color.text));
                        rbCustomerAway.setTextColor(getResources().getColor(R.color.text_blue));
                        rbProductInvalid.setTextColor(getResources().getColor(R.color.text));
                        rbOther.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                        rbChangeItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                        rbCustomerOffline.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                        rbCustomerAway.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check, 0, 0, 0);
                        rbProductInvalid.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                    }
                });

                rbProductInvalid.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        reason = rbProductInvalid.getText().toString();
                        edit.setVisibility(View.GONE);
                        imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
                        rbOther.setTextColor(getResources().getColor(R.color.text));
                        rbChangeItem.setTextColor(getResources().getColor(R.color.text));
                        rbCustomerOffline.setTextColor(getResources().getColor(R.color.text));
                        rbCustomerAway.setTextColor(getResources().getColor(R.color.text));
                        rbProductInvalid.setTextColor(getResources().getColor(R.color.text_blue));
                        rbOther.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                        rbChangeItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                        rbCustomerOffline.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                        rbCustomerAway.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                        rbProductInvalid.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check, 0, 0, 0);
                    }
                });

                rbOther.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        reason = "";
                        edit.setVisibility(View.VISIBLE);
                        rbOther.setTextColor(getResources().getColor(R.color.text_blue));
                        rbChangeItem.setTextColor(getResources().getColor(R.color.text));
                        rbCustomerOffline.setTextColor(getResources().getColor(R.color.text));
                        rbCustomerAway.setTextColor(getResources().getColor(R.color.text));
                        rbProductInvalid.setTextColor(getResources().getColor(R.color.text));
                        rbOther.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check, 0, 0, 0);
                        rbChangeItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                        rbCustomerOffline.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                        rbCustomerAway.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                        rbProductInvalid.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                    }
                });*/
                dialog.show();
            } else if (v.getId() == R.id.tvDropOff) {
                final Dialog dialog = new Dialog(this);
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.dialog_order_delivered);
                dialog.setCanceledOnTouchOutside(false);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = dialog.getWindow();
                lp.copyFrom(window.getAttributes());

                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
                TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
                tvClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                final TextView tvSubmit = (TextView) dialog.findViewById(R.id.tvSubmit);
                tvSubmit.getBackground().setAlpha(128);
                tvSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        acceptPaymentAPI("dropoff", "");
                    }
                });
                final TextView rbOrderDelivered = (TextView) dialog.findViewById(R.id.rbOrderDelivered);
                final TextView rbOrderReturning = (TextView) dialog.findViewById(R.id.rbOrderReturning);
                rbOrderDelivered.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        isReturn = 0;
                        tvSubmit.getBackground().setAlpha(255);
                        tvSubmit.setEnabled(true);
                        rbOrderReturning.setTextColor(getResources().getColor(R.color.text));
                        rbOrderDelivered.setTextColor(getResources().getColor(R.color.text_blue));
                        rbOrderDelivered.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check, 0, 0, 0);
                        rbOrderReturning.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                    }
                });
                rbOrderReturning.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        isReturn = 1;
                        tvSubmit.getBackground().setAlpha(255);
                        tvSubmit.setEnabled(true);
                        rbOrderDelivered.setTextColor(getResources().getColor(R.color.text));
                        rbOrderReturning.setTextColor(getResources().getColor(R.color.text_blue));
                        rbOrderReturning.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check, 0, 0, 0);
                        rbOrderDelivered.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0);
                    }
                });
                dialog.show();
            } else if (v.getId() == R.id.tvCompleted) {
                if (orderDetailResp.data.need_image_verify.equals("1") && orderDetailResp.data.image_verify_type.equals("receipt")) {
                    permissionUtils.requestPermissions(PermissionUtils.REQUEST_CAMERA_GALLERY_PERMISSION, 111);
//                    CaptureReceipt.startActivity(this, new Bundle());
                } else {
                    permissionUtils.requestPermissions(PermissionUtils.REQUEST_EXTERNAL_STORAGE, 110);
                }
            } else if (v.getId() == R.id.ivPickupContact) {
                String phone = orderDetailResp.data.merchant_contact;
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(intent);
            } else if (v.getId() == R.id.ivDropOffContact) {
                String phone = orderDetailResp.data.customer_contact;
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(intent);
            } else if (v.getId() == R.id.ivMap) {
                String destinationLat = orderDetailResp.data.plat;
                String destinationLng = orderDetailResp.data.plng;
                if (orderDetailResp.data.isPickup.equals("3")) {
                    destinationLat = orderDetailResp.data.dllat;
                    destinationLng = orderDetailResp.data.dllng;
                }
                Uri gmmIntentUri = Uri.parse("google.navigation:q="+destinationLat+","+destinationLng+"&mode=l");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            } else if (v.getId() == R.id.ivDistanceMap) {
                String destinationLat = orderDetailResp.data.plat;
                String destinationLng = orderDetailResp.data.plng;
                Uri gmmIntentUri = Uri.parse("google.navigation:q="+destinationLat+","+destinationLng+"&mode=l");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);

            }
        }else {
            AppGlobal.openLocationDialog(OrderDetailActivity.this);

        }
    }

    private void callCancelAPI(String reason) {
        appPreferences = MyApplication.getInstance().getAppPreferences();

        if (AppGlobal.isNetwork(getApplicationContext())) {

            if (presenter == null)
                presenter = new OrderDetailPresenterImpl(this);

            HashMap<String, Object> request = new HashMap<>();
            request.put("orderid", orderId);
            request.put("reason", reason);

            HashMap<String, Object> auth = new HashMap<>();
            auth.put("id", appPreferences.getUserInfo().userid);
            auth.put("token", appPreferences.getToken());

            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_cancel_order));
            map.put(getResources().getString(R.string.request), request);
            map.put("auth", auth);

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);
            presenter.getOrderDetail(requestService, new CommonResp(), REQUEST_CODE_4);
        } else
            AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.msg_no_internet));
    }

    private void getOrderDetails() {
        appPreferences = MyApplication.getInstance().getAppPreferences();

        if (AppGlobal.isNetwork(getApplicationContext())) {

            if (presenter == null)
                presenter = new OrderDetailPresenterImpl(this);

            HashMap<String, Object> request = new HashMap<>();
            request.put("orderid", orderId);
            request.put("type", AppConstant.USER_TYPE_DRIVER);

            HashMap<String, Object> auth = new HashMap<>();
            auth.put("id", appPreferences.getUserInfo().userid);
            auth.put("token", appPreferences.getToken());

            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_get_order_details));
            map.put(getResources().getString(R.string.request), request);
            map.put("auth", auth);

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

            presenter.getOrderDetail(requestService, new OrderDetailResp(), REQUEST_CODE_1);
        } else
            AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.msg_no_internet));
    }

    private void callPlaceBid() {
        appPreferences = MyApplication.getInstance().getAppPreferences();

        if (AppGlobal.isNetwork(getApplicationContext())) {

            if (presenter == null)
                presenter = new OrderDetailPresenterImpl(this);

            HashMap<String, Object> request = new HashMap<>();
            request.put("orderid", orderId);

            HashMap<String, Object> auth = new HashMap<>();
            auth.put("id", appPreferences.getUserInfo().userid);
            auth.put("token", appPreferences.getToken());

            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_postbid));
            map.put(getResources().getString(R.string.request), request);
            map.put("auth", auth);

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

            presenter.getOrderDetail(requestService, new CommonResp(), REQUEST_CODE_2);
        } else
            AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.msg_no_internet));

    }

    private void callStartToPickup() {
        appPreferences = MyApplication.getInstance().getAppPreferences();

        if (AppGlobal.isNetwork(getApplicationContext())) {

            if (presenter == null)
                presenter = new OrderDetailPresenterImpl(this);

            HashMap<String, Object> request = new HashMap<>();
            request.put("riderid", orderDetailResp.data.riderId);
            request.put("orderid", orderId);


            HashMap<String, Object> auth = new HashMap<>();
            auth.put("id", appPreferences.getUserInfo().userid);
            auth.put("token", appPreferences.getToken());

            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_start_pickup));
            map.put(getResources().getString(R.string.request), request);
            map.put("auth", auth);

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

            presenter.getOrderDetail(requestService, new CommonResp(), REQUEST_CODE_3);
            finish();
            startActivity(getIntent());
        } else
            AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.msg_no_internet));

    }

    @Override
    public void onRequestCall() {
        binding.avi.smoothToShow();
    }

    @Override
    public void onResponseSuccess(Object success, int requestCode) {
        if (requestCode == REQUEST_CODE_1 && success instanceof OrderDetailResp) {
            orderDetailResp = (OrderDetailResp) success;
            setData(orderDetailResp);
        } else if (requestCode == REQUEST_CODE_2 && success instanceof CommonResp) {
//            CommonResp commonResp = (CommonResp) success;
//            Toast.makeText(this, commonResp.message, Toast.LENGTH_SHORT).show();
            finish();
            startActivity(getIntent());
        } else if (requestCode == REQUEST_CODE_4 && success instanceof CommonResp) {
            //cancel order
            appPreferences.setAssigned(false);
//            Toast.makeText(getApplicationContext(), ((CommonResp) success).message, Toast.LENGTH_LONG).show();
            final Dialog dialog = new Dialog(this);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.dialog_canceled_order);
            dialog.setCanceledOnTouchOutside(false);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            lp.copyFrom(window.getAttributes());

            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
            TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
            tvClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    finish();
                    MainActivity.startActivity(OrderDetailActivity.this, new Bundle());
                }
            });
            dialog.show();
        } else if (requestCode == REQUEST_CODE_7 && success instanceof CommonResp) {
            signatureUrl = ((CommonResp) success).data;
            callGiveRatingAPI();
        } else if (requestCode == REQUEST_CODE_8) {
            appPreferences.setAssigned(false);
            Bundle bundle = new Bundle();
            bundle.putString("order_action", "complete");
            MainActivity.startActivity(OrderDetailActivity.this, bundle);
            finish();
        }
    }

    @Override
    public void onResponseFailure(Object error, int requestCode) {

        if (error instanceof String) {
            AppGlobal.showSnakeBar(this, binding.rlMain, error.toString());
        } else if (error instanceof OrderDetailResp) {
            showNoticeDialog(((OrderDetailResp) error).message);
        } else if (requestCode == REQUEST_CODE_2 && error instanceof CommonResp) {
            AppGlobal.showSnakeBar(this, binding.rlMain, ((CommonResp) error).message);
        } else {
            finish();
//            startActivity(getIntent());
        }

    }

    @Override
    public void onRequestCallBack(boolean isAuthenticationFailed) {
        if (isAuthenticationFailed)
            main.setAuthenticationFailed();

        binding.avi.smoothToHide();
    }

    private void setData(OrderDetailResp orderDetailResp) {
        appPreferences = MyApplication.getInstance().getAppPreferences();

        if (orderDetailResp != null && orderDetailResp.data != null) {
            binding.setVariable(BR.orderInfo, orderDetailResp.data);

            if (!orderDetailResp.data.status.equalsIgnoreCase("Pending")) {

                if (!orderDetailResp.data.riderId.equalsIgnoreCase(appPreferences.getUserInfo().userid)) {
                    if (orderDetailResp.data.status.equalsIgnoreCase("Canceled")) {
                        showNoticeDialog(getResources().getString(R.string.unavailable_order));
                    } else {
                        showNoticeDialog(getResources().getString(R.string.msg_orderAssignedToOtherRider));
                    }
                }
            }
            binding.tvOrderAmountTitle.setText(getResources().getString(R.string.payment_amount));
            binding.tvOrderAmount.setText(orderDetailResp.data.getPickupAmount());
            if (orderDetailResp.data.riderId.equalsIgnoreCase("0")) {
                binding.distanceLayout.setVisibility(View.VISIBLE);
                Typeface face = Typeface.createFromAsset(getAssets(),
                        "fonts/SF-Pro-Display-Regular.otf");
                binding.tvPickupAddressTitle.setTextColor(getResources().getColor(R.color.hint));
                binding.tvPickupAddressTitle.setTypeface(face);
                binding.tvDropOffAddressTitle.setTextColor(getResources().getColor(R.color.hint));
                binding.tvDropOffAddressTitle.setTypeface(face);
                binding.tvDropOffAddress.setTextColor(getResources().getColor(R.color.text));
                binding.tvPickupAddress.setTextColor(getResources().getColor(R.color.text));
                binding.ivPickupContact.setVisibility(View.GONE);
                binding.ivDropOffContact.setVisibility(View.GONE);
            } else {
                binding.tvPickupAddressTitle.setText(orderDetailResp.data.senderName);
                binding.statusLayout.setVisibility(View.VISIBLE);
                binding.isPickupLayout.setVisibility(View.VISIBLE);
                String isPickup = orderDetailResp.data.isPickup;
                if (orderDetailResp.data.status.equalsIgnoreCase("Canceled")) {
                    binding.llBottomSubmit.setVisibility(View.GONE);
                    binding.tvDropOffAddress.setTextColor(getResources().getColor(R.color.hint));
                    binding.tvPickupAddress.setTextColor(getResources().getColor(R.color.hint));
                    if (isPickup.equals("0")) {
                        binding.tvCancelPickupTitle.setVisibility(View.VISIBLE);
                        binding.tvCancelPickupReason.setVisibility(View.VISIBLE);
                        binding.tvCancelPickupReason.setText("Lý do: " + orderDetailResp.data.cancel_reason);
                        binding.tvCancelPickupTime.setText(orderDetailResp.data.getCancelTime());
                        binding.tvCancelPickupTime.setVisibility(View.VISIBLE);
                        binding.ivCancelPickup.setVisibility(View.VISIBLE);
                        binding.vCancelPickup.setVisibility(View.VISIBLE);
                    } else if (isPickup.equals("1")) {
                        binding.tvPickupCancelTitle.setVisibility(View.VISIBLE);
                        binding.tvPickupCancelReason.setVisibility(View.VISIBLE);
                        binding.tvPickupCancelReason.setText("Lý do: " + orderDetailResp.data.cancel_reason);
                        binding.tvPickupTime.setText(orderDetailResp.data.getCancelTime());
                        binding.ivPickupAddress.setImageDrawable(getResources().getDrawable(R.drawable.pickup_red));
                        binding.tvPickupTime.setVisibility(View.VISIBLE);
                    } else if (isPickup.equals("3")) {
                        binding.tvDropOffCancelTitle.setVisibility(View.VISIBLE);
                        binding.tvDropOffCancelReason.setVisibility(View.VISIBLE);
                        binding.tvDropOffCancelReason.setText("Lý do: " + orderDetailResp.data.cancel_reason);
                        binding.tvDropOffTime.setText(orderDetailResp.data.getCancelTime());
                        binding.tvDropOffTime.setVisibility(View.VISIBLE);
                        binding.ivDropOffAddress.setImageDrawable(getResources().getDrawable(R.drawable.dropoff_red));
                    }
                } else {
                    binding.ivDropOffContact.setVisibility(View.VISIBLE);
                    binding.ivPickupContact.setVisibility(View.VISIBLE);
                    if (isPickup.equals("0")) {
                        binding.bottomSubmitLayout.setVisibility(View.VISIBLE);
                    } else {
                        binding.tvCompleted.setVisibility(View.VISIBLE);
                        if (isPickup.equals("1")) {
                            binding.pickupLayout.setVisibility(View.VISIBLE);
                        } else if (isPickup.equals("3")) {
                            binding.tvPickupTime.setText(orderDetailResp.data.getPickupTime());
                            binding.tvPickupTime.setVisibility(View.VISIBLE);
                            binding.dropOffLayout.setVisibility(View.VISIBLE);
                            binding.tvPickupAddress.setTextColor(getResources().getColor(R.color.hint));
                            binding.tvOrderAmountTitle.setText(getResources().getString(R.string.customer_payment_amount));
                            binding.tvOrderAmount.setText(orderDetailResp.data.getDropoffAmount());
                        }
                    }
                    if (orderDetailResp.data.riderstatus.equalsIgnoreCase("Complete")) {
                        binding.llBottomSubmit.setVisibility(View.GONE);
                        binding.vDropOffAddress.setVisibility(View.VISIBLE);
                        binding.tvCompleted.setVisibility(View.GONE);
                        binding.completeLayout.setVisibility(View.VISIBLE);
                    }

                    if (isPickup.equals("0")) {
                        binding.tvDropOffAddress.setTextColor(getResources().getColor(R.color.text));
                        binding.tvPickupAddress.setTextColor(getResources().getColor(R.color.text));
                    }
                }
                if (isPickup.equals("3")) {
                    binding.vPickupAddress.setBackgroundColor(getResources().getColor(R.color.text_green));
                    binding.tvPickupAddressTitle.setTextColor(getResources().getColor(R.color.text_green));
                    binding.ivPickupAddress.setImageResource(R.drawable.pickup_green);
                }
                if (orderDetailResp.data.isDropoff.equals("2")) {
                    binding.dropOffLayout.setVisibility(View.GONE);
                    binding.tvDropOffAddressTitle.setTextColor(getResources().getColor(R.color.text_green));
                    binding.tvDropOffAddress.setTextColor(getResources().getColor(R.color.hint));
                    binding.ivDropOffAddress.setImageResource(R.drawable.dropoff_green);
                    binding.tvDropOffTime.setText(orderDetailResp.data.getDroppOffTime());
                    binding.tvDropOffTime.setVisibility(View.VISIBLE);
                }

            }

            if (!orderDetailResp.data.order_items.equals("")) {
                try {
                    JSONArray orderItems = new JSONArray(orderDetailResp.data.order_items);
                    ArrayList<OrderItem> items = new ArrayList<OrderItem>();
                    for (int i = 0; i < orderItems.length(); i++) {
                        JSONObject jo = (JSONObject) orderItems.get(i);
                        OrderItem item = new OrderItem();
                        item.name = jo.getString("name");
                        item.amount = jo.getInt("amount");
                        item.quantity = jo.getInt("quantity");
                        item.position = i+1;
                        items.add(item);
                    }

                    binding.rvOrderItems.setLayoutManager(new LinearLayoutManager(this));
                    OrderItemAdapter mAdapter = new OrderItemAdapter(items);
                    binding.rvOrderItems.setAdapter(mAdapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (!appPreferences.isOnline()) {
                binding.tvPlaceBid.setVisibility(View.GONE);
                binding.tvStartToPickup.setVisibility(View.GONE);
                binding.llBottomSubmit.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void acceptPaymentAPI(String type, String amount) {

        if (orderDetailResp != null && orderDetailResp.data != null) {

            if (AppGlobal.isNetwork(getApplicationContext())) {
                appPreferences = MyApplication.getInstance().getAppPreferences();
                OrderDetailPresenter re_presenter = new OrderDetailPresenterImpl(this);

                HashMap<String, Object> request = new HashMap<>();

                if (type.equalsIgnoreCase("arrivepickup")) {

                    request.put("riderid", orderDetailResp.data.riderId);
                    request.put("orderid", orderId);

                } else if (type.equalsIgnoreCase("atdropoff")) {

                    request.put("riderid", orderDetailResp.data.riderId);
                    request.put("orderid", orderId);

                } else if (type.equalsIgnoreCase("repickupdone")) {

                    request.put("riderid", orderDetailResp.data.riderId);
                    request.put("orderid", orderId);

                } else if (type.equalsIgnoreCase("reacceptpayment")) {

                    request.put("riderid", orderDetailResp.data.riderId);
                    request.put("orderid", orderId);

                } else if (type.equalsIgnoreCase("dropoff")) {

                    request.put("riderid", orderDetailResp.data.riderId);
                    request.put("orderid", orderId);
                    request.put("is_return", isReturn);

                }

                HashMap<String, Object> auth = new HashMap<>();
                auth.put("id", appPreferences.getUserInfo().userid);
                auth.put("token", appPreferences.getToken());

                if (type.equalsIgnoreCase("repickupdone")) {

                    HashMap<String, Object> map = new HashMap<>();
                    map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_repickupdone));
                    map.put(getResources().getString(R.string.request), request);
                    map.put("auth", auth);

                    Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);
                    re_presenter.getOrderDetail(requestService, new CommonResp(), REQUEST_CODE_5);

                }  else if (type.equalsIgnoreCase("dropoff")) {

                    HashMap<String, Object> map = new HashMap<>();
                    map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_redropoff));
                    map.put(getResources().getString(R.string.request), request);
                    map.put("auth", auth);

                    Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);
                    re_presenter.getOrderDetail(requestService, new CommonResp(), REQUEST_CODE_6);

                }
                finish();
                startActivity(getIntent());

            } else
                AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.msg_no_internet));
        }

    }

    @Override
    public void onPermissionGranted(int requestCode) {
        switch (requestCode) {

            case 110:
                CaptureSignature.startActivity(this, new Bundle());
                break;
            case 111:
                CaptureReceipt.startActivity(this, new Bundle());
                break;

        }
    }

    @Override
    public void onPermissionDenied(int requestCode) {
        switch (requestCode) {

            case 110:
                Toast.makeText(getApplicationContext(), R.string.storagePermissionRequired, Toast.LENGTH_SHORT).show();
                break;

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionUtils.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void callGiveRatingAPI() {

        if (AppGlobal.isNetwork(this)) {

            AppGlobal.hideKeyboard(this);
            AppPreferences appPreferences = MyApplication.getInstance().getAppPreferences();

            if (ratingPresenter == null)
                ratingPresenter = new RatingPresenterImpl(this);

            HashMap<String, Object> request = new HashMap<>();
            HashMap<String, Object> data = new HashMap<>();

            data.put("orderid", orderId);
            data.put("riderid", orderDetailResp.data.riderId);
            data.put("userid", orderDetailResp.data.userid);
            data.put("overallexperience", "5");
            data.put("communication", "5");
            data.put("signature_image", signatureUrl);
            data.put("comments", "");

            request.put("data", data);

            HashMap<String, Object> auth = new HashMap<>();
            auth.put("id", appPreferences.getUserInfo().userid);
            auth.put("token", appPreferences.getToken());

            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.give_rating));
            map.put(getResources().getString(R.string.request), request);
            map.put("auth", auth);

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);
            ratingPresenter.giveRating(requestService, new CommonResp(), REQUEST_CODE_8);
        } else
            AppGlobal.showSnakeBar(this, binding.rlMain, getString(R.string.msg_no_internet));
    }

    public void showNoticeDialog(String message) {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_notice);
        dialog.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        TextView tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);
        tvMessage.setText(message);
        TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
            }
        });
        dialog.show();
    }
}
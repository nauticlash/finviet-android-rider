package com.bungkusit.activity.orderDetail;

import retrofit2.Call;

public interface OrderDetailPresenter {
    void getOrderDetail(Call<String> requestService, Object respObject, int requestCode);
}
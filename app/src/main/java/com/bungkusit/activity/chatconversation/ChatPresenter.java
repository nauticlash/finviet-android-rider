package com.bungkusit.activity.chatconversation;

import retrofit2.Call;

public interface ChatPresenter {

    void getChatlist(Call<String> requestService, Object respObject, int requestCode);

    void getConverstionist(Call<String> requestService, Object respObject, int requestCode);

    void uploadImages(Call<String> requestService, Object respObject, int requestCode);

}

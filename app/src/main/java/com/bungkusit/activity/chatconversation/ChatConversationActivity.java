package com.bungkusit.activity.chatconversation;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.bungkusit.R;
import com.bungkusit.adapter.ChatConversationAdapter;
import com.bungkusit.databinding.ActivityChatBinding;
import com.bungkusit.model.ChatConversationInfo;
import com.bungkusit.model.ChatConversionResp;
import com.bungkusit.model.Notification;
import com.bungkusit.model.UploadImageResponse;
import com.bungkusit.utils.AppGlobal;
import com.bungkusit.utils.AppPreferences;
import com.bungkusit.utils.MyApplication;
import com.bungkusit.utils.TimeManager;
import com.bungkusit.webservices.interfaces.OnApiCompleteListener;
import com.google.gson.Gson;
import com.squareup.otto.Subscribe;
import java.util.HashMap;
import retrofit2.Call;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_1;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_2;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_3;

public class ChatConversationActivity extends AppCompatActivity implements View.OnClickListener, OnApiCompleteListener, TimeManager.OnTimeChanger {

    private ActivityChatBinding binding;

    private ChatPresenter presenter;
    public String order_id;
    public static String to_Id, from_Id, firstname;
    private String OPPONENT_ID;
    private AppPreferences appPreferences;
    private UploadImageResponse settingResp;
    ChatConversationAdapter chatConversationAdapter;

    public static void startActivity(Activity mContext, Bundle mBundle) {

        Intent intent = new Intent(mContext, ChatConversationActivity.class);
        intent.putExtras(mBundle);

        mContext.startActivityForResult(intent, REQUEST_CODE_1);
        mContext.overridePendingTransition(R.anim.right2middle, R.anim.middle2left);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (binding == null) {
            binding = DataBindingUtil.setContentView(this, R.layout.activity_chat);


            if (getIntent().hasExtra("toId")) {

            }
            order_id = getIntent().getStringExtra("order_id");
            to_Id = getIntent().getStringExtra("toId");
            from_Id = getIntent().getStringExtra("fromId");
            firstname = getIntent().getStringExtra("firstname");
        }

        appPreferences = MyApplication.getInstance().getAppPreferences();
        String userid = appPreferences.getUserInfo().userid;

        if (userid.equals(to_Id)) {
            OPPONENT_ID = from_Id;
        }

        if (userid.equals(from_Id)) {
            OPPONENT_ID = to_Id;
        }

        init();


    }

    private void init() {

        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setStackFromEnd(true);
        binding.rvConversationList.setLayoutManager(mLinearLayoutManager);

        chatConversationAdapter = new ChatConversationAdapter(this);
        binding.rvConversationList.setAdapter(chatConversationAdapter);
        binding.ivsendmessage.setOnClickListener(this);
        binding.ivClose.setOnClickListener(this);
        binding.ivPhone.setOnClickListener(this);
        binding.etmessage.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() > 0) {
                    enableSubmitButton();
                } else {
                    disableSubmitButton();
                }
            }

        });
//        binding.etmessage.setOnEditorActionListener(new TextView.OnEditorActionListener()
//        {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId,
//                                          KeyEvent event)
//            {
//                if (actionId == EditorInfo.IME_ACTION_DONE)
//                {
//                    binding.rvConversationList.smoothScrollToPosition(chatConversationAdapter.getItemCount());
////                    if (isValid())
////                        callsendmessageAPI();
//                }
//                return false;
//            }
//        });
        disableSubmitButton();
        callgetconversationAPI();
    }

    public void enableSubmitButton() {
        binding.ivsendmessage.setImageResource(R.drawable.ic_send_enable);
        binding.ivsendmessage.setEnabled(true);
    }

    public void disableSubmitButton() {
        binding.ivsendmessage.setImageResource(R.drawable.ic_send);
        binding.ivsendmessage.setEnabled(false);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()) {
            case R.id.llMain:
                break;
            case R.id.ivsendmessage:
//                binding.etmessage.requestFocus();
                if (isValid())
                    callsendmessageAPI();
                break;
            case R.id.ivClose:
                binding.llChatNotice.setVisibility(View.GONE);
                binding.llNavBar.setBackgroundColor(getResources().getColor(R.color.text_white));
                break;
            case R.id.ivPhone:
                String phone = getResources().getString(R.string.hotline_number);
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(intent);
                break;
        }
    }

    private boolean isValid() {

        settingResp = null;
        if (binding.etmessage.getText().toString().trim().equalsIgnoreCase("")) {
            return false;
        }
        return true;
    }

    private void callgetconversationAPI() {

        if (AppGlobal.isNetwork(this)) {
            // AppGlobal.hideKeyboard(this);

            if (presenter == null)
                presenter = new ChatPresenterImpl(this);


            HashMap<String, Object> data = new HashMap<>();
            data.put("toId", OPPONENT_ID);

            if (chatConversationAdapter.getItemCount() > 0) {

                data.put("mid", chatConversationAdapter.getItem(chatConversationAdapter.getItemCount() - 1).id);

            } else {
                data.put("mid", "");
            }


            HashMap<String, Object> request = new HashMap<>();
            request.put(getResources().getString(R.string.data), data);

            HashMap<String, Object> auth = new HashMap<>();
            auth.put("id", appPreferences.getUserInfo().userid);
            auth.put("token", appPreferences.getToken());

            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_get_conversation));
            map.put(getResources().getString(R.string.request), request);
            map.put("auth", auth);
            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

            presenter.getConverstionist(requestService, new ChatConversionResp(), REQUEST_CODE_1);
        } else
            AppGlobal.showSnakeBar(this, binding.rlMain, getString(R.string.msg_no_internet));
    }


    private void callsendmessageAPI() {

        if (AppGlobal.isNetwork(this)) {
            AppGlobal.showKeyboard(this);

            if (presenter == null)
                presenter = new ChatPresenterImpl(this);


            HashMap<String, Object> data = new HashMap<>();
            data.put("toId", OPPONENT_ID);
            data.put("message", binding.etmessage.getText().toString());


            if (settingResp != null) {
                data.put("attachment", settingResp.getData());
            } else {
                data.put("attachment", "");

            }
            HashMap<String, Object> request = new HashMap<>();
            request.put(getResources().getString(R.string.data), data);


            HashMap<String, Object> auth = new HashMap<>();
            auth.put("id", appPreferences.getUserInfo().userid);
            auth.put("token", appPreferences.getToken());


            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_sendmessage));
            map.put(getResources().getString(R.string.request), request);
            map.put("auth", auth);
            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

            presenter.getConverstionist(requestService, new NotificationInfo(), REQUEST_CODE_2);
        } else
            AppGlobal.showSnakeBar(this, binding.rlMain, getString(R.string.msg_no_internet));
    }

    @Override
    public void onRequestCall() {
//        main.showProgress();
    }

    @Override
    public void onResponseSuccess(Object success, int requestCode) {

        if (requestCode == REQUEST_CODE_1 && success instanceof ChatConversionResp) {

            chatConversationAdapter.appendAll(((ChatConversionResp) success).data);
            binding.rvConversationList.smoothScrollToPosition(chatConversationAdapter.getItemCount());

        } else if (requestCode == REQUEST_CODE_2 && success instanceof NotificationInfo) {
            disableSubmitButton();
            binding.etmessage.setText("");
            sendMessage(((NotificationInfo) success).data);
        } else if (requestCode == REQUEST_CODE_3 && success instanceof UploadImageResponse) {
            settingResp = (UploadImageResponse) success;
            callsendmessageAPI();
        }
    }

    @Override
    public void onResponseFailure(Object error, int requestCode) {

        if (error instanceof String) {
            AppGlobal.showSnakeBar(this, binding.rlMain, error.toString());
        } else if (error instanceof ChatConversionResp) {
            AppGlobal.showSnakeBar(this, binding.rlMain, ((ChatConversionResp) error).message);

        } else if (error instanceof UploadImageResponse) {
            AppGlobal.showSnakeBar(this, binding.rlMain, ((UploadImageResponse) error).getMessage());
        }
    }

    @Override
    public void onRequestCallBack(boolean isAuthenticationFailed) {

    }

    @Override
    public void onTimerResume(int type) {
    }

    @Override
    public void onTimerPause(int type) {

    }

    @Override
    public void onTimerFinish(int type) {

    }

    @Override
    public void onTimerTick(int type) {

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                callgetconversationAPI();
            }
        });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == this.RESULT_OK) {
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        MyApplication.getInstance().getBus().unregister(this);
    }


    @Subscribe
    public void getNewMessage(final String message) {

        if(message.equalsIgnoreCase("update")){
            return;
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Notification notification = new Gson().fromJson(message, Notification.class);
                sendMessage(notification);
            }
        });

    }


    private void sendMessage(Notification notification) {
        ChatConversationInfo chatConversationInfo = new ChatConversationInfo();
        chatConversationInfo.id = notification.id;
        chatConversationInfo.toId = notification.toId;
        chatConversationInfo.fromId = notification.fromId;
        chatConversationInfo.message = notification.message;
        chatConversationInfo.attachmenturl = notification.attachmenturl;
        chatConversationInfo.fromprofile = notification.fromprofile;
        chatConversationInfo.toprofile = notification.toprofilepic;
        chatConversationInfo.attachment = notification.attachment;
        chatConversationInfo.createDate = notification.create_date;
        chatConversationAdapter.add(chatConversationInfo);

//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                binding.rvConversationList.smoothScrollToPosition(chatConversationAdapter.getItemCount());
//            }
//        }, 200);
        binding.rvConversationList.smoothScrollToPosition(chatConversationAdapter.getItemCount());

    }

class NotificationInfo {
    Notification data;
}

}

package com.bungkusit.activity.chatconversation;


import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.bungkusit.R;
import com.bungkusit.activity.main.MainActivity;
import com.bungkusit.adapter.ChatAdapter;
import com.bungkusit.adapter.ChatConversationAdapter;
import com.bungkusit.databinding.ActivityChatNameBinding;
import com.bungkusit.model.ChatConversationInfo;
import com.bungkusit.model.ChatConversionResp;
import com.bungkusit.model.ChatInfo;
import com.bungkusit.model.ChatResp;
import com.bungkusit.model.Notification;
import com.bungkusit.utils.AppGlobal;
import com.bungkusit.utils.AppPreferences;
import com.bungkusit.utils.Debug;
import com.bungkusit.utils.MyApplication;
import com.bungkusit.webservices.interfaces.OnApiCompleteListener;
import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import java.util.HashMap;

import retrofit2.Call;

import static com.bungkusit.utils.AppConstant.REQUEST_CODE_1;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_2;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener, OnApiCompleteListener {

    private ActivityChatNameBinding binding;

    private ChatPresenter presenter;

    private ChatAdapter chatAdapter;
    private AppPreferences appPreferences;
    ChatConversationAdapter chatConversationAdapter;

    MainActivity main;

    public static void startActivity(Activity mContext, Bundle mBundle) {
        Intent intent = new Intent(mContext, ChatActivity.class);
        intent.putExtras(mBundle);
        mContext.startActivity(intent);
        mContext.overridePendingTransition(R.anim.right2middle, R.anim.middle2left);

    }

    private void init() {
        main = MainActivity.getMainActivity();

        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        appPreferences = MyApplication.getInstance().getAppPreferences();

        Bundle bundle = new Bundle();
        bundle.putString("toId", "1");
        bundle.putString("fromId", appPreferences.getUserInfo().userid);
        bundle.putString("firstname", "");
        bundle.putString("order_id", "");
        ChatConversationActivity.startActivity(this, bundle);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setStackFromEnd(true);
//        binding.rvConversationList.setLayoutManager(mLinearLayoutManager);

        chatConversationAdapter = new ChatConversationAdapter(this);
//        binding.rvConversationList.setAdapter(chatConversationAdapter);
        binding.ivClose.setOnClickListener(this);
        binding.ivPhone.setOnClickListener(this);
        binding.ivsendmessage.setOnClickListener(this);

//        AppGlobal.showKeyboardWithFocus(binding.ilEtmessage, ChatActivity.this);
//        binding.etmessage.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
//                    inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
//                }
//            }
//        });


        binding.etmessage.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() > 0) {
                    enableSubmitButton();
                } else {
                    disableSubmitButton();
                }
            }

        });
        disableSubmitButton();
        callgetMessagelistAPI();
    }

    public void enableSubmitButton() {
        binding.ivsendmessage.setImageResource(R.drawable.ic_send_enable);
        binding.ivsendmessage.setEnabled(true);
    }

    public void disableSubmitButton() {
        binding.ivsendmessage.setImageResource(R.drawable.ic_send);
        binding.ivsendmessage.setEnabled(false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (binding == null)
            binding = DataBindingUtil.setContentView(this, R.layout.activity_chat_name);

        init();
    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()) {
            case R.id.llMain:
                int pos = Integer.parseInt(view.getTag().toString());
                ChatInfo chatInfo = chatAdapter.getItem(pos);
                main.setDecreaseMessageCounter(Integer.parseInt(chatInfo.totalunread));

                chatInfo.totalunread = "0";
                chatAdapter.update(pos, chatInfo);

                Bundle bundle = new Bundle();
                bundle.putString("toId", chatInfo.toId);
                bundle.putString("fromId", chatInfo.fromId);
                bundle.putString("firstname", chatInfo.firstname);
                bundle.putString("order_id", chatInfo.order_id);
                ChatConversationActivity.startActivity(this, bundle);

                break;
            case R.id.ivClose:
                binding.llChatNotice.setVisibility(View.GONE);
                break;
            case R.id.ivPhone:
                String phone = getResources().getString(R.string.hotline_number);
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(intent);
                break;
            case R.id.ivsendmessage:
                callsendmessageAPI();
                break;
        }
    }

    private void callgetMessagelistAPI() {

        if (AppGlobal.isNetwork(this)) {
            ChatPresenter presenter = new ChatPresenterImpl(this);
            HashMap<String, Object> data = new HashMap<>();
            data.put("toId", 1);

            if (chatConversationAdapter.getItemCount() > 0) {

                data.put("mid", chatConversationAdapter.getItem(chatConversationAdapter.getItemCount() - 1).id);

            } else {
                data.put("mid", "");
            }


            HashMap<String, Object> request = new HashMap<>();
            request.put(getResources().getString(R.string.data), data);

            HashMap<String, Object> auth = new HashMap<>();
            auth.put("id", appPreferences.getUserInfo().userid);
            auth.put("token", appPreferences.getToken());

            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_get_conversation));
            map.put(getResources().getString(R.string.request), request);
            map.put("auth", auth);
            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

            presenter.getConverstionist(requestService, new ChatConversionResp(), REQUEST_CODE_1);
        } else
            AppGlobal.showSnakeBar(this, binding.llMain, getString(R.string.msg_no_internet));
    }

    @Override
    public void onRequestCall() {
        //show loading progress
    }

    @Override

    public void onResponseSuccess(Object success, int requestCode) {
        if (requestCode == REQUEST_CODE_1 && success instanceof ChatConversionResp) {
            chatConversationAdapter.appendAll(((ChatConversionResp) success).data);
        }else if (requestCode == REQUEST_CODE_2 && success instanceof NotificationInfo) {
            disableSubmitButton();
            binding.etmessage.setText("");
            sendMessage(((NotificationInfo) success).data);
        }
    }

    @Override
    public void onResponseFailure(Object error, int requestCode) {
        if (error instanceof String) {
//            AppGlobal.showSnakeBar(this, binding.llMain, error.toString());
        } else if (error instanceof ChatResp) {
//            AppGlobal.showSnakeBar(this, binding.llMain, ((ChatResp) error).message);
        }
    }

    @Override
    public void onRequestCallBack(boolean isAuthenticationFailed) {
        if (isAuthenticationFailed)
            main.setAuthenticationFailed();
    }

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        MyApplication.getInstance().getBus().unregister(this);
    }


    @Subscribe
    public void getNewMessage(final String message) {

        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Notification notification = new Gson().fromJson(message, Notification.class);

                for (int i = 0; i < chatAdapter.getItemCount(); i++) {
                    ChatInfo chatInfo = chatAdapter.getItem(i);

                    if (chatInfo.fromId.equals(notification.fromId) && chatInfo.toId.equals(notification.toId)) {
                        String totalunread = chatInfo.totalunread;

                        if (totalunread == null || totalunread.length() == 0) {
                            totalunread = "0";
                        }
                        int count = Integer.parseInt(totalunread);

                        count++;

                        chatInfo.totalunread = "" + count;
                        chatAdapter.update(i, chatInfo);
                    }
                }
            }
        });

    }


    private void callsendmessageAPI() {

        if (AppGlobal.isNetwork(this)) {
            AppGlobal.hideKeyboard(this);


            ChatPresenter chat_presenter = new ChatPresenterImpl(this);


            HashMap<String, Object> data = new HashMap<>();
            data.put("toId", 1);
            data.put("message", binding.etmessage.getText().toString());


//            if (settingResp != null) {
//                data.put("attachment", settingResp.getData());
//            } else {
//                data.put("attachment", "");
//
//            }
            HashMap<String, Object> request = new HashMap<>();
            request.put(getResources().getString(R.string.data), data);


            HashMap<String, Object> auth = new HashMap<>();
            auth.put("id", appPreferences.getUserInfo().userid);
            auth.put("token", appPreferences.getToken());


            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_sendmessage));
            map.put(getResources().getString(R.string.request), request);
            map.put("auth", auth);
            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

            chat_presenter.getConverstionist(requestService, new NotificationInfo(), REQUEST_CODE_2);
        }
    }

    private void sendMessage(Notification notification) {
        ChatConversationInfo chatConversationInfo
                = new ChatConversationInfo();

        chatConversationInfo.id = notification.id;
        chatConversationInfo.toId = notification.toId;
        chatConversationInfo.fromId = notification.fromId;
        chatConversationInfo.message = notification.message;
        chatConversationInfo.attachmenturl = notification.attachmenturl;
        chatConversationInfo.fromprofile = notification.fromprofile;
        chatConversationInfo.toprofile = notification.toprofilepic;
        chatConversationInfo.attachment = notification.attachment;
        chatConversationInfo.createDate = notification.create_date;

        chatConversationAdapter.add(chatConversationInfo);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
//                binding.rvConversationList.smoothScrollToPosition(chatConversationAdapter.getItemCount());
            }
        }, 200);

    }

    class NotificationInfo {
        Notification data;
    }

}

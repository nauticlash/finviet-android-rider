package com.bungkusit.activity.register;

import android.app.Activity;

import com.bungkusit.webservices.interfaces.AppInteractor;
import com.bungkusit.webservices.interfaces.AppInteractorImpl;
import com.bungkusit.webservices.interfaces.OnApiCompleteListener;

import retrofit2.Call;

public class RegisterPresenterImpl implements RegisterPresenter {

    private OnApiCompleteListener onApiCompleteListener;
    private AppInteractor appInteractor;
    private Activity mActivity;

    public RegisterPresenterImpl(Activity mActivity) {

        this.mActivity = mActivity;
        this.onApiCompleteListener = (OnApiCompleteListener) mActivity;
        this.appInteractor = new AppInteractorImpl();
    }

    @Override
    public void doCheckEmail(Call<String> requestService, Object respObject, int requestCode) {
        appInteractor.doCallAPI(mActivity, requestService, respObject, requestCode, onApiCompleteListener);
    }
}
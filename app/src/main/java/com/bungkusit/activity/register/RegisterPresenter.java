package com.bungkusit.activity.register;

import retrofit2.Call;

public interface RegisterPresenter {
    void doCheckEmail(Call<String> requestService, Object respObject, int requestCode);
}
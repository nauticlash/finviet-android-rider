package com.bungkusit.activity.register;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Pair;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;

import com.bungkusit.R;
import com.bungkusit.activity.uploadpicture.Uploadpicture;
import com.bungkusit.databinding.ActivityRegisterBinding;
import com.bungkusit.model.CommonResp;
import com.bungkusit.utils.AppConstant;
import com.bungkusit.utils.AppGlobal;
import com.bungkusit.utils.MyApplication;
import com.bungkusit.utils.PermissionUtils;
import com.bungkusit.views.TextChangeListener;
import com.bungkusit.webservices.interfaces.OnApiCompleteListener;
import com.rey.material.app.DatePickerDialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.ThemeManager;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesProvider;
import retrofit2.Call;

import static com.bungkusit.utils.AppConstant.REQUEST_CODE_1;
import static com.bungkusit.utils.AppGlobal.isGPSAvailable;
import static com.bungkusit.utils.AppGlobal.locationServicesEnabled;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener,
        OnApiCompleteListener {

    private ActivityRegisterBinding binding;

    private RegisterPresenter presenter;

    private String fname, lname, email, mobile, dob, address, confirmPassword,password, lat = "", lng = "", country, state, city = "";

    public static boolean isEdit;
    private LocationGooglePlayServicesProvider provider;
    private int nameChar = 0, addressChar = 0, dobChar = 0;

    public static void startActivity(Activity mContext, Bundle mBundle, View[] view) {
        Intent intent = new Intent(mContext, RegisterActivity.class);
        intent.putExtras(mBundle);

        if (view != null && Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {

            Pair<View, String> p1 = Pair.create(view[0], mContext.getString(R.string.transition_email));
            Pair<View, String> p2 = Pair.create(view[1], mContext.getString(R.string.transition_password));
            Pair<View, String> p3 = Pair.create(view[2], mContext.getString(R.string.transition_login));
            Pair<View, String> p4 = Pair.create(view[3], mContext.getString(R.string.transition_dnt));

            ActivityOptions options =
                    ActivityOptions.
                            makeSceneTransitionAnimation(mContext, p1, p2, p3, p4);

            mContext.startActivity(intent, options.toBundle());

        } else {
            mContext.startActivity(intent);
            mContext.overridePendingTransition(R.anim.right2middle, R.anim.middle2left);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        presenter = new RegisterPresenterImpl(this);

        init();
    }

    private void init() {
        binding.ivSmoke.setBackgroundResource(R.drawable.anim_smoke);

        AnimationDrawable animationDrawable = (AnimationDrawable) binding.ivSmoke.getBackground();
        animationDrawable.start();

        binding.tvLogin.setOnClickListener(this);
        binding.tvNext.setOnClickListener(this);
        binding.tvTermsCondition.setOnClickListener(this);
        binding.ivClose.setOnClickListener(this);
        binding.etDOB.setOnClickListener(this);
        binding.etState.setOnClickListener(this);

        binding.etFName.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                nameChar = s.length();
                if (nameChar > 0 && dobChar > 0 && addressChar > 0) {
                    enableSubmitButton();
                } else {
                    disableSubmitButton();
                }
            }
        });
        binding.etLName.addTextChangedListener(new TextChangeListener(binding.ilLName));
        /*binding.etEmail.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                emailChar = s.length();
                if (nameChar > 0 && dobChar > 0 && addressChar > 0) {
                    enableSubmitButton();
                } else {
                    disableSubmitButton();
                }
            }
        });*/
        binding.etAddress.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                addressChar = s.length();
                if (nameChar > 0 && dobChar > 0 && addressChar > 0) {
                    enableSubmitButton();
                } else {
                    disableSubmitButton();
                }
            }
        });
        binding.etMobile.addTextChangedListener(new TextChangeListener(binding.ilMobile));
        binding.etPassword.addTextChangedListener(new TextChangeListener(binding.ilPassword));
        binding.etCity.addTextChangedListener(new TextChangeListener(binding.ilCity));


        binding.etDOB.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                dobChar = s.length();
                if (nameChar > 0 && dobChar > 0 && addressChar > 0) {
                    enableSubmitButton();
                } else {
                    disableSubmitButton();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        disableSubmitButton();
        AppGlobal.showKeyboardWithFocus(binding.etFName, RegisterActivity.this);
    }

    public void enableSubmitButton() {
        binding.tvNext.getBackground().setAlpha(255);
        binding.tvNext.setEnabled(true);
    }

    public void disableSubmitButton() {
        binding.tvNext.getBackground().setAlpha(128);
        binding.tvNext.setEnabled(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ilDOB:
            case R.id.etDOB:
                int day = 1;
                int month = 0;
                int year = 1990;
                // date picker dialog
                android.app.DatePickerDialog picker = new android.app.DatePickerDialog(this,
                        new android.app.DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                binding.etDOB.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker.show();
                break;
            case R.id.etState:
                showStateDialog();
                break;
            case R.id.tvLogin:
                onBackPressed();
                break;
            case R.id.tvNext:
                if (isValid())
                    callUploadPic();
                break;
            case R.id.tvTermsCondition:
                try {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(getString(R.string.terms_condition_link)));
                    startActivity(i);
                } catch (Exception e) {

                }
                break;
            case R.id.ivClose:
                onBackPressed();
                break;
        }
    }

    public void callUploadPic() {
        HashMap<String, Object> data = new HashMap<>();
        data.put(AppConstant.FIRSTNAME, fname);
        data.put(AppConstant.LASTNAME, lname);
        data.put(AppConstant.EMAIL, email);
        data.put(AppConstant.PASSWORD, password);
        data.put(AppConstant.CONTACTNO, mobile);
        data.put(AppConstant.PLATFORM, AppConstant.PLATFORM_AND);
        data.put(AppConstant.USERTYPE, AppConstant.USER_TYPE_DRIVER);
        data.put(AppConstant.LAT, lat);
        data.put(AppConstant.LNG, lng);
        data.put(AppConstant.DOB, AppGlobal.convertDateFormatToDashed(dob));
        data.put(AppConstant.ADDRESS, address);
        data.put(AppConstant.DEVICEID, MyApplication.getInstance().getAppPreferences().getDeviceToken());
        data.put(AppConstant.COUNTRY, country);
        data.put(AppConstant.STATE, state);
        data.put(AppConstant.CITY, city);
        if (!isEdit) {
            Intent i = new Intent(RegisterActivity.this, Uploadpicture.class);
            i.putExtra("map", data);
            startActivity(i);
        } else {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("map", data);

            setResult(RESULT_OK, returnIntent);
            finish();
        }
    }

    private void dialogDatePicker() {
        DatePickerDialog.Builder builder = null;

        boolean isLightTheme = ThemeManager.getInstance().getCurrentTheme() == 0;


        int styleId = isLightTheme ?
                R.style.Material_App_Dialog_DatePicker_Light : R.style.Material_App_Dialog_DatePicker;


        builder = new DatePickerDialog.Builder(styleId) {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                DatePickerDialog dialog = (DatePickerDialog) fragment.getDialog();

                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                String date = dialog.getFormattedDate(format);
                binding.etDOB.setText(date);

                super.onPositiveActionClicked(fragment);
            }

            @Override
            public void onNegativeActionClicked(DialogFragment fragment) {
                super.onNegativeActionClicked(fragment);
            }
        };

        builder.dateRange(1, 1, 1900,
                AppGlobal.getDay(), AppGlobal.getMonth() - 1, AppGlobal.getYear());

        builder.positiveAction("Đồng ý")
                .negativeAction("CANCEL");
        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(getSupportFragmentManager(), null);

    }

    private boolean isValid() {
        fname = binding.etFName.getText().toString().trim();
        lname = binding.etLName.getText().toString().trim();
        email = binding.etEmail.getText().toString().trim();
        mobile = binding.etMobile.getText().toString().trim();
        password = binding.etPassword.getText().toString().trim();
        confirmPassword = binding.etConfirmPassword.getText().toString().trim();
        dob = binding.etDOB.getText().toString().trim();
        address = binding.etAddress.getText().toString().trim();
        country = binding.spCountry.getSelectedItem().toString();
        state = binding.etState.getText().toString().trim();
        city = binding.etCity.getText().toString().trim();

        if (fname.equals("")) {
            binding.etFName.requestFocus();
            binding.ilError.setError(getString(R.string.error_fname));
            return false;
        } else if (!email.equals("") && !AppGlobal.checkEmail(email)) {
            binding.etEmail.requestFocus();
            binding.ilError.setError(getString(R.string.error_email_invalid));
            binding.etEmail.setBackgroundResource(R.drawable.bg_edittext_round_error);
            return false;
        } else if (dob.equals("")) {
            binding.etDOB.requestFocus();
            binding.ilError.setError(getString(R.string.error_dob));
            return false;
        } else if (address.equals("")) {
            binding.etAddress.requestFocus();
            binding.ilError.setError(getString(R.string.error_address));
            binding.etEmail.setBackgroundResource(R.drawable.bg_edittext_round_error);
            return false;
        } else if (state.equals("")) {
//            binding.spState.requestFocus();
            binding.etState.requestFocus();
//            Toast.makeText(getApplicationContext(), R.string.error_address, Toast.LENGTH_LONG).show();
            binding.ilError.setError(getString(R.string.error_address));
            binding.etEmail.setBackgroundResource(R.drawable.bg_edittext_round_error);
            return false;
        }
//        else if (!AppGlobal.isNull(password, binding.rlMain, binding.etPassword, binding.ilPassword, getResources().getString(R.string.error_password))) {
//            return false;
//        } else if (!AppGlobal.isNull(confirmPassword, binding.rlMain, binding.etConfirmPassword, binding.ilConfirmPassword, getResources().getString(R.string.error_confirmPassword))) {
//            return false;
//        } else if (!password.equals(confirmPassword)) {
//            binding.ilConfirmPassword.setError(getString(R.string.confirmPasswordNotMatch));
//        //    binding.ilPassword.setError(getString(R.string.confirmPasswordNotMatch));
//            return false;
//        }
//        else if (lat.length() == 0 || lng.length() == 0 || lat.equals("0.0") || lng.equals("0.0")) {
//            AppGlobal.openLocationDialog(RegisterActivity.this);
//            return false;
//        }
        return true;
    }


    @Override
    public void onRequestCall() {
        binding.avi.smoothToShow();
    }

    @Override
    public void onResponseSuccess(Object success, int requestCode) {
        if (requestCode == REQUEST_CODE_1 && success instanceof CommonResp) {
            callUploadPic();
        }
    }

    @Override
    public void onResponseFailure(Object error, int requestCode) {
        if (error instanceof String) {
            AppGlobal.showSnakeBar(this, binding.rlMain, error.toString());
        } else if (error instanceof CommonResp) {
//            AppGlobal.showSnakeBar(this, binding.rlMain, ((CommonResp) error).message);
            binding.ilError.setError(((CommonResp) error).message);
        }
    }

    @Override
    public void onRequestCallBack(boolean isAuthenticationFailed) {
        binding.avi.smoothToHide();
    }



    private void showStateDialog() {
        String[] myResArray = getResources().getStringArray(R.array.states_array);
        final List<String> stateArrayList = Arrays.asList(myResArray);
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_select_state);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);

        ImageView ivClose = (ImageView) dialog.findViewById(R.id.ivClose);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        ListView listView = (ListView) dialog.findViewById(R.id.lvState);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this,R.layout.spinner_state_item, R.id.tvStateItem, stateArrayList);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                binding.etState.setText(stateArrayList.get(position));
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
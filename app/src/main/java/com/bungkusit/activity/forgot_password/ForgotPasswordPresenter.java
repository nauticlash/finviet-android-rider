package com.bungkusit.activity.forgot_password;

import retrofit2.Call;

public interface ForgotPasswordPresenter {
    void doForgotPassword(Call<String> requestService, Object respObject, int requestCode);
}
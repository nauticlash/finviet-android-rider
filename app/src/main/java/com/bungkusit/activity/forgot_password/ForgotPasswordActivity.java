package com.bungkusit.activity.forgot_password;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Pair;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.bungkusit.R;
import com.bungkusit.databinding.ActivityForgotPasswordBinding;
import com.bungkusit.model.CommonResp;
import com.bungkusit.model.LoginResp;
import com.bungkusit.utils.AppConstant;
import com.bungkusit.utils.AppGlobal;
import com.bungkusit.utils.Debug;
import com.bungkusit.utils.MyApplication;
import com.bungkusit.views.TextChangeListener;
import com.bungkusit.webservices.interfaces.OnApiCompleteListener;

import java.util.HashMap;

import retrofit2.Call;

import static com.bungkusit.utils.AppConstant.REQUEST_CODE_1;

public class ForgotPasswordActivity extends AppCompatActivity implements OnApiCompleteListener, View.OnClickListener {

    private ActivityForgotPasswordBinding binding;


    private ForgotPasswordPresenter presenter;

    private String email;


    public static void startActivityForResult(Activity mContext, Bundle mBundle, View[] view) {
        Intent intent = new Intent(mContext, ForgotPasswordActivity.class);
        intent.putExtras(mBundle);

        if (view != null && Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {

            Pair<View, String> p1 = Pair.create(view[0], mContext.getString(R.string.transition_email));
            Pair<View, String> p2 = Pair.create(view[1], mContext.getString(R.string.transition_forgot));

            ActivityOptions options =
                    ActivityOptions.
                            makeSceneTransitionAnimation(mContext, p1, p2);

            mContext.startActivityForResult(intent, REQUEST_CODE_1, options.toBundle());

        } else {
            mContext.startActivityForResult(intent, REQUEST_CODE_1);
            mContext.overridePendingTransition(R.anim.right2middle, R.anim.middle2left);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);

        init();

    }

    private void init() {
        presenter = new ForgotPasswordPresenterImpl(this);

        binding.ivSmoke.setBackgroundResource(R.drawable.anim_smoke);

        AnimationDrawable animationDrawable = (AnimationDrawable) binding.ivSmoke.getBackground();
        animationDrawable.start();

        binding.tvBack.setOnClickListener(this);
        binding.tvSubmit.setOnClickListener(this);

        binding.etEmail.addTextChangedListener(new TextChangeListener(binding.ilEmail));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvBack:
                finish();
                break;
            case R.id.tvSubmit:
                AppGlobal.hideKeyboard(ForgotPasswordActivity.this);
                if (isValid())
                    callAPI();
                break;
        }
    }

    private boolean isValid() {

        email = binding.etEmail.getText().toString().trim();

        if (!AppGlobal.isNull(email, binding.rlMain, binding.etEmail, binding.ilEmail, getString(R.string.error_email))) {
            return false;
        } else if (!AppGlobal.checkEmail(email)) {
            binding.etEmail.requestFocus();
            binding.ilEmail.setError(getString(R.string.error_email_invalid));
            return false;
        }

        return true;
    }

    private void callAPI() {

        if (AppGlobal.isNetwork(getApplicationContext())) {
            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_forgot_password));

            HashMap<String, Object> request = new HashMap<>();

            request.put("email", email);
            request.put("usertype", AppConstant.USER_TYPE_DRIVER);

            map.put(getResources().getString(R.string.request), request);

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

            presenter.doForgotPassword(requestService, new CommonResp(), REQUEST_CODE_1);
        } else
            AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.msg_no_internet));
    }

    @Override
    public void onResponseSuccess(Object success, int requestCode) {
        if (success instanceof CommonResp) {
            Toast.makeText(getApplicationContext(), ((CommonResp) success).message, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    public void onResponseFailure(Object error, int requestCode) {

        if (error instanceof CommonResp) {
            AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, ((CommonResp) error).message);
        } else if (error instanceof String) {
            AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, error.toString());
        }
    }

    @Override
    public void onRequestCall() {
        binding.avi.smoothToShow();
        binding.rlMain.setEnabled(false);
    }

    @Override
    public void onRequestCallBack(boolean isAuthenticationFailed) {
        binding.avi.smoothToHide();
        binding.rlMain.setEnabled(true);
        clear();
    }

    private void clear() {
        binding.etEmail.setText("");
        binding.etEmail.requestFocus();
    }
}
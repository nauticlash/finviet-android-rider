package com.bungkusit.activity.my_profile;

import retrofit2.Call;

public interface MyProfilePresenter {


    void getProfile(Call<String> requestService, Object respObject, int requestCode);
    void editProfile(Call<String> requestService, Object respObject, int requestCode);
    void uploadImages(Call<String> requestService, Object respObject, int requestCode);
    void changePassword(Call<String> requestService, Object respObject, int requestCode);

}

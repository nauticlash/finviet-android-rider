package com.bungkusit.activity.my_profile;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bungkusit.R;
import com.bungkusit.activity.main.MainPresenter;
import com.bungkusit.activity.main.MainPresenterImpl;
import com.bungkusit.databinding.ActivityMyProfileBinding;
import com.bungkusit.model.ChangeStatusResp;
import com.bungkusit.model.CommonResp;
import com.bungkusit.model.LoginResp;
import com.bungkusit.model.UploadImageResponse;
import com.bungkusit.model.UserInfo;
import com.bungkusit.utils.AppGlobal;
import com.bungkusit.utils.AppPreferences;
import com.bungkusit.utils.MyApplication;
import com.bungkusit.utils.PermissionUtils;
import com.bungkusit.webservices.api.RestClient;
import com.bungkusit.webservices.interfaces.OnApiCompleteListener;
import com.kbeanie.multipicker.api.CacheLocation;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bungkusit.utils.AppConstant.REQUEST_CODE_1;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_2;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_3;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_4;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_5;

public class MyProfileActivity extends AppCompatActivity implements View.OnClickListener, OnApiCompleteListener,
        PermissionUtils.OnPermissionResponse, ImagePickerCallback {

    private ActivityMyProfileBinding binding;
    private MyProfilePresenter presenter;
    private AppPreferences appPreferences;

    private PermissionUtils permissionUtils;
    private CameraImagePicker cameraPicker;
    private ImagePicker imagePicker;

    LoginResp loginResp;

    private String profilePath;

    private String fname, email, mobile, dob, address, profilepic = "", state;
    private UserInfo userInfo;
    private boolean isEdit = false;

    String oldPassword, newPassword, confirmPassword;
    private boolean isUpload = false;
    private float cardElevation = 0;

    public static void startActivity(Activity mContext, Bundle mBundle) {
        Intent intent = new Intent(mContext, MyProfileActivity.class);
        intent.putExtras(mBundle);

        mContext.startActivity(intent);
        mContext.overridePendingTransition(R.anim.right2middle, R.anim.middle2left);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_profile);

        init();
    }

    private void init() {

        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        binding.tvChange.setOnClickListener(this);

        appPreferences = MyApplication.getInstance().getAppPreferences();
        permissionUtils = new PermissionUtils(this);

        callGetUpdateProfileDetailsAPI();

        binding.setUserInfo(appPreferences.getUserInfo());
        binding.setAppPreference(appPreferences);
        binding.setDob(AppGlobal.convertDateToSlashFormat(appPreferences.getUserInfo().dob));
        cardElevation = binding.toolbarBg.getCardElevation();
        binding.toolbarBg.setCardElevation(0);

        binding.ivCamera.setOnClickListener(this);
        binding.etDOB.setOnClickListener(this);
        binding.tvSubmit.setOnClickListener(this);
        binding.etState.setOnClickListener(this);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
    @Override
    public void onBackPressed() {

        if (!userInfo.firstname.equals(binding.etFName.getText().toString()) || !userInfo.state.equals(binding.etState.getText().toString()) || !userInfo.address.equals(binding.etAddress.getText().toString()) || !userInfo.email.equals(binding.etEmail.getText().toString()) || !userInfo.dob.equals(AppGlobal.convertDateFormatToDashed(binding.etDOB.getText().toString()))) {
//        if (isEdit) {
            showQuitDialog();
        } else {
            finish();
//            startActivity(getIntent());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivCamera:
                permissionUtils.requestPermissions(PermissionUtils.REQUEST_CAMERA_GALLERY_PERMISSION, PermissionUtils.REQUEST_CODE_CAMERA_GALLERY_PERMISSION);
                break;
            case R.id.etState:
                showStateDialog();
                break;
            case R.id.etDOB:
                // dialogDatePicker();
                int day = 1;
                int month = 0;
                int year = 1990;
                String birthday = appPreferences.getUserInfo().dob;
                if (!birthday.equals("")) {
                    year = Integer.parseInt(birthday.substring(0, 4));
                    month = Integer.parseInt(birthday.substring(5, 7)) - 1;
                    day = Integer.parseInt(birthday.substring(8));
                }
                // date picker dialog
                DatePickerDialog picker = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            try {
                                SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy");
                                SimpleDateFormat input = new SimpleDateFormat("d/M/yyyy");
                                Date formattedDate = input.parse(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                                binding.etDOB.setText(output.format(formattedDate));
                            } catch (Exception e) {
                                binding.etDOB.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }
                    }, year, month, day);
                picker.show();
                break;
            case R.id.tvSubmit:
                if (isValid()) {
                    uploadImages();
                    callUpdateProfileAPI();
                }
                break;
            case R.id.tvChange:
                if (!appPreferences.isAssigned()) {
                    if (appPreferences.isOnline()) {
//                changePasswordDialog();
                        binding.toolbarTitle.setTextColor(getResources().getColor(R.color.text_bold));
//                        binding.toolbarTitle.setBackground(getResources().getDrawable(R.drawable.bg_white_round_shadow_bottom_small));
                        binding.toolbarBg.setCardElevation(cardElevation);
                        binding.llProfileEdit.setVisibility(View.VISIBLE);
                        binding.etFName.setVisibility(View.VISIBLE);
                        binding.ivCamera.setVisibility(View.VISIBLE);
                        binding.llProfileView.setVisibility(View.GONE);
                        binding.tvFName.setVisibility(View.GONE);
                        binding.tvSubmit.setVisibility(View.VISIBLE);
                        binding.tvChange.setVisibility(View.GONE);
                        binding.rlProfile.setBackgroundColor(getResources().getColor(R.color.text_white));
                        binding.llProfile.setPadding(0, 30, 0, 0);
                        binding.rlMain.setBackgroundColor(getResources().getColor(R.color.text_white));
                        binding.ivOnlineStatus.setVisibility(View.GONE);
                        isEdit = true;
                    } else {
                        showGoOnlineDialog();
                    }
                } else {
                    showIsAssignedDialog();
                }
                break;
        }
    }

    private void callGetUpdateProfileDetailsAPI() {

        AppPreferences appPreferences = MyApplication.getInstance().getAppPreferences();

        if (appPreferences != null && appPreferences.getUserInfo() != null) {

            if (AppGlobal.isNetwork(getApplicationContext())) {

                AppGlobal.hideKeyboard(this);

                if (presenter == null)
                    presenter = new MyProfilePresenterImpl(this);

                HashMap<String, Object> request = new HashMap<>();

                HashMap<String, Object> auth = new HashMap<>();
                auth.put("id", appPreferences.getUserInfo().userid);
                auth.put("token", appPreferences.getToken());

                HashMap<String, Object> map = new HashMap<>();
                map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_get_user_profile));
                map.put(getResources().getString(R.string.request), request);
                map.put("auth", auth);

                Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

                presenter.getProfile(requestService, new LoginResp(), REQUEST_CODE_4);
            } else
                AppGlobal.showSnakeBar(getApplicationContext(), binding.llMain, getString(R.string.msg_no_internet));

        }

    }

    private void callUpdateChangePasswordAPI() {

        AppPreferences appPreferences = MyApplication.getInstance().getAppPreferences();

        if (appPreferences != null && appPreferences.getUserInfo() != null) {

            if (AppGlobal.isNetwork(getApplicationContext())) {

                AppGlobal.hideKeyboard(this);

                if (presenter == null)
                    presenter = new MyProfilePresenterImpl(this);

                HashMap request = new HashMap();

                request.put("oldpassword", oldPassword);
                request.put("newpassword", newPassword);
                request.put("confirmpassword", confirmPassword);

                HashMap auth = new HashMap();
                auth.put("id", appPreferences.getUserInfo().userid);
                auth.put("token", appPreferences.getToken());

                HashMap map = new HashMap();
                map.put(getResources().getString(R.string.service), getResources().getString(R.string.update_change_password));
                map.put(getResources().getString(R.string.request), request);
                map.put("auth", auth);

                Call requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

                presenter.changePassword(requestService, new CommonResp(), REQUEST_CODE_3);
            } else
                AppGlobal.showSnakeBar(getApplicationContext(), binding.llMain, getString(R.string.msg_no_internet));


        }


    }

    private boolean isValid() {
        fname = binding.etFName.getText().toString();
        email = binding.etEmail.getText().toString();
        mobile = binding.etMobile.getText().toString();
        dob = binding.etDOB.getText().toString();
        address = binding.etAddress.getText().toString();
        state = binding.etState.getText().toString();
        if (fname.trim().equals("")) {
            binding.etFName.requestFocus();
            AppGlobal.showSnakeBar(getApplicationContext(), binding.llMain, getResources().getString(R.string.error_fname));
            return false;
        } else if (!email.equals("") && !AppGlobal.checkEmail(email)) {
            binding.etEmail.requestFocus();
            AppGlobal.showSnakeBar(getApplicationContext(), binding.llMain, getResources().getString(R.string.error_email_invalid));
            return false;
        } else if (mobile.trim().equals("")) {
            binding.etMobile.requestFocus();
            AppGlobal.showSnakeBar(getApplicationContext(), binding.llMain, getResources().getString(R.string.error_mobile));
            return false;
        } else if (mobile.length() < 7) {
            binding.etMobile.requestFocus();
            AppGlobal.showSnakeBar(getApplicationContext(), binding.llMain, getResources().getString(R.string.error_mobile_invalid));
            return false;
        } else if (dob.trim().equals("")) {
            binding.etDOB.requestFocus();
            AppGlobal.showSnakeBar(getApplicationContext(), binding.llMain, getResources().getString(R.string.error_dob));
            return false;
        } else if (address.trim().equals("")) {
            binding.etAddress.requestFocus();
            AppGlobal.showSnakeBar(getApplicationContext(), binding.llMain, getResources().getString(R.string.error_address));
            return false;
        }
        return true;
    }


    private void uploadImages() {
        if (profilePath != null) {
            if (AppGlobal.isNetwork(this)) {
                isUpload = true;
                AppGlobal.hideKeyboard(this);

                if (presenter == null)
                    presenter = new MyProfilePresenterImpl(this);

                MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[1];

                if (!profilePath.contains("http")) {
                    File file = new File(profilePath);
                    RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);
                    surveyImagesParts[0] = MultipartBody.Part.createFormData("image", file.getName(), surveyBody);

                    new RestClient().getAPIListService().UploadProfilepic(surveyImagesParts[0]).enqueue(new Callback<UploadImageResponse>() {
                        @Override
                        public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> response) {
                            try {

                                if (response.body().getSuccess().matches("1")) {
                                    profilepic = response.body().getData();

                                    callUpdateProfileImageAPI();
                                } else {
                                    AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, response.body().getMessage());
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, "Error in Uploading Pan Image");
                            }
//                            binding.avi.hide();
                        }

                        @Override
                        public void onFailure(Call<UploadImageResponse> call, Throwable t) {
                            binding.avi.hide();
                            Log.e("lookinside", "failed" + t.toString());
                            t.printStackTrace();
                            Log.e("AddReview", "fail=" + t.getMessage());
                        }
                    });
                }
            } else
                AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.msg_no_internet));
        }
    }

    private void callUpdateProfileImageAPI() {
        AppPreferences appPreferences = MyApplication.getInstance().getAppPreferences();

        if (appPreferences != null && appPreferences.getUserInfo() != null) {

            if (AppGlobal.isNetwork(getApplicationContext())) {

                AppGlobal.hideKeyboard(this);

                if (presenter == null)
                    presenter = new MyProfilePresenterImpl(this);

                HashMap<String, Object> request = new HashMap<>();

                request.put("userid", appPreferences.getUserInfo().userid);
                request.put("profilepic", profilepic);

                HashMap<String, Object> auth = new HashMap<>();
                auth.put("id", appPreferences.getUserInfo().userid);
                auth.put("token", appPreferences.getToken());

                HashMap<String, Object> map = new HashMap<>();
                map.put(getResources().getString(R.string.service), getResources().getString(R.string.update_profile_pic));
                map.put(getResources().getString(R.string.request), request);
                map.put("auth", auth);

                Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

                presenter.editProfile(requestService, new LoginResp(), REQUEST_CODE_2);
            } else {
                isUpload = false;
            }
//                AppGlobal.showSnakeBar(getApplicationContext(), binding.llMain, getString(R.string.msg_no_internet));

        } else {
            isUpload = false;
        }


    }

    private void callUpdateProfileAPI() {

        AppPreferences appPreferences = MyApplication.getInstance().getAppPreferences();

        if (appPreferences != null && appPreferences.getUserInfo() != null) {

            if (AppGlobal.isNetwork(getApplicationContext())) {

                AppGlobal.hideKeyboard(this);

                if (presenter == null)
                    presenter = new MyProfilePresenterImpl(this);

                HashMap<String, Object> request = new HashMap<>();

                request.put("userid", appPreferences.getUserInfo().userid);
                request.put("firstname", fname);
                request.put("address", address);
                request.put("email", email);
                request.put("contactno", mobile);
                request.put("dob", AppGlobal.convertDateFormatToDashed(dob));
                request.put("referralcode", "");
                request.put("state", state);

                HashMap<String, Object> auth = new HashMap<>();
                auth.put("id", appPreferences.getUserInfo().userid);
                auth.put("token", appPreferences.getToken());

                HashMap<String, Object> map = new HashMap<>();
                map.put(getResources().getString(R.string.service), getResources().getString(R.string.update_profile));
                map.put(getResources().getString(R.string.request), request);
                map.put("auth", auth);

                Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

                presenter.editProfile(requestService, new LoginResp(), REQUEST_CODE_1);
            } else
                AppGlobal.showSnakeBar(getApplicationContext(), binding.llMain, getString(R.string.msg_no_internet));

        }

    }


    @Override
    public void onPermissionGranted(int requestCode) {
        switch (requestCode) {
            case PermissionUtils.REQUEST_CODE_CAMERA_GALLERY_PERMISSION:
                selectImage();
                break;
        }
    }

    @Override
    public void onPermissionDenied(int requestCode) {
        switch (requestCode) {
            case PermissionUtils.REQUEST_CODE_LOCATION_PERMISSION:
                AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.locationpermissiondenied));
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionUtils.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void selectImage() {
        final CharSequence[] options = {getResources().getString(R.string.take_a_photo),
                getResources().getString(R.string.choose_from_gallery),
                getResources().getString(R.string.cancel)};

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MyProfileActivity.this);
        builder.setTitle("Chọn ảnh");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(getResources().getString(R.string.take_a_photo))) {
                    takePicture();
                } else if (options[item].equals(getResources().getString(R.string.choose_from_gallery))) {
                    pickImageSingle();
                } else if (options[item].equals(getResources().getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void pickImageSingle() {
        imagePicker = new ImagePicker(this);
        imagePicker.setFolderName("Random");
        imagePicker.setRequestId(1234);
        imagePicker.ensureMaxSize(500, 500);
        imagePicker.shouldGenerateMetadata(true);
        imagePicker.shouldGenerateThumbnails(true);
        imagePicker.setImagePickerCallback(this);
        Bundle bundle = new Bundle();
        bundle.putInt("android.intent.extras.CAMERA_FACING", 1);
        imagePicker.pickImage();
    }

    public void takePicture() {
        cameraPicker = new CameraImagePicker(this);
        cameraPicker.setDebugglable(true);
        cameraPicker.setCacheLocation(CacheLocation.EXTERNAL_STORAGE_APP_DIR);
        cameraPicker.setImagePickerCallback(this);
        cameraPicker.shouldGenerateMetadata(true);
        cameraPicker.shouldGenerateThumbnails(true);
        profilePath = cameraPicker.pickImage();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(this);
                    imagePicker.setImagePickerCallback(this);
                }
                imagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(this);
                    cameraPicker.setImagePickerCallback(this);
                    cameraPicker.reinitialize(profilePath);
                }
                cameraPicker.submit(data);
            }
        }
    }

    @Override
    public void onImagesChosen(List<ChosenImage> images) {
        profilePath = images.get(0).getOriginalPath();
        Uri profileUri = Uri.parse(profilePath);
        binding.ivProfilePic.setImageURI(profileUri);
    }

    @Override
    public void onError(String s) {
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
    }


    @Override
    public void onRequestCall() {
        binding.avi.smoothToShow();
    }

    @Override
    public void onResponseSuccess(Object success, int requestCode) {

        if (requestCode == REQUEST_CODE_1 && success instanceof LoginResp) {
            if (!isUpload) {
                binding.avi.smoothToHide();
                finish();
                startActivity(getIntent());
                AppGlobal.showToast(MyProfileActivity.this, getLayoutInflater(), R.string.update_profile_success_message, AppGlobal.TOAST_LENGTH_SHORT);
                appPreferences.setUserInfo(((LoginResp) success).data);
            }
        } else if (requestCode == REQUEST_CODE_2 && success instanceof LoginResp) {
            binding.avi.smoothToHide();
            loginResp = ((LoginResp) success);
            appPreferences.setUserInfo(((LoginResp) success).data);
            isUpload = false;

            Glide.with(this)
                    .load(loginResp.data.profilepic)
                    .asBitmap()
                    .override(150, 150)
                    .into(binding.ivProfilePic);
            finish();
            startActivity(getIntent());
            AppGlobal.showToast(MyProfileActivity.this, getLayoutInflater(), R.string.update_profile_success_message, AppGlobal.TOAST_LENGTH_SHORT);
            appPreferences.setUserInfo(((LoginResp) success).data);
        } else if (requestCode == REQUEST_CODE_3 && success instanceof CommonResp) {
            binding.avi.smoothToHide();
            Toast.makeText(getApplicationContext(), ((CommonResp) success).message, Toast.LENGTH_SHORT).show();
        } else if (requestCode == REQUEST_CODE_4 && success instanceof LoginResp) {
            binding.avi.smoothToHide();
            userInfo = ((LoginResp) success).data;
            appPreferences.setUserInfo(userInfo);
            if (userInfo.email.equals("")) {
                binding.tvEmail.setAlpha((float) 0.3);
            }
        } else if (requestCode == REQUEST_CODE_5 && success instanceof ChangeStatusResp) {
            binding.avi.smoothToHide();
        }

    }

    @Override
    public void onResponseFailure(Object error, int requestCode) {
        binding.avi.smoothToHide();
        if (requestCode == REQUEST_CODE_2) {
            isUpload = false;
            finish();
            startActivity(getIntent());
        }
        if (error instanceof String) {
            AppGlobal.showSnakeBar(getApplicationContext(), binding.llMain, error.toString());
        } else if (error instanceof CommonResp) {
            AppGlobal.showSnakeBar(getApplicationContext(), binding.llMain, ((CommonResp) error).message);
        }
    }

    @Override
    public void onRequestCallBack(boolean isAuthenticationFailed) {
//        binding.avi.smoothToHide();
        if (isAuthenticationFailed)
            finish();
    }

    private void showQuitDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_update_profile_quit);
        dialog.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        TextView tvBack = (TextView) dialog.findViewById(R.id.tvBack);
        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
                startActivity(getIntent());
            }
        });
        TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void showGoOnlineDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_profile_offline);
        dialog.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        TextView tvGoOnline = (TextView) dialog.findViewById(R.id.tvGoOnline);
        tvGoOnline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                appPreferences.setOnline(true);
                callAPIForChangeStatus(true);
                finish();
                startActivity(getIntent());
            }
        });
        TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void callAPIForChangeStatus(Boolean value) {

        appPreferences = MyApplication.getInstance().getAppPreferences();

        if (appPreferences.getUserInfo() != null) {

            if (AppGlobal.isNetwork(getApplicationContext())) {

                if (appPreferences.isLogin()) {

                    HashMap<String, Object> request = new HashMap<>();
                    if (value) {
                        request.put("status", "online");
                    } else {
                        request.put("status", "offline");
                    }

                    HashMap<String, Object> auth = new HashMap<>();
                    auth.put("id", appPreferences.getUserInfo().userid);
                    auth.put("token", appPreferences.getToken());

                    HashMap<String, Object> map = new HashMap<>();
                    map.put(getResources().getString(R.string.service), getResources().getString(R.string.change_live_status));
                    map.put(getResources().getString(R.string.request), request);
                    map.put("auth", auth);

                    Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

                    MainPresenter mainPresenter = new MainPresenterImpl(this);

                    mainPresenter.changeStatus(requestService, new ChangeStatusResp(), REQUEST_CODE_5);
                }
            } else
                AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.msg_no_internet));

        }

    }

    private void showStateDialog() {
        String[] myResArray = getResources().getStringArray(R.array.states_array);
        final List<String> stateArrayList = Arrays.asList(myResArray);
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_select_state);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        ImageView ivClose = (ImageView) dialog.findViewById(R.id.ivClose);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        ListView listView = (ListView) dialog.findViewById(R.id.lvState);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.spinner_state_item, R.id.tvStateItem, stateArrayList);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                binding.etState.setText(stateArrayList.get(position));
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void showIsAssignedDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_is_assigned);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

}

package com.bungkusit.activity.my_profile;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

import com.bungkusit.webservices.interfaces.AppInteractor;
import com.bungkusit.webservices.interfaces.AppInteractorImpl;
import com.bungkusit.webservices.interfaces.OnApiCompleteListener;

import retrofit2.Call;

public class MyProfilePresenterImpl implements MyProfilePresenter {

    private OnApiCompleteListener onApiCompleteListener;
    private AppInteractor appInteractor;
    private Activity mActivity;

    public MyProfilePresenterImpl(Activity mActivity) {
        this.mActivity = mActivity;
        this.onApiCompleteListener = (OnApiCompleteListener) mActivity;
        this.appInteractor = new AppInteractorImpl();
    }

    @Override
    public void getProfile(Call<String> requestService, Object respObject, int requestCode) {
        appInteractor.doCallAPI(mActivity, requestService, respObject, requestCode, onApiCompleteListener);
    }

    @Override
    public void editProfile(Call<String> requestService, Object respObject, int requestCode) {
        appInteractor.doCallAPI(mActivity, requestService, respObject, requestCode, onApiCompleteListener);
    }

    @Override
    public void uploadImages(Call<String> requestService, Object respObject, int requestCode) {
        appInteractor.doCallAPI(mActivity, requestService, respObject, requestCode, onApiCompleteListener);
    }

    @Override
    public void changePassword(Call<String> requestService, Object respObject, int requestCode) {
        appInteractor.doCallAPI(mActivity, requestService, respObject, requestCode, onApiCompleteListener);
    }

}

package com.bungkusit.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;

import com.bungkusit.R;
import com.bungkusit.activity.login.LoginActivity;
import com.bungkusit.activity.login.LoginPresenter;
import com.bungkusit.activity.login.LoginPresenterImpl;
import com.bungkusit.activity.main.MainActivity;
import com.bungkusit.activity.main.MainPresenter;
import com.bungkusit.activity.main.MainPresenterImpl;
import com.bungkusit.activity.register.RegisterActivity;
import com.bungkusit.activity.uploadpicture.Uploadpicture;
import com.bungkusit.databinding.ActivitySplashBinding;
import com.bungkusit.fcm.Config;
import com.bungkusit.model.CommonResp;
import com.bungkusit.model.LoginResp;
import com.bungkusit.service.LocationService;
import com.bungkusit.utils.AppConstant;
import com.bungkusit.utils.AppGlobal;
import com.bungkusit.utils.AppPreferences;
import com.bungkusit.utils.Debug;
import com.bungkusit.utils.MyApplication;
import com.bungkusit.utils.PermissionUtils;
import com.bungkusit.webservices.interfaces.OnApiCompleteListener;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.HashMap;
import java.util.List;

import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesProvider;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;

import static com.bungkusit.utils.AppConstant.REQUEST_CODE_1;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_5;
import static com.bungkusit.utils.AppGlobal.isGPSAvailable;
import static com.bungkusit.utils.AppGlobal.locationServicesEnabled;


public class SplashActivity extends AppCompatActivity implements View.OnClickListener,
        OnApiCompleteListener, EasyPermissions.PermissionCallbacks {

    private ActivitySplashBinding binding;

    private LoginPresenter presenter;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private PermissionUtils permissionUtils;

    private LocationGooglePlayServicesProvider provider;

    public static String lat = "", lng = "";
    private AlertDialog.Builder alertDialogBuilder;

    Intent mServiceIntent;
    private LocationService mLocationService;

    private static final String[] LOCATION =
            {Manifest.permission.ACCESS_FINE_LOCATION};

    private static final int RC_LOCATION_PERM = 124;
    private static final int RC_LOCATION_PERM1 = 125;
    private AppPreferences appPreferences;
    private int iVersionCode;
    private PackageInfo pInfo;
    private AlertDialog dialog;

    private boolean hasLocationPermissions() {
        return EasyPermissions.hasPermissions(this, LOCATION);
    }

    public static void startActivity(Activity mContext, Bundle mBundle) {
        Intent intent = new Intent(mContext, SplashActivity.class);
        intent.putExtras(mBundle);

        mContext.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        appPreferences = MyApplication.getInstance().getAppPreferences();
        isGPSAvailable = locationServicesEnabled(MyApplication.getInstance());
        //checkVersionAPI();
        mLocationService = new LocationService(getApplicationContext());
        mServiceIntent = new Intent(getApplicationContext(), mLocationService.getClass());

        presenter = new LoginPresenterImpl(this);

        if (MyApplication.getInstance().getAppPreferences().isLogin() && MyApplication.getInstance().getAppPreferences().isVerify()) {

            if (!isMyServiceRunning(mLocationService.getClass())) {
                mServiceIntent.setAction(AppConstant.ACTION.STARTFOREGROUND_ACTION);
                LocationService.IS_SERVICE_RUNNING = true;
                startService(mServiceIntent);
            }

            MainActivity.startActivity(SplashActivity.this, new Bundle());
            finish();
        } else {

            LoginActivity.startActivity(SplashActivity.this, new Bundle());
            finish();
        }

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    String message = intent.getStringExtra("message");
                }
            }
        };

        displayFirebaseRegId();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.tvLogin:
                login();
                break;

            case R.id.tvRegister:
                register();
                break;
        }
    }


    @AfterPermissionGranted(RC_LOCATION_PERM)
    public void login() {

        if (hasLocationPermissions()) {
            // Have permissions, do the thing!
            if (SmartLocation.with(getApplicationContext()).location().state().locationServicesEnabled() || isGPSAvailable) {

                if (!isMyServiceRunning(mLocationService.getClass())) {
                    mServiceIntent.setAction(AppConstant.ACTION.STARTFOREGROUND_ACTION);
                    startService(mServiceIntent);
                }

                LoginActivity.startActivity(SplashActivity.this, new Bundle());
            } else {
                AppGlobal.openLocationDialog(SplashActivity.this, getString(R.string.locationrationlecontinuewithlogin));
            }

        } else {
            // Ask for  permissions
            EasyPermissions.requestPermissions(
                    this,
                    getString(R.string.permission_location_rationale),
                    RC_LOCATION_PERM,
                    LOCATION);
        }

    }

    @AfterPermissionGranted(RC_LOCATION_PERM1)
    public void register() {


        if (hasLocationPermissions()) {
            // Have permissions, do the thing!
            if (SmartLocation.with(getApplicationContext()).location().state().locationServicesEnabled() || isGPSAvailable) {

                if (!isMyServiceRunning(mLocationService.getClass())) {
                    mServiceIntent.setAction(AppConstant.ACTION.STARTFOREGROUND_ACTION);
                    startService(mServiceIntent);
                }

                RegisterActivity.startActivity(SplashActivity.this, new Bundle(), null);
            } else {
                AppGlobal.openLocationDialog(SplashActivity.this, getString(R.string.locationrationlecontinueregistration));
            }

        } else {
            // Ask for both permissions
            EasyPermissions.requestPermissions(
                    this,
                    getString(R.string.permission_location_rationale),
                    RC_LOCATION_PERM1,
                    LOCATION);
        }

    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", "");
        Debug.e("Tag", "Firebase reg id: " + regId);
        MyApplication.getInstance().getAppPreferences().setDeviceToken(regId);

    }

    @Override
    protected void onResume() {
        super.onResume();

        checkVersionAPI();

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));


    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (requestCode == REQUEST_CODE_1) {
                boolean isActClose = data.getExtras().getBoolean("isActClose");

                if (isActClose) {
                    MainActivity.startActivity(SplashActivity.this, new Bundle());
                }
            }
        }

        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {

            if (!hasLocationPermissions()) {

                EasyPermissions.requestPermissions(
                        this,
                        getString(R.string.permission_location_rationale),
                        RC_LOCATION_PERM,
                        LOCATION);
            }
        }
    }


    private void callAPI(HashMap<String, Object> request) {


        if (AppGlobal.isNetwork(getApplicationContext())) {
            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_login));

            map.put(getResources().getString(R.string.request), request);

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

            presenter.doLogin(requestService, new LoginResp(), REQUEST_CODE_1);
        } else
            AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.msg_no_internet));
    }

    private void checkVersionAPI() {

        if (AppGlobal.isNetwork(getApplicationContext())) {

            MainPresenter presenter = new MainPresenterImpl(this);

            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_check_version));

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

            presenter.checkVersion(requestService, new CommonResp(), REQUEST_CODE_5);
        }
    }


    @Override
    public void onResponseSuccess(Object success, int requestCode) {

        if (requestCode == REQUEST_CODE_5 && success instanceof CommonResp) {
            try {

                iVersionCode = Integer.parseInt(((CommonResp) success).riderapp);

                pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);


            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        } else if (success instanceof LoginResp) {
            LoginResp loginResp = (LoginResp) success;
            if (loginResp.data.isVerify.equalsIgnoreCase("0")) {

                AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, loginResp.message);

                if (AppGlobal.isEmpty(loginResp.data.frontPhoto)) {
                    HashMap<String, Object> data = new HashMap<>();
                    data.put(AppConstant.FIRSTNAME, loginResp.data.firstname);
                    data.put(AppConstant.LASTNAME, loginResp.data.lastname);
                    data.put(AppConstant.EMAIL, loginResp.data.email);
                    data.put(AppConstant.PASSWORD, loginResp.data.password);
                    data.put(AppConstant.CONTACTNO, loginResp.data.contactno);
                    data.put(AppConstant.PLATFORM, AppConstant.PLATFORM_AND);
                    data.put(AppConstant.USERTYPE, AppConstant.USER_TYPE_DRIVER);
                    data.put(AppConstant.LAT, loginResp.data.lat);
                    data.put(AppConstant.LNG, loginResp.data.lng);
                    data.put(AppConstant.DOB, loginResp.data.dob);
                    data.put(AppConstant.ADDRESS, loginResp.data.address);
                    data.put(AppConstant.DEVICEID, MyApplication.getInstance().getAppPreferences().getDeviceToken());

                    Intent i = new Intent(SplashActivity.this, Uploadpicture.class);
                    i.putExtra("map", data);
                    startActivity(i);
                }
                return;
            }

            AppPreferences appPreferences = MyApplication.getInstance().getAppPreferences();
            appPreferences.setLogin(true);
            appPreferences.setUserInfo(loginResp.data);
            appPreferences.setToken(loginResp.token);
            appPreferences.setSecretLogId(loginResp.secret_log_id);

            MainActivity.startActivity(SplashActivity.this, new Bundle());
        }
    }

    @Override
    public void onResponseFailure(Object error, int requestCode) {

        if (error instanceof LoginResp) {
            AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, ((LoginResp) error).message);
        } else if (error instanceof String) {
            AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, error.toString());
        }
    }

    @Override
    public void onRequestCall() {

        binding.rlMain.setEnabled(false);
    }

    @Override
    public void onRequestCallBack(boolean isAuthenticationFailed) {
        binding.rlMain.setEnabled(true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        Debug.e("SplashActivity", "onPermissionsGranted:" + requestCode + ":" + perms.size());
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        Debug.e("SplashActivity", "onPermissionsDenied:" + requestCode + ":" + perms.size());

        // (Optional) Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
        // This will display a dialog directing them to enable the permission in app settings.
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {

        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Debug.e("isMyServiceRunning?", true + "");
                return true;
            }
        }

        return false;
    }

    @Override
    protected void onDestroy() {
        stopService(mServiceIntent);

        super.onDestroy();

    }


}

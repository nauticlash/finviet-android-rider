package com.bungkusit.activity.login;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.bungkusit.R;
import com.bungkusit.activity.main.MainActivity;
import com.bungkusit.activity.register.RegisterActivity;
import com.bungkusit.activity.uploadpicture.Uploadpicture;
import com.bungkusit.databinding.ActivityLoginBinding;
import com.bungkusit.fcm.Config;
import com.bungkusit.model.LoginResp;
import com.bungkusit.service.LocationService;
import com.bungkusit.utils.AppConstant;
import com.bungkusit.utils.AppGlobal;
import com.bungkusit.utils.AppPreferences;
import com.bungkusit.utils.Debug;
import com.bungkusit.utils.MyApplication;
import com.bungkusit.webservices.interfaces.OnApiCompleteListener;
import com.google.firebase.messaging.FirebaseMessaging;
import android.text.TextWatcher;
import android.text.Editable;

import java.util.HashMap;

import retrofit2.Call;

import static com.bungkusit.utils.AppConstant.REQUEST_CODE_1;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_2;

public class LoginActivity extends AppCompatActivity implements OnApiCompleteListener, View.OnClickListener {

    private ActivityLoginBinding binding;

    private LoginPresenter presenter;
    private LocationService mLocationService;
    public static final String EXTRA_IS_KICKED_OUT = "isKickedOut";
    Intent mServiceIntent;

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private String email, password;
    private int phoneChar = 0, passwordChar = 0;

    public static void startActivity(Activity mContext, Bundle mBundle) {
        Intent intent = new Intent(mContext, LoginActivity.class);
        intent.putExtras(mBundle);

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivityForResult(intent, REQUEST_CODE_1);
        mContext.overridePendingTransition(R.anim.right2middle, R.anim.middle2left);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLocationService = new LocationService(getApplicationContext());
        mServiceIntent = new Intent(getApplicationContext(), mLocationService.getClass());
        if (MyApplication.getInstance().getAppPreferences().isLogin() && MyApplication.getInstance().getAppPreferences().isVerify()) {

            if (!isMyServiceRunning(mLocationService.getClass())) {
                mServiceIntent.setAction(AppConstant.ACTION.STARTFOREGROUND_ACTION);
                LocationService.IS_SERVICE_RUNNING = true;
                startService(mServiceIntent);
            }

            MainActivity.startActivity(LoginActivity.this, new Bundle());
            finish();
        }
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        init();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    String message = intent.getStringExtra("message");
                }
            }
        };

        displayFirebaseRegId();


        if (getIntent().hasExtra(EXTRA_IS_KICKED_OUT)) {
            final Dialog dialog = new Dialog(this);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.alert_login_other);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setCancelable(true);
            TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            lp.copyFrom(window.getAttributes());

            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
            tvClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", "");
        Debug.e("Tag", "Firebase reg id: " + regId);
        MyApplication.getInstance().getAppPreferences().setDeviceToken(regId);

    }

    private void init() {

        clear();
        presenter = new LoginPresenterImpl(this);


        binding.tvLogin.setOnClickListener(this);
        binding.tvRegister.setOnClickListener(this);
        binding.tvForgotPassword.setOnClickListener(this);
      //  binding.ivFacebook.setOnClickListener(this);


        binding.etEmail.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                phoneChar = s.length();
                if (phoneChar > 0 && passwordChar > 0) {
                    enableSubmitButton();
                } else {
                    disableSubmitButton();
                }
            }

        });
        binding.etPassword.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                passwordChar = s.length();
                if (phoneChar > 0 && passwordChar > 0) {
                    enableSubmitButton();
                } else {
                    disableSubmitButton();
                }
            }
        });
        disableSubmitButton();
    }

    public void enableSubmitButton() {
        binding.tvLogin.getBackground().setAlpha(255);
        binding.tvLogin.setEnabled(true);
    }

    public void disableSubmitButton() {
        binding.tvLogin.getBackground().setAlpha(128);
        binding.tvLogin.setEnabled(false);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvLogin:
                AppGlobal.hideKeyboard(LoginActivity.this);
                if (isValid())
                    callAPI();
                break;
            case R.id.tvRegister:
                View[] view = new View[4];
                view[0] = binding.ilError;
                view[1] = binding.ilError;
                view[2] = binding.tvLogin;
                view[3] = binding.tvRegister;
                RegisterActivity.startActivity(LoginActivity.this, new Bundle(), view);
                break;
            case R.id.tvForgotPassword:
//                view = new View[4];
//                view[0] = binding.ilError;
//                view[1] = binding.tvForgotPassword;
//                ForgotPasswordActivity.startActivityForResult(LoginActivity.this, new Bundle(), view);
                String phone = getResources().getString(R.string.hotline_number);
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(intent);
                break;
        }
    }

    private boolean isValid() {

        email = binding.etEmail.getText().toString().trim();
        password = binding.etPassword.getText().toString().trim();

        if (!AppGlobal.isNull(email, binding.rlMain, binding.etEmail, binding.ilError, getString(R.string.error_mobile))) {
            binding.ilError.setVisibility(View.VISIBLE);
            return false;
//        } else if (!AppGlobal.checkEmail(email)) {
//            binding.etEmail.requestFocus();
//            binding.ilError.setError(getString(R.string.error_mobile_invalid));
//            binding.etEmail.setBackgroundColor(getResources().getColor(R.color.input_background_error));
//            return false;
        } else if (!AppGlobal.isNull(password, binding.rlMain, binding.etPassword, binding.ilError, getResources().getString(R.string.error_password))) {
            binding.ilError.setVisibility(View.VISIBLE);
            return false;
        }

        return true;
    }

    private void callAPI() {


        if (AppGlobal.isNetwork(getApplicationContext())) {
            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_login));

            HashMap<String, Object> request = new HashMap<>();

            request.put("email", email);
            request.put("password", password);
            request.put("usertype", AppConstant.USER_TYPE_DRIVER);
            request.put("platform", AppConstant.PLATFORM_AND);
            request.put("deviceid", MyApplication.getInstance().getAppPreferences().getDeviceToken());
            request.put("fbid", "");
            request.put("firstname", "");

            map.put(getResources().getString(R.string.request), request);

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);
//            Call<String> requestService = MyApplication.getInstance().getAPIListService().callRiderAPI(map, getResources().getString(R.string.service_login));

            presenter.doLogin(requestService, new LoginResp(), REQUEST_CODE_1);
        } else
            AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.msg_no_internet));
    }

    private void callAPI(HashMap<String, Object> request) {


        if (AppGlobal.isNetwork(getApplicationContext())) {
            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_login));

            map.put(getResources().getString(R.string.request), request);

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

            presenter.doLogin(requestService, new LoginResp(), REQUEST_CODE_2);
        } else
            AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.msg_no_internet));
    }

    @Override
    public void onResponseSuccess(Object success, int requestCode) {

        if (requestCode == REQUEST_CODE_1 && success instanceof LoginResp) {
            LoginResp loginResp = (LoginResp) success;


            FirebaseMessaging.getInstance().subscribeToTopic("rider_"+loginResp.data.userid);

            if (loginResp.data.isVerify.equalsIgnoreCase("0")) {
//                AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, loginResp.message);
                binding.ilError.setVisibility(View.VISIBLE);
                binding.ilError.setError(loginResp.message);
                AppPreferences appPreferences = MyApplication.getInstance().getAppPreferences();
                appPreferences.setVerify(false);
                return;
            }
            AppPreferences appPreferences = MyApplication.getInstance().getAppPreferences();
            appPreferences.setLogin(true);
            appPreferences.setVerify(true);
            appPreferences.setUserInfo(loginResp.data);
            appPreferences.setToken(loginResp.token);
            appPreferences.setSecretLogId(loginResp.secret_log_id);
            appPreferences.setNotificationBedgeInc(Integer.parseInt(loginResp.data.newnotification));

            if(loginResp.data.onlineStatus.equals("online")){
                appPreferences.setOnline(true);
            } else {
                appPreferences.setOnline(false);
            }

//            Intent returnIntent = new Intent();
//            returnIntent.putExtra("isActClose", true);
//            setResult(RESULT_OK, returnIntent);
//            finish();

            MainActivity.startActivity(LoginActivity.this, new Bundle());

        } else  if (requestCode == REQUEST_CODE_2 && success instanceof LoginResp) {

            LoginResp loginResp = (LoginResp) success;

            FirebaseMessaging.getInstance().subscribeToTopic("rider_"+loginResp.data.userid);

            if (loginResp.data.isVerify.equalsIgnoreCase("0")) {
                AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, loginResp.message);
                if (AppGlobal.isEmpty(loginResp.data.frontPhoto)) {
                    HashMap<String, Object> data = new HashMap<>();
                    data.put(AppConstant.FIRSTNAME, loginResp.data.firstname);
                    data.put(AppConstant.LASTNAME, loginResp.data.lastname);
                    data.put(AppConstant.EMAIL, loginResp.data.email);
                    data.put(AppConstant.PASSWORD, loginResp.data.password);
                    data.put(AppConstant.CONTACTNO, loginResp.data.contactno);
                    data.put(AppConstant.PLATFORM, AppConstant.PLATFORM_AND);
                    data.put(AppConstant.USERTYPE, AppConstant.USER_TYPE_DRIVER);
                    data.put(AppConstant.LAT, loginResp.data.lat);
                    data.put(AppConstant.LNG, loginResp.data.lng);
                    data.put(AppConstant.DOB, loginResp.data.dob);
                    data.put(AppConstant.ADDRESS, loginResp.data.address);
                    data.put(AppConstant.DEVICEID, MyApplication.getInstance().getAppPreferences().getDeviceToken());

                    Intent i = new Intent(LoginActivity.this, Uploadpicture.class);
                    i.putExtra("map", data);
                    startActivity(i);
                }
                return;
            }
            AppPreferences appPreferences = MyApplication.getInstance().getAppPreferences();
            appPreferences.setLogin(true);
            appPreferences.setUserInfo(loginResp.data);
            appPreferences.setToken(loginResp.token);
            appPreferences.setSecretLogId(loginResp.secret_log_id);

            MainActivity.startActivity(LoginActivity.this, new Bundle());
        }

    }

    @Override
    public void onResponseFailure(Object error, int requestCode) {

        if (error instanceof LoginResp) {
            //AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, ((LoginResp) error).message);
//            Toast.makeText(LoginActivity.this,((LoginResp) error).message,Toast.LENGTH_LONG).show();
            binding.ilError.setVisibility(View.VISIBLE);
            binding.ilError.setError(((LoginResp) error).message);
            binding.etEmail.setBackgroundResource(R.drawable.bg_edittext_round_error);
            binding.etPassword.setBackgroundResource(R.drawable.bg_edittext_round_error);
//            AppGlobal.displayAlertDilog(LoginActivity.this,((LoginResp) error).message);
        } else if (error instanceof String) {
            AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, error.toString());
        }
    }

    @Override
    public void onRequestCall() {
        binding.avi.smoothToShow();
        binding.rlMain.setEnabled(false);
    }

    @Override
    public void onRequestCallBack(boolean isAuthenticationFailed) {
        binding.avi.smoothToHide();
        binding.rlMain.setEnabled(true);
        clear();
    }

    private void clear() {
//        binding.etEmail.setText("");
        binding.etPassword.setText("");
        AppGlobal.showKeyboardWithFocus(binding.etEmail, LoginActivity.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_1) {
                boolean isActClose = data.getExtras().getBoolean("isActClose");
                if (isActClose) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("isActClose", isActClose);
                    setResult(RESULT_OK, returnIntent);
                    finish();
                }
            }
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {

        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Debug.e("isMyServiceRunning?", true + "");
                return true;
            }
        }

        return false;
    }
}
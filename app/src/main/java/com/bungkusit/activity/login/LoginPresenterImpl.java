package com.bungkusit.activity.login;

import android.app.Activity;

import com.bungkusit.webservices.interfaces.AppInteractor;
import com.bungkusit.webservices.interfaces.AppInteractorImpl;
import com.bungkusit.webservices.interfaces.OnApiCompleteListener;

import retrofit2.Call;

public class LoginPresenterImpl implements LoginPresenter {

    private OnApiCompleteListener onApiCompleteListener;
    private AppInteractor appInteractor;
    private Activity mActivity;

    public LoginPresenterImpl(Activity mActivity) {

        this.mActivity = mActivity;
        this.onApiCompleteListener = (OnApiCompleteListener) mActivity;
        this.appInteractor = new AppInteractorImpl();
    }

    @Override
    public void doLogin(Call<String> requestService, Object respObject, int requestCode) {
        appInteractor.doCallAPI(mActivity, requestService, respObject, requestCode, onApiCompleteListener);
    }

    @Override
    public void changeStatus(Call<String> requestService, Object respObject, int requestCode) {
        appInteractor.doCallAPI(mActivity, requestService, respObject, requestCode, onApiCompleteListener);
    }
}
package com.bungkusit.activity.login;

import retrofit2.Call;

public interface LoginPresenter {
    void doLogin(Call<String> requestService, Object respObject, int requestCode);
    void changeStatus(Call<String> requestService, Object respObject, int requestCode);
}
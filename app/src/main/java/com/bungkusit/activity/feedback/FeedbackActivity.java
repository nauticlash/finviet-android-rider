package com.bungkusit.activity.feedback;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import android.widget.TextView;

import com.bungkusit.R;
import com.bungkusit.activity.orderDetail.OrderDetailActivity;
import com.bungkusit.adapter.NotificationAdapter;
import com.bungkusit.databinding.ActivityFeedbackBinding;
import com.bungkusit.databinding.ActivityNotificationBinding;
import com.bungkusit.model.CommonResp;
import com.bungkusit.model.NotificationResp;
import com.bungkusit.utils.AppConstant;
import com.bungkusit.utils.AppGlobal;
import com.bungkusit.utils.AppPreferences;
import com.bungkusit.utils.Debug;
import com.bungkusit.utils.MyApplication;
import com.bungkusit.webservices.interfaces.OnApiCompleteListener;

import java.util.HashMap;

import retrofit2.Call;

import static com.bungkusit.utils.AppConstant.REQUEST_CODE_1;

public class FeedbackActivity extends AppCompatActivity implements OnApiCompleteListener, View.OnClickListener {

    private ActivityFeedbackBinding binding;

    private FeedbackPresenter presenter;

    private String orderId = "";
    public static final String EXTRA_ORDER_ID = "orderId";
    public static final String EXTRA_TITLE = "title";

    public String title;
    private int titleChar = 0, contentChar = 0;

    public static void startActivity(Activity mContext, Bundle mBundle) {
        Intent intent = new Intent(mContext, FeedbackActivity.class);
        intent.putExtras(mBundle);

        mContext.startActivity(intent);
        mContext.overridePendingTransition(R.anim.right2middle, R.anim.middle2left);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_feedback);

        if (getIntent().hasExtra(EXTRA_ORDER_ID))
            orderId = getIntent().getStringExtra(EXTRA_ORDER_ID);


        if (getIntent().hasExtra(EXTRA_TITLE))
            binding.setTitle(getIntent().getStringExtra(EXTRA_TITLE));


        init();
    }

    private void init() {
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        binding.setOrderId(orderId);
        binding.editTextTitle.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                titleChar = s.length();
                if (titleChar > 0 && contentChar > 0) {
                    enableSubmitButton();
                } else {
                    disableSubmitButton();
                }
            }

        });

        binding.editTextTitle.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        });

        binding.editTextDescription.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                contentChar = s.length();
                if (titleChar > 0 && contentChar > 0) {
                    enableSubmitButton();
                } else {
                    disableSubmitButton();
                }
            }

        });

        binding.editTextDescription.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        });

        binding.tvSubmit.setOnClickListener(this);
        disableSubmitButton();

    }

    public void enableSubmitButton() {
        binding.tvSubmit.getBackground().setAlpha(255);
        binding.tvSubmit.setEnabled(true);
    }

    public void disableSubmitButton() {
        binding.tvSubmit.getBackground().setAlpha(128);
        binding.tvSubmit.setEnabled(false);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tvSubmit) {
            if (isValid()) {
                callFeedbackAPI();
            }
        }
    }

    private boolean isValid() {
        if (binding.editTextTitle.getText().toString().trim().equals("")) {
            AppGlobal.showSnakeBar(getApplicationContext(), binding.llMain, getString(R.string.pleaseentertitle));
            return false;
        } else if (binding.editTextDescription.getText().toString().trim().equals("")) {
            AppGlobal.showSnakeBar(getApplicationContext(), binding.llMain, getString(R.string.pleaseenterdescription));
            return false;
        }
        return true;
    }

    private void callFeedbackAPI() {
        if (AppGlobal.isNetwork(getApplicationContext())) {

            AppPreferences appPreferences = MyApplication.getInstance().getAppPreferences();

            if (presenter == null)
                presenter = new FeedbackPresenterImpl(this);

            HashMap<String, Object> request = new HashMap<>();
            request.put("orderid", orderId);
            request.put("subject", binding.editTextTitle.getText().toString().trim());
            request.put("description", binding.editTextDescription.getText().toString().trim());

            HashMap<String, Object> auth = new HashMap<>();
            auth.put("id", appPreferences.getUserInfo().userid);
            auth.put("token", appPreferences.getToken());

            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.give_feedback));
            map.put(getResources().getString(R.string.request), request);
            map.put("auth", auth);

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

            presenter.giveFeedback(requestService, new CommonResp(), REQUEST_CODE_1);
        } else
            openFailDialog();
    }

    @Override
    public void onRequestCall() {
        binding.avi.smoothToShow();
    }

    @Override
    public void onResponseSuccess(Object success, int requestCode) {
        binding.avi.smoothToHide();
        if (requestCode == REQUEST_CODE_1 && success instanceof CommonResp) {
            final Dialog dialog = new Dialog(this);
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.alert_feedback_success);
            dialog.setCanceledOnTouchOutside(false);
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            lp.copyFrom(window.getAttributes());

            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);
            TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
            TextView tvBack = (TextView) dialog.findViewById(R.id.tvBack);
            tvClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    binding.editTextTitle.setText("");
                    binding.editTextDescription.setText("");
                    disableSubmitButton();
                }
            });
            tvBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    finish();
                }
            });
            dialog.show();
        }
    }

    @Override
    public void onResponseFailure(Object error, int requestCode) {
        binding.avi.smoothToHide();
        if (error instanceof String) {

        }
        if (requestCode == REQUEST_CODE_1) {
            openFailDialog();
        }
    }

    @Override
    public void onRequestCallBack(boolean isAuthenticationFailed) {
        binding.avi.smoothToHide();
    }

    private void openFailDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.alert_feedback_fail);
        dialog.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        TextView tvRetry = (TextView) dialog.findViewById(R.id.tvRetry);
        TextView tvBack = (TextView) dialog.findViewById(R.id.tvBack);
        tvRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
            }
        });
        dialog.show();

    }
}
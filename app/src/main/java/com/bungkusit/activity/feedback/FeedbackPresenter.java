package com.bungkusit.activity.feedback;

import retrofit2.Call;

public interface FeedbackPresenter {
    void giveFeedback(Call<String> requestService, Object respObject, int requestCode);
}
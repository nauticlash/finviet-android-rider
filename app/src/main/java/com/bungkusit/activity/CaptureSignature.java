package com.bungkusit.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import com.bungkusit.R;
import com.bungkusit.databinding.ActivitySignatureBinding;
import com.bungkusit.utils.Debug;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import static com.bungkusit.utils.AppConstant.REQUEST_CODE_1;

public class CaptureSignature extends AppCompatActivity {

    private ActivitySignatureBinding binding;
    private Signature mSignature;
    private Paint paint;

    public static void startActivity(Activity mContext, Bundle mBundle) {
        Intent intent = new Intent(mContext, CaptureSignature.class);
        intent.putExtras(mBundle);
        mContext.startActivityForResult(intent, REQUEST_CODE_1);
        mContext.overridePendingTransition(R.anim.right2middle, R.anim.middle2left);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signature);

        binding.btnSave.setEnabled(false);

        mSignature = new Signature(this, null);
        binding.llSignature.addView(mSignature);

        binding.btnSave.setOnClickListener(onButtonClick);
        binding.btnClear.setOnClickListener(onButtonClick);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        disableSaveButton();
    }

    private void enableSaveButton() {
        binding.btnSave.getBackground().setAlpha(255);
        binding.btnSave.setEnabled(true);
    }

    private void disableSaveButton() {
        binding.btnSave.getBackground().setAlpha(128);
        binding.btnSave.setEnabled(false);
    }

    Button.OnClickListener onButtonClick = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (v == binding.btnClear) {
                mSignature.clear();
            } else if (v == binding.btnSave) {
                mSignature.save();
            }
        }
    };

    public class Signature extends View {
        static final float STROKE_WIDTH = 10f;
        static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        Paint paint = new Paint();
        Path path = new Path();

        float lastTouchX;
        float lastTouchY;
        final RectF dirtyRect = new RectF();

        public Signature(Context context, AttributeSet attrs) {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        public void clear() {
            path.reset();
            invalidate();
            disableSaveButton();
        }

        public void save() {
            try {

                Bitmap returnedBitmap = Bitmap.createBitmap(binding.llSignature.getWidth(),
                        binding.llSignature.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(returnedBitmap);
                Drawable bgDrawable = binding.llSignature.getBackground();
                if (bgDrawable != null)
                    bgDrawable.draw(canvas);
                else
                    canvas.drawColor(Color.WHITE);
                binding.llSignature.draw(canvas);

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                returnedBitmap.compress(Bitmap.CompressFormat.PNG, 50, bytes);

                String path = Environment.getExternalStorageDirectory()
                        + File.separator + "." + getString(R.string.app_name);

                File file = new File(path);
                if (!file.exists())
                    file.mkdir();

                file = new File(path + File.separator + "signature.jpg");

                file.createNewFile();

                FileOutputStream fo = new FileOutputStream(file);
                fo.write(bytes.toByteArray());
                fo.close();


                Intent returnIntent = new Intent();
                returnIntent.putExtra("signaturePath", file.getAbsolutePath());
                setResult(RESULT_OK, returnIntent);
                finish();

            } catch (Exception e) {
            }
        }

        @Override
        protected void onDraw(Canvas canvas) {

            canvas.drawPath(path, paint);
        }

        @SuppressLint("ClickableViewAccessibility")
        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float eventX = event.getX();
            float eventY = event.getY();
            enableSaveButton();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:

                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++) {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }


//    void uploadItemImage(final Context context, final String imagePath) {
//
//        binding.imageviewReceiptCancel.setVisibility(View.GONE);
//        binding.avi.show();
//        File file = new File(imagePath);
//
//        final RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
//        final MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), reqFile);
//
//        new RestClient().getAPIListService().UploadProfilepic(body).enqueue(new Callback<UploadImageResponse>() {
//            @Override
//            public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> response) {
//                try {
//                    Debug.e("Gson Response", "Gson Response" + new Gson().toJson(response.body()));
//
//                    if (response.body().getSuccess().matches("1")) {
//
//                        uploadreceiptpath = response.body().getData();
//                        Debug.e("tag", "uploadreceiptpath ::" + uploadreceiptpath);
//
//                        makePaymentDialog("Destination");
//
//                    } else {
//                        AppGlobal.showSnakeBar(OrderDetailInProgressActivity.this, binding.rlMain, response.body().getMessage());
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    binding.imageviewReceiptCancel.setVisibility(View.VISIBLE);
//                    uploadreceiptpath = "";
//                    //AppGlobal.showSnakeBar(ReOrderDetailInProgressActivity.this, binding.rlMain, "Error in Uploading Image");
//                    Toast.makeText(getApplicationContext(), "Error in Uploading Image", Toast.LENGTH_SHORT).show();
//                }
//                binding.avi.hide();
//            }
//
//            @Override
//            public void onFailure(Call<UploadImageResponse> call, Throwable t) {
//                binding.avi.hide();
//                Log.e("lookinside", "failed" + t.toString());
//                t.printStackTrace();
//                Log.e("AddReview", "fail=" + t.getMessage());
//            }
//        });
//    }

}
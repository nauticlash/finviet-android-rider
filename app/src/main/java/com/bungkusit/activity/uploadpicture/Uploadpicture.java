package com.bungkusit.activity.uploadpicture;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bungkusit.R;
import com.bungkusit.activity.SplashActivity;
import com.bungkusit.activity.login.LoginActivity;
import com.bungkusit.activity.login.LoginPresenter;
import com.bungkusit.activity.login.LoginPresenterImpl;
import com.bungkusit.activity.register.RegisterActivity;
import com.bungkusit.databinding.ActivityUploadpictureBinding;
import com.bungkusit.model.RegisterResp;
import com.bungkusit.model.UploadImageResponse;
import com.bungkusit.model.VerifyResp;
import com.bungkusit.utils.AppConstant;
import com.bungkusit.utils.AppGlobal;
import com.bungkusit.utils.Debug;
import com.bungkusit.utils.MyApplication;
import com.bungkusit.utils.PermissionUtils;
import com.bungkusit.webservices.api.RestClient;
import com.bungkusit.webservices.interfaces.OnApiCompleteListener;
import com.google.gson.Gson;
import com.kbeanie.multipicker.api.CacheLocation;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesProvider;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.bungkusit.utils.AppConstant.REQUEST_CODE_1;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_2;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_3;
import static com.bungkusit.utils.AppGlobal.isGPSAvailable;
import static com.bungkusit.utils.AppGlobal.locationServicesEnabled;
import android.os.CountDownTimer;

public class Uploadpicture extends AppCompatActivity implements OnApiCompleteListener, View.OnClickListener
        , PermissionUtils.OnPermissionResponse, ImagePickerCallback {

    private static int THUMBNAIL_SIZE;
    private ActivityUploadpictureBinding binding;

    private LoginPresenter presenter;
    private String fname, lname, email, mobile, dob, address, password, confirmPassword, lat = "", lng = "", country = "", state = "", city = "",
            profilepic = "1", front_photo = "2", back_photo = "3", ic_front_photo = "4", ic_back_photo = "5", bike_number_plate = "6", bike_pic = "7", insurance_latter_pic = "8", road_tex_pic = "9";

    private PermissionUtils permissionUtils;
    private CameraImagePicker cameraPicker;
    private ImagePicker imagePicker;

    private String profilePath;

    private LocationGooglePlayServicesProvider provider;
    private HashMap<String, String> hashMap;
    String type = "", param_profilepic = "", param_front_photo = "", param_back_photo = "", param_ic_front_photo = "", param_ic_back_photo = "", param_bike_number_plate = "", param_bike_pic = "",
            param_insurance_latter_pic = "", param_road_tex_pic = "";

    public static boolean isUploading = false;
    public static boolean isApiInProgress = false;
    private String otp1 = "", otp2 = "", otp3 = "", otp4 = "";
    private int phoneChar = 0, passwordChar = 0, confirmPasswordChar = 0;
    private String token = "";
    private String user_id = "";

    private Dialog loadingDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_uploadpicture);
        isGPSAvailable = locationServicesEnabled(MyApplication.getInstance());

        try {
            Intent intent = getIntent();
            if (intent != null) {
                hashMap = (HashMap<String, String>) intent.getSerializableExtra("map");
                fname = hashMap.get(AppConstant.FIRSTNAME);
                lname = hashMap.get(AppConstant.LASTNAME);
                email = hashMap.get(AppConstant.EMAIL);
                mobile = hashMap.get(AppConstant.CONTACTNO);
                dob = hashMap.get(AppConstant.DOB);
                address = hashMap.get(AppConstant.ADDRESS);
                password = hashMap.get(AppConstant.PASSWORD);
                lat = hashMap.get(AppConstant.LAT);
                lng = hashMap.get(AppConstant.LNG);
                country = hashMap.get(AppConstant.COUNTRY);
                state = hashMap.get(AppConstant.STATE);
                city = hashMap.get(AppConstant.CITY);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        init();
    }

    private void callAPI() {

        if (AppGlobal.isNetwork(getApplicationContext())) {
            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_rider_register));

            HashMap<String, Object> request = new HashMap<>();
            HashMap<String, Object> data = new HashMap<>();

            if (lname.equals("")) {
                lname = "a";
//                String[] arrName = fname.split(" ");
//                if (arrName.length > 1) {
//                    fname = arrName[0];
//                    lname = arrName[1];
//                }
            }
            data.put("fbid", "");
            data.put("firstname", fname);
            data.put("lastname", lname);
            data.put("email", email);
            data.put("password", password);
            data.put("contactno", mobile);
            data.put("platform", AppConstant.PLATFORM_AND);
            data.put("usertype", AppConstant.USER_TYPE_DRIVER);
            data.put("lat", lat);
            data.put("lng", lng);
            data.put("dob", dob);
            data.put("address", address);
            data.put("deviceid", MyApplication.getInstance().getAppPreferences().getDeviceToken());
            data.put("profilepic", param_profilepic);
            data.put("front_photo", param_front_photo);
            data.put("back_photo", param_back_photo);
            data.put("ic_front_photo", param_ic_front_photo);
            data.put("ic_back_photo", param_ic_back_photo);
            data.put("bike_number_plate", param_bike_number_plate);
            data.put("bike_pic", param_bike_pic);
            data.put("insurance_latter_pic", param_insurance_latter_pic);
            data.put("road_tex_pic", param_road_tex_pic);
            data.put("country", country);
            data.put("state", state);
            data.put("city", city);

            request.put(getResources().getString(R.string.data), data);
            map.put(getResources().getString(R.string.request), request);

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);
            presenter.doLogin(requestService, new RegisterResp(), REQUEST_CODE_1);

            isApiInProgress = true;

        } else
            AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.msg_no_internet));

    }

    private void verifyAPI() {

        if (AppGlobal.isNetwork(getApplicationContext())) {
            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_rider_verify));

            HashMap<String, Object> data = new HashMap<>();

            HashMap<String, Object> auth = new HashMap<>();
            auth.put("id", this.user_id);
            auth.put("token", this.token);

            data.put("code", otp1 + otp2 + otp3 + otp4);

            map.put(getResources().getString(R.string.request), data);
            map.put("auth", auth);
            otp1 = "";
            otp2 = "";
            otp3 = "";
            otp4 = "";

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);
            presenter.doLogin(requestService, new VerifyResp(), REQUEST_CODE_2);

        } else
            AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.msg_no_internet));

    }

    @Override
    public void onResponseSuccess(Object success, int requestCode) {

        isApiInProgress = false;

        if (requestCode == REQUEST_CODE_1 && success instanceof RegisterResp) {
            RegisterResp registerResp = (RegisterResp) success;
            if (registerResp.success == 0) {
                Toast.makeText(getApplicationContext(), registerResp.message, Toast.LENGTH_SHORT).show();
            } else {
                this.user_id = registerResp.userid;
                this.token = registerResp.token;
//                Toast.makeText(getApplicationContext(), "Your OTP code is " + registerResp.code, Toast.LENGTH_LONG).show();
                showRegistrationDialog();
            }
        } else if (requestCode == REQUEST_CODE_2 && success instanceof VerifyResp) {
            loadingDialog.dismiss();
            showSuccessDialog();
        } else if (requestCode == REQUEST_CODE_3 && success instanceof VerifyResp) {
        }
    }

    @Override
    public void onResponseFailure(Object error, int requestCode) {
        isApiInProgress = false;

        if (error instanceof RegisterResp) {
            RegisterResp registerResp = (RegisterResp) error;
            Toast.makeText(getApplicationContext(), registerResp.message, Toast.LENGTH_SHORT).show();
        } else if (requestCode == REQUEST_CODE_2 && error instanceof VerifyResp) {
            loadingDialog.dismiss();
            resendCode();
            showRegistrationDialog();
            AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getResources().getString(R.string.otp_failed));
//            showFailDialog();
        } else if (error instanceof String) {
            showFailDialog();
//            AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, error.toString());
        }
    }

    @Override
    public void onRequestCall() {
        binding.avi.smoothToShow();
        binding.rlMain.setEnabled(false);
    }

    @Override
    public void onRequestCallBack(boolean isAuthenticationFailed) {
        binding.avi.smoothToHide();
        binding.rlMain.setEnabled(true);
    }

    private void init() {
        presenter = new LoginPresenterImpl(this);
//
//        binding.ivSmoke.setBackgroundResource(R.drawable.anim_smoke);
//
//        AnimationDrawable animationDrawable = (AnimationDrawable) binding.ivSmoke.getBackground();
//        animationDrawable.start();

        permissionUtils = new PermissionUtils(this);
        permissionUtils.requestPermissions(PermissionUtils.REQUEST_LOCATION_PERMISSION, PermissionUtils.REQUEST_CODE_LOCATION_PERMISSION);


//        binding.llTop.setOnClickListener(this);
//        binding.ivProfilepic.setOnClickListener(this);
//        binding.ivBackLicence.setOnClickListener(this);
//        binding.ivFrontLicence.setOnClickListener(this);
//        binding.ivFrontIc.setOnClickListener(this);
//        binding.ivBackIc.setOnClickListener(this);
//        binding.ivBikeNumberPlate.setOnClickListener(this);
//        binding.ivBikePicture.setOnClickListener(this);
//        binding.ivInsuranceLetter.setOnClickListener(this);
//        binding.ivRoadTax.setOnClickListener(this);
        binding.tvRegister.setOnClickListener(this);
        binding.ivBack.setOnClickListener(this);
        binding.ivClose.setOnClickListener(this);

        binding.etMobile.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                phoneChar = s.length();
                if (phoneChar > 0 && passwordChar > 0 && confirmPasswordChar > 0) {
                    enableSubmitButton();
                } else {
                    disableSubmitButton();
                }
            }

        });

        binding.etPassword.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                passwordChar = s.length();
                if (phoneChar > 0 && passwordChar > 0 && confirmPasswordChar > 0) {
                    enableSubmitButton();
                } else {
                    disableSubmitButton();
                }
            }

        });

        binding.etConfirmPassword.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                confirmPasswordChar = s.length();
                if (phoneChar > 0 && passwordChar > 0 && confirmPasswordChar > 0) {
                    enableSubmitButton();
                } else {
                    disableSubmitButton();
                }
            }

        });
        disableSubmitButton();
    }
    public void enableSubmitButton() {
        binding.tvRegister.getBackground().setAlpha(255);
        binding.tvRegister.setEnabled(true);
    }

    public void disableSubmitButton() {
        binding.tvRegister.getBackground().setAlpha(128);
        binding.tvRegister.setEnabled(false);
    }

    public void showRegistrationDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_registration);
        dialog.setCanceledOnTouchOutside(false);
        TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
        final TextView tvTimer = (TextView) dialog.findViewById(R.id.tvTimer);
        final EditText otpET1 = (EditText) dialog.findViewById(R.id.otpET1);
        final EditText otpET2 = (EditText) dialog.findViewById(R.id.otpET2);
        final EditText otpET3 = (EditText) dialog.findViewById(R.id.otpET3);
        final EditText otpET4 = (EditText) dialog.findViewById(R.id.otpET4);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                finish();
//                SplashActivity.startActivity(Uploadpicture.this, new Bundle());

            }
        });
        tvTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resendCode();
                dialog.dismiss();
                showRegistrationDialog();
            }
        });
        otpET1.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(count == 1) {
                    otp1 = s.toString();
                    otpET2.requestFocus();
                }
            }
        });

        otpET2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL && otp2.equalsIgnoreCase("") && event.getAction() == KeyEvent.ACTION_UP) {
                    otpET1.requestFocus();
                    otpET1.setText("");
                    otp1 = "";
                    return true;
                }
                return false;
            }
        });
        otpET2.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(count == 1) {
                    otp2 = s.toString();
                    otpET3.requestFocus();
                }
            }
        });

        otpET3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL && otp3.equalsIgnoreCase("")) {
                    otpET2.requestFocus();
                    otpET2.setText("");
                    otp2 = "";
                }
                return false;
            }
        });
        otpET3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL && otp3.equalsIgnoreCase("") && event.getAction() == KeyEvent.ACTION_UP) {
                    otpET2.requestFocus();
                    otpET2.setText("");
                    otp2 = "";
                    return true;
                }
                return false;
            }
        });
        otpET3.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(count == 1) {
                    otp3 = s.toString();
                    otpET4.requestFocus();
                }
            }
        });

        otpET4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL && otp4.equalsIgnoreCase("") && event.getAction() == KeyEvent.ACTION_UP) {
                    otpET3.requestFocus();
                    otpET3.setText("");
                    otp3 = "";
                    return true;
                }
                return false;
            }
        });
        otpET4.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(count == 1) {
                    otp4 = s.toString();
                    if (!otp1.equals("") && !otp2.equals("") && !otp3.equals("") && !otp4.equals("")) {
                        dialog.dismiss();
                        showLoadingDialog();
                    }
                }
            }
        });

        new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                tvTimer.setText("Mã xác thực sẽ được gửi lại sau " + millisUntilFinished / 1000 + " giây");
                tvTimer.setEnabled(false);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                tvTimer.setText("Gửi lại mã xác thực");
                tvTimer.setEnabled(true);
                tvTimer.setTextColor(getResources().getColor(R.color.text_blue));
            }

        }.start();

        dialog.show();
    }

    public void showLoadingDialog() {
        loadingDialog = new Dialog(this);
        loadingDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loadingDialog.setContentView(R.layout.alert_loading);
        loadingDialog.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = loadingDialog.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        verifyAPI();
//        if (otp1.equals("7") && otp2.equals("2") && otp3.equals("3") && otp4.equals("4")) {
//            loadingDialog.dismiss();
//            showSuccessDialog();
//        } else {
//            loadingDialog.dismiss();
//            showFailDialog();
//        }
    }

    public void showSuccessDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.alert_register_success);
        dialog.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                LoginActivity.startActivity(Uploadpicture.this, new Bundle());
//                SplashActivity.startActivity(Uploadpicture.this, new Bundle());

            }
        });
        dialog.show();
    }

    public void showFailDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.alert_register_fail);
        dialog.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        TextView tvBackToLogin = (TextView) dialog.findViewById(R.id.tvBackToLogin);
        tvBackToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                LoginActivity.startActivity(Uploadpicture.this, new Bundle());
//                SplashActivity.startActivity(Uploadpicture.this, new Bundle());

            }
        });
        TextView tvRetry = (TextView) dialog.findViewById(R.id.tvRetry);
        tvRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
//                LoginActivity.startActivity(Uploadpicture.this, new Bundle());
//                SplashActivity.startActivity(Uploadpicture.this, new Bundle());
                resendCode();
                showRegistrationDialog();
            }
        });
        dialog.show();
    }

    public void showConfirmQuitDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.alert_register_quit);
        dialog.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        TextView tvBackToLogin = (TextView) dialog.findViewById(R.id.tvBackToLogin);
        tvBackToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                LoginActivity.startActivity(Uploadpicture.this, new Bundle());

            }
        });
        TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.llTop:
//                Debug.v("Tag", "llTop");
//                Intent intent = new Intent(Uploadpicture.this, RegisterActivity.class);
//                intent.putExtra("map", hashMap);
//                RegisterActivity.isEdit = true;
//                startActivity(intent, REQUEST_BACK);
                finish();
                break;
            case R.id.tvRegister:

//                if (isUploading) {
//                    Toast.makeText(getApplicationContext(), R.string.wait_uploading_inprogress, Toast.LENGTH_LONG).show();
//                } else {
//                    if (param_front_photo.equalsIgnoreCase("")) {
//                        AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.frontside_lc));
//                    } else if (param_back_photo.equalsIgnoreCase("")) {
//                        AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.backside_lc));
//                    } else if (param_ic_front_photo.equalsIgnoreCase("")) {
//                        AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.frontMykad));
//                    } else if (param_ic_back_photo.equalsIgnoreCase("")) {
//                        AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.backMykad));
//                    } else if (param_bike_number_plate.equalsIgnoreCase("")) {
//                        AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.numberPlatePicbike));
//                    } else if (param_bike_pic.equalsIgnoreCase("")) {
//                        AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.selectBikePicture));
//                    } else if (param_insurance_latter_pic.equalsIgnoreCase("")) {
//                        AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.inssuranceLetterPic));
//                    } else if (param_road_tex_pic.equalsIgnoreCase("")) {
//                        AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.selectRoadTaxPicture));
//                    } else {
//                        isGPSAvailable = locationServicesEnabled(MyApplication.getInstance());
//                        if (SmartLocation.with(getApplicationContext()).location().state().locationServicesEnabled() || isGPSAvailable) {
//                            if (!isApiInProgress) {
//                                callAPI();
//                            }
//                        } else {
//                            Toast.makeText(Uploadpicture.this, "Please enable GPS Location From Setting to Continue", Toast.LENGTH_LONG).show();
//                        }
//
//                    }
//                }

                mobile = binding.etMobile.getText().toString().trim();
                password = binding.etPassword.getText().toString().trim();
                confirmPassword = binding.etConfirmPassword.getText().toString().trim();
                if (mobile.equals("")) {
                    binding.etMobile.requestFocus();
                    Toast.makeText(Uploadpicture.this, R.string.error_mobile, Toast.LENGTH_LONG).show();
                    break;
                } else if (mobile.length() < 7 ) {
                    binding.etMobile.requestFocus();
                    Toast.makeText(Uploadpicture.this, R.string.error_mobile_invalid, Toast.LENGTH_LONG).show();
                    break;
                }else if (password.equals("")) {
                    binding.etPassword.requestFocus();
                    Toast.makeText(Uploadpicture.this, R.string.error_password, Toast.LENGTH_LONG).show();
                    break;
                } else if (confirmPassword.equals("")) {
                    binding.etConfirmPassword.requestFocus();
                    Toast.makeText(Uploadpicture.this, R.string.error_confirmPassword, Toast.LENGTH_LONG).show();
                    break;
                } else if (!password.equals(confirmPassword)) {
                    binding.etConfirmPassword.requestFocus();
                    Toast.makeText(Uploadpicture.this, R.string.confirmPasswordNotMatch, Toast.LENGTH_LONG).show();
                    break;
                }
                isGPSAvailable = locationServicesEnabled(MyApplication.getInstance());
                if (SmartLocation.with(getApplicationContext()).location().state().locationServicesEnabled() || isGPSAvailable) {
                    if (!isApiInProgress) {
                        callAPI();
                    }
                } else {
                    Toast.makeText(Uploadpicture.this, "Please enable GPS Location From Setting to Continue", Toast.LENGTH_LONG).show();
                }

                //showRegistrationDialog();
                //   callAPI();
                break;
            case R.id.ivProfilepic:
            case R.id.ivBackLicence:
            case R.id.ivFrontLicence:
            case R.id.ivFrontIc:
            case R.id.ivBackIc:
            case R.id.ivBikeNumberPlate:
            case R.id.ivBikePicture:
            case R.id.ivInsuranceLetter:
            case R.id.ivRoadTax:

                if (isUploading) {
                    Toast.makeText(getApplicationContext(), R.string.wait_uploading_inprogress, Toast.LENGTH_LONG).show();
                } else {
                    type = v.getTag().toString();
                    permissionUtils.requestPermissions(PermissionUtils.REQUEST_CAMERA_GALLERY_PERMISSION, PermissionUtils.REQUEST_CODE_CAMERA_GALLERY_PERMISSION);
                }

                break;
            case R.id.ivBack:
                onBackPressed();
//                showConfirmQuitDialog();
                break;
            case R.id.ivClose:
                showConfirmQuitDialog();
                break;
        }
    }

    void uploadItemImage(final String imagePath, String type) {

        isUploading = true;
        binding.avi.show();
        File file = new File(imagePath);

        final String imageType = type;

        Debug.e("Image type", "Clicked on" + imageType);

        final RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
        final MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), reqFile);

        new RestClient().getAPIListService().UploadProfilepic(body).enqueue(new Callback<UploadImageResponse>() {
            @Override
            public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> response) {
                try {
                    Debug.e("Gson Response", "Gson Response" + new Gson().toJson(response.body()));

                    Debug.e("Image type", "set in " + imageType);

                    isUploading = false;

                    if (response.body().getSuccess().matches("1")) {
                        if (imageType.equalsIgnoreCase(profilepic)) {
                            param_profilepic = response.body().getData();
                        } else if (imageType.equalsIgnoreCase(front_photo)) {
                            param_front_photo = response.body().getData();
                        } else if (imageType.equalsIgnoreCase(back_photo)) {
                            param_back_photo = response.body().getData();
                        } else if (imageType.equalsIgnoreCase(ic_back_photo)) {
                            param_ic_back_photo = response.body().getData();
                        } else if (imageType.equalsIgnoreCase(ic_front_photo)) {
                            param_ic_front_photo = response.body().getData();
                        } else if (imageType.equalsIgnoreCase(bike_number_plate)) {
                            param_bike_number_plate = response.body().getData();
                        } else if (imageType.equalsIgnoreCase(bike_pic)) {
                            param_bike_pic = response.body().getData();
                        } else if (imageType.equalsIgnoreCase(insurance_latter_pic)) {
                            param_insurance_latter_pic = response.body().getData();
                        } else if (imageType.equalsIgnoreCase(road_tex_pic)) {
                            param_road_tex_pic = response.body().getData();
                        }
                        //Debug.e("tag", "ImagePath" + profilepic);
                    } else {
                        AppGlobal.showSnakeBar(Uploadpicture.this, binding.rlMain, response.body().getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    isUploading = false;
                    binding.avi.hide();
                    AppGlobal.showSnakeBar(Uploadpicture.this, binding.rlMain, getString(R.string.errorUploadingImage));
                }
                binding.avi.hide();
            }

            @Override
            public void onFailure(Call<UploadImageResponse> call, Throwable t) {
                binding.avi.hide();
                isUploading = false;
                t.printStackTrace();
                AppGlobal.showSnakeBar(Uploadpicture.this, binding.rlMain, getString(R.string.errorUploadingImage));

                if (imageType.equalsIgnoreCase(profilepic)) {
                    param_profilepic = "";
                    binding.ivProfilepic.setImageResource(R.drawable.profile_pic);
                } else if (imageType.equalsIgnoreCase(front_photo)) {
                    param_front_photo = "";
                    binding.ivFrontLicence.setImageResource(R.drawable.upload_license);
                } else if (imageType.equalsIgnoreCase(back_photo)) {
                    param_back_photo = "";
                    binding.ivBackLicence.setImageResource(R.drawable.upload_license);
                } else if (imageType.equalsIgnoreCase(ic_back_photo)) {
                    param_ic_back_photo = "";
                    binding.ivBackIc.setImageResource(R.drawable.upload_license);
                } else if (imageType.equalsIgnoreCase(ic_front_photo)) {
                    param_ic_front_photo = "";
                    binding.ivFrontIc.setImageResource(R.drawable.upload_license);
                } else if (imageType.equalsIgnoreCase(bike_number_plate)) {
                    param_bike_number_plate = "";
                    binding.ivBikeNumberPlate.setImageResource(R.drawable.upload_license);
                } else if (imageType.equalsIgnoreCase(bike_pic)) {
                    param_bike_pic = "";
                    binding.ivBikePicture.setImageResource(R.drawable.upload_license);
                } else if (imageType.equalsIgnoreCase(insurance_latter_pic)) {
                    param_insurance_latter_pic = "";
                    binding.ivInsuranceLetter.setImageResource(R.drawable.upload_license);
                } else if (imageType.equalsIgnoreCase(road_tex_pic)) {
                    param_road_tex_pic = "";
                    binding.ivRoadTax.setImageResource(R.drawable.upload_license);
                }


            }
        });
    }


    @Override
    public void onPermissionGranted(int requestCode) {
        switch (requestCode) {
            case PermissionUtils.REQUEST_CODE_CAMERA_GALLERY_PERMISSION:
                selectImage();
                break;
            case PermissionUtils.REQUEST_CODE_LOCATION_PERMISSION:
                provider = new LocationGooglePlayServicesProvider();
                provider.setCheckLocationSettings(true);

                SmartLocation smartLocation = new SmartLocation.Builder(getApplicationContext()).logging(true).build();
                smartLocation.location(provider).oneFix().start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        lat = location.getLatitude() + "";
                        lng = location.getLongitude() + "";

                    }
                });
                break;
        }
    }

    @Override
    public void onPermissionDenied(int requestCode) {
        switch (requestCode) {
            case PermissionUtils.REQUEST_CODE_LOCATION_PERMISSION:
                AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.locationpermissiondenied));
                break;
            case PermissionUtils.REQUEST_CODE_CAMERA_GALLERY_PERMISSION:
                isUploading = false;
                AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.storagePermissionDenied));
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionUtils.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void selectImage() {
        final CharSequence[] options = {getResources().getString(R.string.take_a_photo),
                getResources().getString(R.string.choose_from_gallery),
                getResources().getString(R.string.cancel)};

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(Uploadpicture.this);
        builder.setTitle("Chọn hình ảnh");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(getResources().getString(R.string.take_a_photo))) {
                    takePicture();
                } else if (options[item].equals(getResources().getString(R.string.choose_from_gallery))) {
                    pickImageSingle();
                } else if (options[item].equals(getResources().getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void pickImageSingle() {
        imagePicker = new ImagePicker(this);
        imagePicker.setFolderName("Random");
        imagePicker.setRequestId(1234);
        imagePicker.ensureMaxSize(500, 500);
        imagePicker.shouldGenerateMetadata(true);
        imagePicker.shouldGenerateThumbnails(true);
        imagePicker.setImagePickerCallback(this);
        Bundle bundle = new Bundle();
        bundle.putInt("android.intent.extras.CAMERA_FACING", 1);
        imagePicker.pickImage();
    }

    public void takePicture() {
        cameraPicker = new CameraImagePicker(this);
        cameraPicker.setDebugglable(true);
        cameraPicker.setCacheLocation(CacheLocation.EXTERNAL_STORAGE_APP_DIR);
        cameraPicker.setImagePickerCallback(this);
        cameraPicker.shouldGenerateMetadata(true);
        cameraPicker.shouldGenerateThumbnails(true);
        profilePath = cameraPicker.pickImage();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(this);
                    imagePicker.setImagePickerCallback(this);
                }
                imagePicker.submit(data);
            } else if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = new CameraImagePicker(this);
                    cameraPicker.setImagePickerCallback(this);
                    cameraPicker.reinitialize(profilePath);
                }
                cameraPicker.submit(data);
            }
        }
    }

    @Override
    public void onImagesChosen(List<ChosenImage> images) {

        profilePath = images.get(0).getOriginalPath();

        Uri selectedImage = Uri.fromFile(new File(profilePath));

        try {
            Bitmap bitmap = getThumbnail(selectedImage);

            if (type.equalsIgnoreCase(profilepic)) {
                binding.ivProfilepic.setImageBitmap(bitmap);
            } else if (type.equalsIgnoreCase(front_photo)) {
                binding.ivFrontLicence.setImageBitmap(bitmap);
            } else if (type.equalsIgnoreCase(back_photo)) {
                binding.ivBackLicence.setImageBitmap(bitmap);
            } else if (type.equalsIgnoreCase(ic_back_photo)) {
                binding.ivBackIc.setImageBitmap(bitmap);
            } else if (type.equalsIgnoreCase(ic_front_photo)) {
                binding.ivFrontIc.setImageBitmap(bitmap);
            } else if (type.equalsIgnoreCase(bike_number_plate)) {
                binding.ivBikeNumberPlate.setImageBitmap(bitmap);
            } else if (type.equalsIgnoreCase(bike_pic)) {
                binding.ivBikePicture.setImageBitmap(bitmap);
            } else if (type.equalsIgnoreCase(insurance_latter_pic)) {
                binding.ivInsuranceLetter.setImageBitmap(bitmap);
            } else if (type.equalsIgnoreCase(road_tex_pic)) {
                binding.ivRoadTax.setImageBitmap(bitmap);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        if (selectedImage != null) {
            uploadItemImage(profilePath, type);
        }

    }

    @Override
    public void onError(String s) {
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
        isUploading = false;
    }


    public static Bitmap getThumbnail(Uri uri) throws FileNotFoundException, IOException {

        THUMBNAIL_SIZE = 300;
        InputStream input = MyApplication.getInstance().getContentResolver().openInputStream(uri);

        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither = true;//optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();

        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1)) {
            return null;
        }

        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

        double ratio = (originalSize > THUMBNAIL_SIZE) ? (originalSize / THUMBNAIL_SIZE) : 1.0;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);
        bitmapOptions.inDither = true; //optional
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//
        input = MyApplication.getInstance().getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }

    private static int getPowerOfTwoForSampleRatio(double ratio) {
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        if (k == 0) return 1;
        else return k;
    }

    private void resendCode() {
        if (AppGlobal.isNetwork(getApplicationContext())) {
            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_resend_code));

            HashMap<String, Object> data = new HashMap<>();

            HashMap<String, Object> auth = new HashMap<>();
            auth.put("id", this.user_id);
            auth.put("token", this.token);

            data.put("contactno", this.mobile);

            map.put(getResources().getString(R.string.request), data);
            map.put("auth", auth);

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);
            presenter.doLogin(requestService, new VerifyResp(), REQUEST_CODE_3);

        }
    }
}
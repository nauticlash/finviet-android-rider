package com.bungkusit.activity;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import com.bungkusit.R;
import com.bungkusit.databinding.ActivityReceiptBinding;
import com.bungkusit.utils.PermissionUtils;
import java.io.File;
import java.io.FileOutputStream;

import static com.bungkusit.utils.AppConstant.REQUEST_CODE_1;

public class CaptureReceipt extends AppCompatActivity implements PermissionUtils.OnPermissionResponse {

    private ActivityReceiptBinding binding;
    private Camera camera;
    private SurfaceView preview;
    private SurfaceHolder previewHolder;
    private boolean cameraConfigured;
    private boolean inPreview = false;
    private Camera.PictureCallback mPicture;
    public static Bitmap bitmap;
    private PermissionUtils permissionUtils;
    private byte[] image_data;

    public static void startActivity(Activity mContext, Bundle mBundle) {
        Intent intent = new Intent(mContext, CaptureReceipt.class);
        intent.putExtras(mBundle);
        mContext.startActivityForResult(intent, REQUEST_CODE_1);
        mContext.overridePendingTransition(R.anim.right2middle, R.anim.middle2left);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_receipt);

        permissionUtils = new PermissionUtils(this);
        permissionUtils.requestPermissions(PermissionUtils.REQUEST_CAMERA_GALLERY_PERMISSION, PermissionUtils.REQUEST_CODE_CAMERA_GALLERY_PERMISSION);
        binding.btnSave.setOnClickListener(onButtonClick);
        binding.btnClear.setOnClickListener(onButtonClick);
        binding.btnCapture.setOnClickListener(onButtonClick);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        preview = (SurfaceView) findViewById(R.id.svPreview);
        previewHolder = preview.getHolder();
        previewHolder.addCallback(surfaceCallback);
        previewHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    @Override
    public void onResume() {
        super.onResume();
        permissionUtils.requestPermissions(PermissionUtils.REQUEST_CAMERA_GALLERY_PERMISSION, PermissionUtils.REQUEST_CODE_CAMERA_GALLERY_PERMISSION);
    }

    @Override
    public void onPause() {
        if (inPreview) {
            camera.stopPreview();
            camera.release();
            camera = null;
            inPreview = false;
        }
        super.onPause();
    }

    Button.OnClickListener onButtonClick = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.btnClear:
                    binding.btnClear.setVisibility(View.GONE);
                    binding.btnSave.setVisibility(View.GONE);
                    binding.btnCapture.setVisibility(View.VISIBLE);
                    startPreview();
                    break;
                case R.id.btnSave:
                    try {
                        String path = Environment.getExternalStorageDirectory()
                                + File.separator + "." + getString(R.string.app_name);

                        File file = new File(path);
                        if (!file.exists())
                            file.mkdir();

                        file = new File(path + File.separator + "signature.jpg");

                        file.createNewFile();

                        FileOutputStream fo = new FileOutputStream(file);
                        fo.write(image_data);
                        fo.close();


                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("signaturePath", file.getAbsolutePath());
                        setResult(RESULT_OK, returnIntent);
                        finish();
                    } catch (Exception e) {
                        finish();
                        startActivity(getIntent());
                    }
                    break;
                case R.id.btnCapture:
                    camera.takePicture(null, null, mPicture);
                    binding.btnClear.setVisibility(View.VISIBLE);
                    binding.btnSave.setVisibility(View.VISIBLE);
                    binding.btnCapture.setVisibility(View.GONE);
                    break;
                default:
                    break;
            }
        }
    };

    private Camera.Size getBestPreviewSize(int width, int height,
                                           Camera.Parameters parameters) {
        Camera.Size result = null;

        for (Camera.Size size : parameters.getSupportedPreviewSizes()) {
            if (size.width <= width && size.height <= height) {
                if (result == null) {
                    result = size;
                } else {
                    int resultArea = result.width * result.height;
                    int newArea = size.width * size.height;

                    if (newArea > resultArea) {
                        result = size;
                    }
                }
            }
        }

        return result;
    }

    private void initPreview(int width, int height) {
        if (camera != null && previewHolder.getSurface() != null) {
            try {
                camera.setPreviewDisplay(previewHolder);
            } catch (Throwable t) {
            }

            if (!cameraConfigured) {
                Camera.Parameters parameters = camera.getParameters();
                Camera.Size size = getBestPreviewSize(width, height,
                        parameters);

                if (size != null) {
                    parameters.setPreviewSize(size.width, size.height);
                    camera.setParameters(parameters);
                    cameraConfigured = true;
                }
            }
        }
    }

    private void startPreview() {
        if (cameraConfigured && camera != null) {
            camera.startPreview();
            inPreview = true;
        }
    }


    SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {
        public void surfaceCreated(SurfaceHolder holder) {
            // no-op -- wait until surfaceChanged()
        }

        public void surfaceChanged(SurfaceHolder holder,
                                   int format, int width,
                                   int height) {
            initPreview(width, height);
            startPreview();
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            // no-op
        }
    };

    private Camera.PictureCallback getPictureCallback() {
        Camera.PictureCallback picture = new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                image_data = data;
                bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
            }
        };
        return picture;
    }

    @Override
    public void onPermissionGranted(int requestCode) {
        switch (requestCode) {
            case PermissionUtils.REQUEST_CODE_CAMERA_GALLERY_PERMISSION:
                camera = Camera.open();
                camera.setDisplayOrientation(90);
                mPicture = getPictureCallback();
                startPreview();
                break;
        }
    }

    @Override
    public void onPermissionDenied(int requestCode) {
        /*switch (requestCode) {
            case PermissionUtils.REQUEST_CODE_LOCATION_PERMISSION:
                AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.locationpermissiondenied));
                break;
        }*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionUtils.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

}
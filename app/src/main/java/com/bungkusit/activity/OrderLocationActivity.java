package com.bungkusit.activity;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;

import com.bungkusit.BR;
import com.bungkusit.R;
import com.bungkusit.activity.main.MainActivity;
import com.bungkusit.activity.orderDetail.OrderDetailActivity;
import com.bungkusit.activity.orderDetail.OrderDetailPresenter;
import com.bungkusit.activity.orderDetail.OrderDetailPresenterImpl;
import com.bungkusit.adapter.OrderItemAdapter;
import com.bungkusit.databinding.ActivityOrderLocationBinding;
import com.bungkusit.model.CommonResp;
import com.bungkusit.model.OrderDetailResp;
import com.bungkusit.model.OrderItem;
import com.bungkusit.utils.AppConstant;
import com.bungkusit.utils.AppGlobal;
import com.bungkusit.utils.AppPreferences;
import com.bungkusit.utils.MyApplication;
import com.bungkusit.webservices.interfaces.OnApiCompleteListener;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.nlopez.smartlocation.SmartLocation;
import retrofit2.Call;

import static com.bungkusit.utils.AppConstant.REQUEST_CODE_1;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_2;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_3;
import static com.bungkusit.utils.AppGlobal.isGPSAvailable;
import static com.bungkusit.utils.AppGlobal.locationServicesEnabled;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class OrderLocationActivity extends AppCompatActivity implements OnApiCompleteListener,
        OnMapReadyCallback, View.OnClickListener, LocationListener {

    public static final String EXTRA_ORDER_ID = "order_id";
    private ActivityOrderLocationBinding binding;
    private String orderId = "";
    private GoogleMap mMap;
    OrderDetailResp orderDetailResp;
    private AppPreferences appPreferences;
    private OrderDetailPresenter presenter;
    private FusedLocationProviderClient fusedLocationClient;
    LatLng pickupLocation;
    LatLng deliveryLocation;
    LatLngBounds.Builder builder;
    private Polyline polyline;

    ArrayList<LatLng> MarkerPoints;

    public static void startActivity(Activity mContext, Bundle mBundle) {
        Intent intent = new Intent(mContext, OrderLocationActivity.class);
        intent.putExtras(mBundle);
        mContext.startActivity(intent);
        mContext.overridePendingTransition(R.anim.right2middle, R.anim.middle2left);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapsInitializer.initialize(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_order_location);
        isGPSAvailable = locationServicesEnabled(MyApplication.getInstance());
        appPreferences = MyApplication.getInstance().getAppPreferences();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_map);
        mapFragment.getMapAsync(this);
        MarkerPoints =  new ArrayList<>();
        if (getIntent().hasExtra(EXTRA_ORDER_ID))
            orderId = getIntent().getStringExtra(EXTRA_ORDER_ID);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        init();
    }

    private void init() {
        binding.tvPlaceBid.setOnClickListener(this);
        binding.tvViewDetail.setOnClickListener(this);
        binding.tvStartOrder.setOnClickListener(this);
        binding.ivBack.setOnClickListener(this);
        binding.ivCurrent.setOnClickListener(this);
    }

    @Override
    public void onResponseSuccess(Object success, int requestCode) {
        if (requestCode == REQUEST_CODE_1 && success instanceof OrderDetailResp) {
            orderDetailResp = (OrderDetailResp) success;
            setData(orderDetailResp);
        } else if (requestCode == REQUEST_CODE_2) {
            Bundle bundle = new Bundle();
            bundle.putString(OrderDetailActivity.EXTRA_ORDER_ID, orderId);
            OrderDetailActivity.startActivity(this, bundle);
        } else if (requestCode == REQUEST_CODE_3) {
            Bundle bundle = new Bundle();
            bundle.putString(OrderDetailActivity.EXTRA_ORDER_ID, orderId);
            OrderDetailActivity.startActivity(this, bundle);
        }
    }

    @Override
    public void onResponseFailure(Object error, int requestCode) {
        Bundle bundle = new Bundle();
        bundle.putString(OrderDetailActivity.EXTRA_ORDER_ID, orderId);
        finish();
        OrderDetailActivity.startActivity(this, bundle);
    }

    @Override
    public void onRequestCall() {
    }


    @Override
    public void onRequestCallBack(boolean isAuthenticationFailed) {
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);

//        getOrderDetailAPI();
    }

    private void getOrderDetailAPI() {

        if (AppGlobal.isNetwork(getApplicationContext())) {

            appPreferences = MyApplication.getInstance().getAppPreferences();
            if (presenter == null)
                presenter = new OrderDetailPresenterImpl(this);

            HashMap<String, Object> request = new HashMap<>();
            request.put("orderid", orderId);
            request.put("type", AppConstant.USER_TYPE_DRIVER);

            HashMap<String, Object> auth = new HashMap<>();
            auth.put("id", appPreferences.getUserInfo().userid);
            auth.put("token", appPreferences.getToken());

            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_get_order_details));
            map.put(getResources().getString(R.string.request), request);
            map.put("auth", auth);

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

            presenter.getOrderDetail(requestService, new OrderDetailResp(), REQUEST_CODE_1);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getOrderDetailAPI();
    }

    @Override
    public void onLocationChanged(Location location) {

        if (mMap != null) {
            mMap.clear();
            if (MarkerPoints.size() > 1) {
                MarkerPoints.clear();
            }
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            if (!(AppGlobal.isEmpty(orderDetailResp.data.plat) && AppGlobal.isEmpty(orderDetailResp.data.plng))) {
                pickupLocation = new LatLng(Double.parseDouble(orderDetailResp.data.plat), Double.parseDouble(orderDetailResp.data.plng));
                mMap.addMarker(new MarkerOptions().position(pickupLocation)
                        .title(getResources().getString(R.string.pickup_location))
                        .snippet(orderDetailResp.data.pickupAddress)
                        .icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.ic_location_pickup)));
                MarkerPoints.add(pickupLocation);
                builder.include(pickupLocation);
            }

            if (!(AppGlobal.isEmpty(orderDetailResp.data.dllat) && AppGlobal.isEmpty(orderDetailResp.data.dllng))) {
                deliveryLocation = new LatLng(Double.parseDouble(orderDetailResp.data.dllat), Double.parseDouble(orderDetailResp.data.dllng));
                mMap.addMarker(new MarkerOptions().position(deliveryLocation)
                        .title(getResources().getString(R.string.drop_off_location))
                        .snippet(orderDetailResp.data.deliveryAddress)
                        .icon(BitmapDescriptorFactory
                                .fromResource(R.drawable.ic_location_dropoff)));
                MarkerPoints.add(deliveryLocation);
                builder.include(deliveryLocation);
            }
            Drawable d = getResources().getDrawable(R.drawable.rider_marker);
            Canvas canvas = new Canvas();
            Bitmap bitmap = Bitmap.createBitmap(d.getIntrinsicWidth(), d.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            canvas.setBitmap(bitmap);
            d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
            d.draw(canvas);
            BitmapDescriptor bd = BitmapDescriptorFactory.fromBitmap(bitmap);
            mMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).icon(bd));
        }
    }

    private void setData(OrderDetailResp orderDetailResp) {
        if (orderDetailResp != null && orderDetailResp.data != null) {
            binding.setVariable(BR.orderInfo, orderDetailResp.data);

            if (mMap != null) {
                // Already two locations
                if (MarkerPoints.size() > 1) {
                    MarkerPoints.clear();
                }
                Drawable d = getResources().getDrawable(R.drawable.rider_marker);
                Canvas canvas = new Canvas();
                Bitmap bitmap = Bitmap.createBitmap(d.getIntrinsicWidth(), d.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
                canvas.setBitmap(bitmap);
                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                d.draw(canvas);

                LatLng lastLocation = appPreferences.getLastLocation();
                builder = new LatLngBounds.Builder();
                if (lastLocation != null) {
                    BitmapDescriptor bd = BitmapDescriptorFactory.fromBitmap(bitmap);
                    mMap.addMarker(new MarkerOptions().position(lastLocation).icon(bd));
                    MarkerPoints.add(lastLocation);
                    builder.include(lastLocation);
                }

                if (!(AppGlobal.isEmpty(orderDetailResp.data.plat) && AppGlobal.isEmpty(orderDetailResp.data.plng))) {
                    pickupLocation = new LatLng(Double.parseDouble(orderDetailResp.data.plat), Double.parseDouble(orderDetailResp.data.plng));
                    mMap.addMarker(new MarkerOptions().position(pickupLocation)
                            .title(getResources().getString(R.string.pickup_location))
                            .snippet(orderDetailResp.data.pickupAddress)
                            .icon(BitmapDescriptorFactory
                                    .fromResource(R.drawable.ic_location_pickup)));
                    if (!orderDetailResp.data.isPickup.equals("3")) {
                        MarkerPoints.add(pickupLocation);
                        builder.include(pickupLocation);
                    }
                }

                if (!(AppGlobal.isEmpty(orderDetailResp.data.dllat) && AppGlobal.isEmpty(orderDetailResp.data.dllng))) {
                    deliveryLocation = new LatLng(Double.parseDouble(orderDetailResp.data.dllat), Double.parseDouble(orderDetailResp.data.dllng));
                    mMap.addMarker(new MarkerOptions().position(deliveryLocation)
                            .title(getResources().getString(R.string.drop_off_location))
                            .snippet(orderDetailResp.data.deliveryAddress)
                            .icon(BitmapDescriptorFactory
                                    .fromResource(R.drawable.ic_location_dropoff)));
                    if (orderDetailResp.data.isPickup.equals("3")) {
                        MarkerPoints.add(deliveryLocation);
                        builder.include(deliveryLocation);
                    }
                    if (orderDetailResp.data.isPickup.equals("0")) {
                        builder.include(deliveryLocation);
                    }

                }
                if (lastLocation == null) {
                    builder.include(pickupLocation);
                    builder.include(deliveryLocation);
                }

                // Checks, whether start and end locations are captured
                if (MarkerPoints.size() >= 2) {
                    LatLng origin = MarkerPoints.get(0);
                    LatLng dest = MarkerPoints.get(1);

                    // Getting URL to the Google Directions API
                    String url = getUrl(origin, dest);
                    Log.d("onMapClick", url.toString());
                    FetchUrl FetchUrl = new FetchUrl();

                    // Start downloading json data from Google Directions API
                    FetchUrl.execute(url);
                    //move map camera
                }
                LatLngBounds bounds = builder.build();
                int width = getResources().getDisplayMetrics().widthPixels;
                int height = getResources().getDisplayMetrics().heightPixels;
                int padding = (int) (width * 0.10);
                int padding_bot = (int) (height * 0.45);
                mMap.setPadding( padding, padding, padding, padding_bot);
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, 0);
                mMap.animateCamera(cu);

            }
            if (orderDetailResp.data.status.equalsIgnoreCase("Pending")) {
                binding.tvPlaceBid.setVisibility(View.VISIBLE);
            } else if (orderDetailResp.data.status.equalsIgnoreCase("In Progress") && orderDetailResp.data.riderId.equals(appPreferences.getUserInfo().userid) && orderDetailResp.data.isPickup.equalsIgnoreCase("0")) {
                binding.tvPlaceBid.setVisibility(View.GONE);
                binding.tvStartOrder.setVisibility(View.VISIBLE);
            } else {
                binding.tvPlaceBid.setVisibility(View.GONE);
                binding.tvStartOrder.setVisibility(View.GONE);
                binding.tvViewDetail.setText(R.string.view_details);
            }
            binding.tvViewDetail.setVisibility(View.VISIBLE);
            if (!orderDetailResp.data.order_items.equals("")) {
                try {
                    JSONArray orderItems = new JSONArray(orderDetailResp.data.order_items);
                    ArrayList<OrderItem> items = new ArrayList<OrderItem>();
                    for (int i = 0; i < orderItems.length(); i++) {
                        JSONObject jo = (JSONObject) orderItems.get(i);
                        OrderItem item = new OrderItem();
                        item.name = jo.getString("name");
                        item.amount = jo.getInt("amount");
                        item.quantity = jo.getInt("quantity");
                        item.position = i+1;
                        items.add(item);
                    }

                    binding.rvOrderItems.setLayoutManager(new LinearLayoutManager(this));
                    OrderItemAdapter mAdapter = new OrderItemAdapter(items);
                    binding.rvOrderItems.setAdapter(mAdapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        isGPSAvailable = locationServicesEnabled(MyApplication.getInstance());
        if (SmartLocation.with(OrderLocationActivity.this).location().state().locationServicesEnabled() || isGPSAvailable) {
            switch (v.getId()) {
                case R.id.tvViewDetail:
                    Bundle bundle = new Bundle();
                    bundle.putString(OrderDetailActivity.EXTRA_ORDER_ID, orderDetailResp.data.orderid);
                    OrderDetailActivity.startActivity(this, bundle);
                    break;
                case R.id.tvPlaceBid:
                    callPlaceBid();
                    break;
                case R.id.tvStartOrder:
                    callStartToPickup();
                    break;
                case R.id.ivBack:
                    MainActivity.startActivity(this, new Bundle());
                    finish();
                    break;
                case R.id.ivCurrent:
                    if (mMap != null) {
                        LatLng lastLocation = appPreferences.getLastLocation();
                        if (lastLocation != null) {
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                                    lastLocation, 15.0f));
                        } else {

                            LatLngBounds bounds = builder.build();
                            int width = getResources().getDisplayMetrics().widthPixels;
                            int height = getResources().getDisplayMetrics().heightPixels;
                            int padding = (int) (width * 0.10);
                            int padding_bot = (int) (height * 0.45);
                            mMap.setPadding( padding, padding, padding, padding_bot);
                            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, 0);
                            mMap.animateCamera(cu);
                        }
                    }
                    break;
            }
        } else {
            AppGlobal.openLocationDialog(OrderLocationActivity.this);
        }
    }

    private String getUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;


        // Sensor enabled
        String sensor = "sensor=false";

        String google_api_key = getResources().getString(R.string.google_api_key_new);

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&key=" + google_api_key;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }


    // Fetches data from url passed
    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data.toString());
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }


    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data.toString());
            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask",jsonData[0].toString());
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());

                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask","Executing routes");
                Log.d("ParserTask",routes.toString());

            } catch (Exception e) {
                Log.d("ParserTask",e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points;
            PolylineOptions lineOptions = null;

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(8);
                lineOptions.color(getResources().getColor(R.color.text_blue));

                Log.d("onPostExecute","onPostExecute lineoptions decoded");

            }

            // Drawing polyline in the Google Map for the i-th route
            if(lineOptions != null) {
                if (polyline != null) {
                    polyline.remove();
                }
                polyline = mMap.addPolyline(lineOptions);
            }
            else {
                Log.d("onPostExecute","without Polylines drawn");
            }
        }
    }

    private void callPlaceBid() {
        appPreferences = MyApplication.getInstance().getAppPreferences();

        if (AppGlobal.isNetwork(getApplicationContext())) {


            OrderDetailPresenter order_presenter = new OrderDetailPresenterImpl(this);

            HashMap<String, Object> request = new HashMap<>();
            request.put("orderid", orderId);

            HashMap<String, Object> auth = new HashMap<>();
            auth.put("id", appPreferences.getUserInfo().userid);
            auth.put("token", appPreferences.getToken());

            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_postbid));
            map.put(getResources().getString(R.string.request), request);
            map.put("auth", auth);

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

            order_presenter.getOrderDetail(requestService, new CommonResp(), REQUEST_CODE_2);
        }
    }

    private void callStartToPickup() {
        appPreferences = MyApplication.getInstance().getAppPreferences();

        if (AppGlobal.isNetwork(getApplicationContext())) {

            OrderDetailPresenter order_presenter = new OrderDetailPresenterImpl(this);

            HashMap<String, Object> request = new HashMap<>();
            request.put("riderid", orderDetailResp.data.riderId);
            request.put("orderid", orderId);


            HashMap<String, Object> auth = new HashMap<>();
            auth.put("id", appPreferences.getUserInfo().userid);
            auth.put("token", appPreferences.getToken());

            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_start_pickup));
            map.put(getResources().getString(R.string.request), request);
            map.put("auth", auth);

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

            order_presenter.getOrderDetail(requestService, new CommonResp(), REQUEST_CODE_3);
        }
    }
}

package com.bungkusit.activity.main;

import retrofit2.Call;

public interface MainPresenter {

    void updateLocation(Call<String> requestService, Object respObject, int requestCode);
    void changeStatus(Call<String> requestService, Object respObject, int requestCode);
    void checkVersion(Call<String> requestService, Object respObject, int requestCode);
    void getNotification(Call<String> requestService, Object respObject, int requestCode);
    void editProfile(Call<String> requestService, Object respObject, int requestCode);
}
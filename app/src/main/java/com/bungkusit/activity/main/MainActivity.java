package com.bungkusit.activity.main;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bungkusit.R;
import com.bungkusit.activity.ChangePasswordActivity;
import com.bungkusit.activity.chatconversation.ChatConversationActivity;
import com.bungkusit.activity.feedback.FeedbackActivity;
import com.bungkusit.activity.login.LoginActivity;
import com.bungkusit.activity.my_profile.MyProfileActivity;
import com.bungkusit.activity.notification.NotificationActivity;
import com.bungkusit.adapter.ViewPagerAdapter;
import com.bungkusit.databinding.ActivityMainBinding;
import com.bungkusit.databinding.AlertLogoutBinding;
import com.bungkusit.databinding.NavHeaderMainBinding;
import com.bungkusit.fcm.Config;
import com.bungkusit.fragment.all_order.NewOrderFragment;
import com.bungkusit.model.ChangeStatusResp;
import com.bungkusit.model.CommonResp;
import com.bungkusit.model.LoginResp;
import com.bungkusit.model.NotificationResp;
import com.bungkusit.model.UserInfo;
import com.bungkusit.service.LocationService;
import com.bungkusit.service.MyLocation;
import com.bungkusit.utils.AppConstant;
import com.bungkusit.utils.AppGlobal;
import com.bungkusit.utils.AppPreferences;
import com.bungkusit.utils.ConnectivityReceiver;
import com.bungkusit.utils.Debug;
import com.bungkusit.utils.MyApplication;
import com.bungkusit.utils.PermissionUtils;
import com.bungkusit.utils.TimeManager;
import com.bungkusit.views.textview.TextViewRegular;
import com.bungkusit.webservices.interfaces.OnApiCompleteListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesProvider;
import retrofit2.Call;

import static com.bungkusit.utils.AppConstant.REQUEST_CODE_2;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_4;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_5;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_6;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_7;
import static com.bungkusit.utils.AppGlobal.isGPSAvailable;
import static com.bungkusit.utils.AppGlobal.locationServicesEnabled;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, TimeManager.OnTimeChanger,
        ConnectivityReceiver.ConnectivityReceiverListener, PermissionUtils.OnPermissionResponse, OnApiCompleteListener, View.OnClickListener {

    private ActivityMainBinding binding;
    private NavHeaderMainBinding navHeaderMainBinding;

    private AppPreferences appPreferences;

    private ActionBarDrawerToggle toggle;
    private Fragment fContainer;
    private int navigationState;

    public static MainActivity main;

    // Added By Ajay 2-11-2017
    private PermissionUtils permissionUtils;
    private LocationGooglePlayServicesProvider provider;

    private MainPresenter presenter;

    private TextView textCartItemCount;
    private Menu menu;

    private MenuItem menuItem0, menuItem1;

    private LocationService mLocationService;
    Intent mServiceIntent;

    private TimeManager timeManager;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    public static final String NOTIFICATION_MSG = "adminmsg";

    public static final String[] REQUEST_LOCATION_PERMISSION = new String[]{"android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_COARSE_LOCATION"};
    private NotificationResp notificationResp;
    public static final String EXTRA_ACTIVITY_ACTION = "";


    public static void startActivity(Activity mContext, Bundle mBundle) {
        Intent intent = new Intent(mContext, MainActivity.class);
        intent.putExtras(mBundle);

        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
        mContext.overridePendingTransition(R.anim.right2middle, R.anim.middle2left);
        mContext.finish();
    }

    private FirebaseAnalytics firebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        main = this;
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        isGPSAvailable = locationServicesEnabled(MyApplication.getInstance());
        //update device
        AppGlobal.updateDevice(MainActivity.this);

        navHeaderMainBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.nav_header_main, binding.navView, false);
        binding.navView.addHeaderView(navHeaderMainBinding.getRoot());
        binding.navView.setItemIconTintList(null);
        binding.navView.setVerticalScrollBarEnabled(false);

        appPreferences = MyApplication.getInstance().getAppPreferences();

        try {

            FirebaseMessaging.getInstance().subscribeToTopic("rider_" + appPreferences.getUserInfo().userid);
            FirebaseMessaging.getInstance().subscribeToTopic("rider");
            FirebaseMessaging.getInstance().subscribeToTopic("android");
            FirebaseMessaging.getInstance().subscribeToTopic("android_rider");

        } catch (Exception e) {
        }

        navHeaderMainBinding.setUserInfo(appPreferences.getUserInfo());
        navHeaderMainBinding.setAppPreference(appPreferences);
        binding.setUserInfo(appPreferences.getUserInfo());
        navHeaderMainBinding.ivProfile.setOnClickListener(this);

        if (appPreferences.getUserInfo() != null) {

            mLocationService = new LocationService(getApplicationContext());
            mServiceIntent = new Intent(getApplicationContext(), mLocationService.getClass());

            // HienNH
            // Start My Location Service
            Intent myLocation = new Intent(getApplicationContext(), MyLocation.class);
            startService(myLocation);

            if (MyApplication.getInstance().getAppPreferences().isLogin() && MyApplication.getInstance().getAppPreferences().isVerify()) {

                if (!isMyServiceRunning(mLocationService.getClass())) {
                    mServiceIntent.setAction(AppConstant.ACTION.STARTFOREGROUND_ACTION);
                    startService(mServiceIntent);
                    LocationService.IS_SERVICE_RUNNING = true;
                }

            }

        }

        setSupportActionBar(binding.layoutAppbar.toolbar);

        manageFragments(0);

        init();

        sendLocation();

        updateMenuStatus();

        if (!appPreferences.isOnline()) {
            showGoOnlineDialog();
        } else {
            goOnline();
        }

        if (getIntent().hasExtra(NOTIFICATION_MSG)) {

            AppPreferences appPreferences = MyApplication.getInstance().getAppPreferences();
            appPreferences.setNotificationBedgeInc();

            String notificationMessage = getIntent().getStringExtra(NOTIFICATION_MSG);

            if (notificationMessage.length() > 0) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                builder1.setTitle(R.string.bungkusitTeamMsg);
                builder1.setMessage(Html.fromHtml(notificationMessage));
                builder1.setCancelable(true);
                builder1.setPositiveButton(
                        "Đồng ý",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        }


        // Obtain the Firebase Analytics instance.
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        if (appPreferences.getUserInfo() != null) {

            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, appPreferences.getUserInfo().userid);
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Home Screen");


            //Logs an app event.
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

            //Sets whether analytics collection is enabled for this app on this device.
            firebaseAnalytics.setAnalyticsCollectionEnabled(true);

            //Sets the minimum engagement time required before starting a session. The default value is 10000 (10 seconds). Let's make it 20 seconds just for the fun
            firebaseAnalytics.setMinimumSessionDuration(20000);

            //Sets the duration of inactivity that terminates the current session. The default value is 1800000 (30 minutes).
            firebaseAnalytics.setSessionTimeoutDuration(500);

            //Sets the user ID property.
            firebaseAnalytics.setUserId(appPreferences.getUserInfo().userid);

            //Sets a user property to a given value.
            firebaseAnalytics.setUserProperty("firstname", appPreferences.getUserInfo().firstname);
        }

        if (getIntent().hasExtra(EXTRA_ACTIVITY_ACTION)) {
            switch(getIntent().getStringExtra(EXTRA_ACTIVITY_ACTION)) {
                case "editPasswordSuccess":
                    binding.drawerLayout.openDrawer(GravityCompat.START);
                    AppGlobal.showToast(this, getLayoutInflater(), R.string.change_password_success, AppGlobal.TOAST_LENGTH_SHORT);
                    break;
                default:
                    break;
            }
        }
    }

    private void init() {
        setSupportActionBar(binding.layoutAppbar.toolbar);
        appPreferences = MyApplication.getInstance().getAppPreferences();

        MyApplication.getInstance().setConnectivityListener(this);

        presenter = new MainPresenterImpl(this);

        toggle = new ActionBarDrawerToggle(this, binding.drawerLayout, binding.layoutAppbar.toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        binding.drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        setDrawerEnabled(true);
        binding.layoutAppbar.toolbar.setNavigationIcon(R.drawable.ic_menu_white);

        binding.layoutAppbar.toolbar.setNavigationOnClickListener(onClickListener);



        binding.navView.setNavigationItemSelectedListener(this);
        binding.layoutAppbar.viewOffline.setOnClickListener(null);
        binding.layoutAppbar.llToggleOnline.setOnClickListener(this);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override


            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {

                    String notificationMessage = intent.getStringExtra("message");

                    try {

                        JSONObject notificationData = new JSONObject(notificationMessage);

                        if (notificationData.has("mtype")) {
                            String mType = notificationData.getString("mtype");

                            if (mType.equals(getString(R.string.type_neworder))) {
                                JSONObject data = notificationData.getJSONObject("data");
                                if (data.has("message")) {
                                    MyApplication.getInstance().getBus().post("message=neworder");
                                }
                            } else if (mType.equals(getString(R.string.type_assign_by_user))) {
//                                manageFragments(1);
                                TabLayout.Tab tab = binding.layoutAppbar.tablayout.getTabAt(1);
                                tab.select();
                                NewOrderFragment newOrderFragment = (NewOrderFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + 1);
                                if (newOrderFragment != null) {
                                    JSONObject data = notificationData.getJSONObject("data");
                                    newOrderFragment.order_id = data.getString("order_id");
                                    newOrderFragment.showAcceptedDialog();
                                }
                            }

                            if (mType.equals(getString(R.string.type_cancelorder))) {

                                JSONObject data = notificationData.getJSONObject("data");

                                if (data.has("message")) {
                                    String title = data.getString("message");
                                    AppGlobal.displayAlertDilog(MainActivity.this, title);
                                }

                            }
                        }


                    } catch (JSONException e) {


                        if (notificationMessage.length() > 0) {
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                            builder1.setTitle(R.string.bungkusitTeamMsg);
                            builder1.setMessage(Html.fromHtml(notificationMessage));
                            builder1.setCancelable(true);
                            builder1.setPositiveButton(
                                    "Đồng ý",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                        }
                        e.printStackTrace();
                    }
                }
            }
        };



        if (getIntent().hasExtra("order_action")) {
            String action = getIntent().getStringExtra("order_action");
            if (action.equals("complete")) {
                binding.layoutAppbar.tablayout.getTabAt(2).select();
                final Dialog dialog = new Dialog(this);
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.dialog_completed_order);
                dialog.setCanceledOnTouchOutside(false);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                Window window = dialog.getWindow();
                lp.copyFrom(window.getAttributes());

                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
                TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
                tvClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        }

        if (getIntent().hasExtra("active_tab")) {
            Integer tab = getIntent().getIntExtra("active_tab", 0);
            binding.layoutAppbar.tablayout.getTabAt(tab).select();
        }
    }


    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            if (!binding.layoutAppbar.avi.isShown()) {
                if (navigationState != 1)
                    onBackPressed();
                else {
                    binding.drawerLayout.openDrawer(GravityCompat.START);
                }
            }

        }
    };

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llToggleOnline:
                if (!appPreferences.isAssigned()) {
                    if (appPreferences.isOnline()) {
                        showGoOfflineDialog();
                    } else {
                        showGoOnlineDialog();
                    }
                } else {
                    final Dialog dialog = new Dialog(this);
                    dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.setContentView(R.layout.dialog_is_assigned_online);
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.setCancelable(true);
                    TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = dialog.getWindow();
                    lp.copyFrom(window.getAttributes());

                    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                    tvClose.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                }
                break;
            case R.id.ivProfile:
                MyProfileActivity.startActivity(MainActivity.this, new Bundle());
                if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    binding.drawerLayout.closeDrawer(GravityCompat.START);
                }
                break;
        }
    }

    private void sendLocation() {
        permissionUtils = new PermissionUtils(this);
        permissionUtils.requestPermissions(REQUEST_LOCATION_PERMISSION, PermissionUtils.REQUEST_CODE_LOCATION_PERMISSION);

    }

    @Override
    public void onBackPressed() {

        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            if (!binding.layoutAppbar.avi.isShown()) {
                if (!isStackEmpty()) {
                    super.onBackPressed();
                    return;
                }
            }
        }
    }

    @Override
    public void onPermissionGranted(int requestCode) {
        switch (requestCode) {
            case PermissionUtils.REQUEST_CODE_LOCATION_PERMISSION:
                provider = new LocationGooglePlayServicesProvider();
                startTimer();
                break;
        }

    }

    @Override
    public void onPermissionDenied(int requestCode) {
        switch (requestCode) {

            case PermissionUtils.REQUEST_CODE_LOCATION_PERMISSION:

                Debug.e("permission", "you denied location permission");
                AppGlobal.showSnakeBar(getApplicationContext(), binding.drawerLayout, getString(R.string.locationpermissiondenied));

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(R.string.requirelocationpermissin);
                builder.setMessage(R.string.apprequireslocation);
                builder.setCancelable(false);
                builder.setPositiveButton(
                        "Đồng ý",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //  permissionUtils = new PermissionUtils(MainActivity.this);
                                permissionUtils.requestPermissions(PermissionUtils.REQUEST_LOCATION_PERMISSION, PermissionUtils.REQUEST_CODE_LOCATION_PERMISSION);
                                dialog.cancel();
                            }
                        });

                builder.setNegativeButton("Đóng App", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });

                AlertDialog alert1 = builder.create();
                alert1.show();

                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionUtils.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onResponseSuccess(Object success, int requestCode) {
        if (requestCode == REQUEST_CODE_2 && success instanceof ChangeStatusResp) {
            if (((ChangeStatusResp) success).success == 0) {
                toggleStatus();
            }
            updateMenuStatus();
        } else if (requestCode == REQUEST_CODE_4 && success instanceof CommonResp) {
            appPreferences = MyApplication.getInstance().getAppPreferences();
            appPreferences.setLogin(false);
            appPreferences.setUserInfo(null);
            appPreferences.setToken("");
            appPreferences.setSecretLogId("");

            mLocationService = new LocationService(getApplicationContext());
            mServiceIntent = new Intent(getApplicationContext(), mLocationService.getClass());

            if (isMyServiceRunning(mLocationService.getClass())) {
                mServiceIntent.setAction(AppConstant.ACTION.STOPFOREGROUND_ACTION);
                startService(mServiceIntent);
                LocationService.IS_SERVICE_RUNNING = false;
            }


            LoginActivity.startActivity(MainActivity.this, new Bundle());
            finish();

        } else if (requestCode == REQUEST_CODE_5 && success instanceof CommonResp) {
            try {
                int iVersionCode = Integer.parseInt(((CommonResp) success).riderapp);

                PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);

                if (iVersionCode > pInfo.versionCode) {
                    updateVersionDialog();
                }

            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        } else if (requestCode == REQUEST_CODE_6 && success instanceof NotificationResp) {
            notificationResp = (NotificationResp) success;
            AppPreferences appPreferences = MyApplication.getInstance().getAppPreferences();
            appPreferences.setNotificationBedgeInc(Integer.parseInt(notificationResp.count));

        } else if (requestCode == REQUEST_CODE_7 && success instanceof CommonResp) {

        }
    }

    @Override
    public void onResponseFailure(Object error, int requestCode) {

        if (error instanceof LoginResp) {
            AppGlobal.showSnakeBar(getApplicationContext(), binding.drawerLayout, ((LoginResp) error).message);
        } else if (error instanceof String) {
            AppGlobal.showSnakeBar(getApplicationContext(), binding.drawerLayout, error.toString());
        }
    }

    @Override
    public void onRequestCall() {
        binding.drawerLayout.setEnabled(false);
    }

    @Override
    public void onRequestCallBack(boolean isAuthenticationFailed) {
        binding.drawerLayout.setEnabled(true);

        if (isAuthenticationFailed)
            setAuthenticationFailed();
    }

    private boolean isStackEmpty() {
        boolean isPopFragment = false;
        if (AppConstant.currentFragment == null || binding.layoutAppbar.cvNoInternet.isShown())
            return isPopFragment;

        return isPopFragment;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        final int id = item.getItemId();

        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START) && id != R.id.nav_logout) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        }
        if (id == R.id.nav_home) {
            manageFragments(0);
        } else if (id == R.id.nav_my_profile) {
            MyProfileActivity.startActivity(MainActivity.this, new Bundle());
        } else if (id == R.id.nav_messages) {
            Bundle bundle = new Bundle();
            bundle.putString("toId", "1");
            bundle.putString("fromId", appPreferences.getUserInfo().userid);
            bundle.putString("firstname", "");
            bundle.putString("order_id", "");
            ChatConversationActivity.startActivity(this, bundle);
        } else if (id == R.id.nav_feedback) {
            FeedbackActivity.startActivity(MainActivity.this, new Bundle());
        } else if (id == R.id.nav_change_pass) {
            if (appPreferences.isAssigned()) {
                showIsAssignedDialog();
            } else {
                ChangePasswordActivity.startActivity(MainActivity.this, new Bundle());
            }
        } else if (id == R.id.nav_logout) {
            if (appPreferences.isAssigned()) {
                showIsAssignedDialog();
            } else {
                final AlertLogoutBinding alertBinding =
                        DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.alert_logout, null, false);
                final AlertDialog.Builder alertDialogBuilder
                        = new AlertDialog.Builder(this);
                alertDialogBuilder.setView(alertBinding.getRoot());
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                alertDialog.setCanceledOnTouchOutside(false);
                alertDialog.show();
                alertBinding.tvExit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                        logoutAPI();
                        binding.drawerLayout.closeDrawer(GravityCompat.START);
                    }
                });
                alertBinding.tvCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
            }

        }

        return true;
    }
    public int dp2px(float dipValue) {
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }
    public void manageFragments(int position) {

        switch (position) {
            case 0:
                setTitle(getResources().getString(R.string.all_order));
                fContainer = null;
                setTabVisibility(true);
                ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
                adapter.addFragment(NewOrderFragment.newInstance(getString(R.string.newf)), getString(R.string.newf));
                adapter.addFragment(NewOrderFragment.newInstance(getString(R.string.assigned)), getString(R.string.assigned));
                adapter.addFragment(NewOrderFragment.newInstance(getString(R.string.completed)), getString(R.string.completed));
                binding.layoutAppbar.rlMain.setVisibility(View.GONE);
                binding.layoutAppbar.tablayout.setVisibility(View.VISIBLE);
                binding.layoutAppbar.viewPager.setVisibility(View.VISIBLE);
                binding.layoutAppbar.viewPager.setAdapter(adapter);
                binding.layoutAppbar.viewPager.setOffscreenPageLimit(3);
                binding.layoutAppbar.tablayout.setupWithViewPager(binding.layoutAppbar.viewPager);

                for(int i=0; i < binding.layoutAppbar.tablayout.getTabCount(); i++) {
                    View tab = ((ViewGroup) binding.layoutAppbar.tablayout.getChildAt(0)).getChildAt(i);
                    ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
                    if (i == 0) {
                        p.setMargins(dp2px(14), 5, 0, 5);
                    } else if (i==(binding.layoutAppbar.tablayout.getTabCount()-1)) {
                        p.setMargins(0, 5, dp2px(14), 5);
                    } else {
                        p.setMargins(30, 5, 30, 5);

                    }
                    tab.setPadding(0, 0, 0, 0);
                    tab.requestLayout();
                }

                binding.layoutAppbar.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });

                binding.layoutAppbar.tablayout.setOnTabSelectedListener(
                        new TabLayout.ViewPagerOnTabSelectedListener(binding.layoutAppbar.viewPager) {
                            @Override
                            public void onTabSelected(TabLayout.Tab tab) {
                                super.onTabSelected(tab);

                                LinearLayout tabLayout = (LinearLayout)((ViewGroup) binding.layoutAppbar.tablayout.getChildAt(0)).getChildAt(tab.getPosition());
                                TextView tabTextView = (TextView) tabLayout.getChildAt(1);
                                Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/SF-Pro-Display-Medium.otf");
                                tabTextView.setTypeface(typeface);
                                reloadOrders();
                            }

                            @Override
                            public void onTabUnselected(TabLayout.Tab tab) {
                                LinearLayout tabLayout = (LinearLayout)((ViewGroup) binding.layoutAppbar.tablayout.getChildAt(0)).getChildAt(tab.getPosition());
                                TextView tabTextView = (TextView) tabLayout.getChildAt(1);
                                Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/SF-Pro-Display-Regular.otf");
                                tabTextView.setTypeface(typeface, Typeface.NORMAL);
                            }


                        });
                break;
        }

        if (fContainer != null) {
            FragmentTransaction fTrans = getSupportFragmentManager().beginTransaction();
            fTrans.replace(R.id.act_main_flContainer, fContainer, AppConstant.currentFragment);
            fTrans.commitAllowingStateLoss();
        }
    }

    public void setTabVisibility(boolean flag) {
        binding.layoutAppbar.tablayout.setVisibility(flag ? View.VISIBLE : View.GONE);
        binding.layoutAppbar.viewPager.setVisibility(flag ? View.VISIBLE : View.GONE);
        binding.layoutAppbar.actMainFlContainer.setVisibility(flag ? View.GONE : View.VISIBLE);
    }

    public void toggleActionBarIcon(int state) {

        AppGlobal.hideKeyboard(MainActivity.this);
        if (navigationState != state) {
            navigationState = state;

            if (state == 0) {
                binding.layoutAppbar.toolbar.setNavigationIcon(R.drawable.back_ic);
                isDrawerLock(true);
            } else {
                binding.layoutAppbar.toolbar.setNavigationIcon(R.drawable.ic_menu_white);
                isDrawerLock(false);
            }
        }
    }

    public void isDrawerLock(boolean flag) {
        if (flag)
            binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        else
            binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    public void setTitle(String title) {
        binding.layoutAppbar.toolbar.setTitle(title);
    }

    public void setAuthenticationFailed() {

        AppPreferences appPreferences = MyApplication.getInstance().getAppPreferences();

        if (timeManager != null) {
            timeManager.cancel();
        }

        Toast.makeText(getApplicationContext(), getString(R.string.error_authentication), Toast.LENGTH_LONG).show();

        appPreferences.setLogin(false);
        appPreferences.setUserInfo(null);
        appPreferences.setToken("");
        appPreferences.setSecretLogId("");

        mLocationService = new LocationService(getApplicationContext());
        mServiceIntent = new Intent(getApplicationContext(), mLocationService.getClass());

        if (isMyServiceRunning(mLocationService.getClass())) {
            mServiceIntent.setAction(AppConstant.ACTION.STOPFOREGROUND_ACTION);
            startService(mServiceIntent);
            LocationService.IS_SERVICE_RUNNING = false;
        }

        LoginActivity.startActivity(MainActivity.this, new Bundle());
        finish();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (!isConnected) {
            binding.layoutAppbar.cvNoInternet.setVisibility(View.VISIBLE);
            binding.layoutAppbar.actMainFlContainer.setVisibility(View.GONE);
        } else {
            binding.layoutAppbar.cvNoInternet.setVisibility(View.GONE);
            binding.layoutAppbar.actMainFlContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_notification, menu);
        this.menu = menu;

        this.menuItem0 = menu.findItem(R.id.action_online_offline);
        this.menuItem1 = menu.findItem(R.id.action_notification);

        View notificationBellactionView = MenuItemCompat.getActionView(menuItem1);

        textCartItemCount = notificationBellactionView.findViewById(R.id.cart_badge);
        if (appPreferences.getNotificationBedge() > 0) {
            textCartItemCount.setText("" + appPreferences.getNotificationBedge());
            textCartItemCount.setVisibility(View.VISIBLE);
        } else {
            textCartItemCount.setText("" + appPreferences.getNotificationBedge());
        }

        notificationBellactionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NotificationActivity.startActivity(MainActivity.this, new Bundle());
            }
        });

        updateMenuStatus();

        return true;
    }

    public void showGoOnlineDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_go_online);
        TextView tvGoOnline = (TextView) dialog.findViewById(R.id.tvGoOnline);
        TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        tvGoOnline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                goOnline();
            }
        });
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    public void showGoOfflineDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_go_offline);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        TextView tvGoOffline = (TextView) dialog.findViewById(R.id.tvGoOffline);
        TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        tvGoOffline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                goOffline();

            }
        });
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_online_offline:


                if (appPreferences.isOnline()) {

                    if (appPreferences.isAssigned()) {
                        //do nothing do not allow to go offline while order is assigned
                    } else {
                        appPreferences.setOnline(false);
                        navHeaderMainBinding.setAppPreference(appPreferences);
                        showGoOfflineDialog();
                    }
                } else {
                    showGoOnlineDialog();
                }

                break;
        }
        return true;
    }

    private void toggleStatus() {
        if (appPreferences.isOnline()) {
            appPreferences.setOnline(false);
            navHeaderMainBinding.setAppPreference(appPreferences);
            showGoOnlineDialog();
        } else {
//            binding.layoutAppbar.viewOffline.setVisibility(View.GONE);
            navHeaderMainBinding.setAppPreference(appPreferences);
            appPreferences.setOnline(true);
        }
        updateMenuStatus();
    }


    private void updateMenuStatus() {
        if (menu != null) {
            MenuItem statusMenuItem = menu.findItem(R.id.action_online_offline);
            if (appPreferences.isOnline()) {
                statusMenuItem.setIcon(R.drawable.online_switch);
            } else {
                statusMenuItem.setIcon(R.drawable.offline_switch);
            }
        }
    }

    private void callAPIForChangeStatus(Boolean value) {

        appPreferences = MyApplication.getInstance().getAppPreferences();

        if (appPreferences.getUserInfo() != null) {

            if (AppGlobal.isNetwork(getApplicationContext())) {

                if (appPreferences.isLogin()) {

                    HashMap<String, Object> request = new HashMap<>();
                    if (value) {
                        request.put("status", "online");
                    } else {
                        request.put("status", "offline");
                    }

                    HashMap<String, Object> auth = new HashMap<>();
                    auth.put("id", appPreferences.getUserInfo().userid);
                    auth.put("token", appPreferences.getToken());

                    HashMap<String, Object> map = new HashMap<>();
                    map.put(getResources().getString(R.string.service), getResources().getString(R.string.change_live_status));
                    map.put(getResources().getString(R.string.request), request);
                    map.put("auth", auth);

                    Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

                    presenter.changeStatus(requestService, new ChangeStatusResp(), REQUEST_CODE_2);
                }
            } else
                AppGlobal.showSnakeBar(getApplicationContext(), binding.drawerLayout, getString(R.string.msg_no_internet));

        }


    }

    public void logoutAPI() {


        appPreferences = MyApplication.getInstance().getAppPreferences();

        if (appPreferences.getUserInfo() != null) {

            if (AppGlobal.isNetwork(getApplicationContext())) {

                if (appPreferences.isLogin()) {

                    HashMap<String, Object> request = new HashMap<>();

                    request.put("secret_log_id", appPreferences.getSecretLogId());

                    HashMap<String, Object> auth = new HashMap<>();
                    auth.put("id", appPreferences.getUserInfo().userid);
                    auth.put("token", appPreferences.getToken());

                    HashMap<String, Object> map = new HashMap<>();
                    map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_logout));
                    map.put(getResources().getString(R.string.request), request);
                    map.put("auth", auth);

                    Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

                    presenter.changeStatus(requestService, new CommonResp(), REQUEST_CODE_4);
                }
            } else
                AppGlobal.showSnakeBar(getApplicationContext(), binding.drawerLayout, getString(R.string.msg_no_internet));

        }


    }


    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        MyApplication.getInstance().getBus().unregister(this);

    }

    public void setNotificationCounter() {
        if (textCartItemCount != null) {
            if (appPreferences.getNotificationBedge() > 0) {
                textCartItemCount.setText("" + appPreferences.getNotificationBedge());
                textCartItemCount.setVisibility(View.VISIBLE);
            } else {
                textCartItemCount.setText("" + appPreferences.getNotificationBedge());
            }
        }
    }

    public void setDecreaseMessageCounter(int iMinus) {

        if (iMinus != 0) {
            View view = binding.navView.getMenu().findItem(R.id.nav_messages).getActionView();

            TextViewRegular tvMessageCounter = view.findViewById(R.id.tvMessageCount);
            String vCount = tvMessageCounter.getText().toString();
            if (vCount.length() == 0)
                vCount = "0";
            int iCount = Integer.parseInt(vCount);
            if (iCount >= iMinus)
                iCount -= iMinus;

            saveMessageCounter(iCount);
            tvMessageCounter.setText(String.valueOf(iCount));
        }
    }

    private void saveMessageCounter(int iCount) {

        if (appPreferences.getUserInfo() != null) {
            UserInfo userInfo = appPreferences.getUserInfo();
            userInfo.totalunread_msgs = String.valueOf(iCount);
            appPreferences.setUserInfo(userInfo);
        }


    }

    public static MainActivity getMainActivity() {
        return main;
    }


    private void startTimer() {

        timeManager = new TimeManager(5000, -1, 101);
        timeManager.setTimerListner(this);
        timeManager.start();
    }

    @Override
    public void onTimerResume(int type) {

    }

    @Override
    public void onTimerPause(int type) {

    }

    @Override
    public void onTimerFinish(int type) {

    }

    @Override
    public void onTimerTick(int type) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                appPreferences = MyApplication.getInstance().getAppPreferences();
                isGPSAvailable = locationServicesEnabled(MyApplication.getInstance());
//                Debug.e("Tag", "Mainactivity timer=> running" + appPreferences.isAssigned());

                setNotificationCounter();

                if (!SmartLocation.with(MainActivity.this).location().state().locationServicesEnabled() || !isGPSAvailable) {
                    AppGlobal.openLocationDialog(MainActivity.this);
                    timeManager.pause();
                } else {
//                    ViewGroup vg = (ViewGroup) binding.layoutAppbar.tablayout.getChildAt(0);
//                    if (appPreferences.isAssigned()) {
////                        vg.getChildAt(0).setEnabled(false);
////                        vg.getChildAt(2).setEnabled(false);
//                        binding.layoutAppbar.tablayout.getTabAt(1).select();
////                        binding.layoutAppbar.viewPager.setPagingEnabled(false);
//                    }
//                    vg.getChildAt(0).setEnabled(true);
//                    vg.getChildAt(2).setEnabled(true);
//                    binding.layoutAppbar.viewPager.setPagingEnabled(true);
                }
            }
        });


    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {

        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Debug.e("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Debug.e("isMyServiceRunning?", false + "");
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cancelTimer();
    }

    private void cancelTimer() {
        if (timeManager != null) {
            timeManager.cancel();
            timeManager.setTimerListner(null);
            timeManager = null;
        }
    }

    private void checkVersionAPI() {

        if (AppGlobal.isNetwork(getApplicationContext())) {

            MainPresenter presenter = new MainPresenterImpl(this);

            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_check_version));

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

            presenter.checkVersion(requestService, new CommonResp(), REQUEST_CODE_5);
        }
    }


    private void updateVersionDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_new_version);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        TextView tvSubmit = (TextView) dialog.findViewById(R.id.tvSubmit);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String appPackageName = MainActivity.this.getPackageName(); //
                try {
                    MainActivity.this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    MainActivity.this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });

        dialog.show();
    }

    public void setDrawerEnabled(boolean enabled) {

        if (enabled) {
            binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            toggle.onDrawerStateChanged(DrawerLayout.LOCK_MODE_UNLOCKED);
            binding.layoutAppbar.toolbar.setNavigationOnClickListener(onClickListener);
            binding.navView.setVisibility(View.VISIBLE);
        } else {
            binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            toggle.onDrawerStateChanged(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            binding.layoutAppbar.toolbar.setNavigationOnClickListener(null);
            binding.navView.setVisibility(View.GONE);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        setDrawerEnabled(true);

        isGPSAvailable = locationServicesEnabled(MyApplication.getInstance());

        appPreferences = MyApplication.getInstance().getAppPreferences();

        MyApplication.getInstance().getBus().register(this);


        navHeaderMainBinding.setUserInfo(appPreferences.getUserInfo());
        navHeaderMainBinding.setAppPreference(appPreferences);

        if (SmartLocation.with(MainActivity.this).location().state().locationServicesEnabled() || isGPSAvailable) {
            if (timeManager != null) {
                timeManager.resume();
            }

            if (appPreferences.getUserInfo() != null) {

                binding.setUserInfo(appPreferences.getUserInfo());
                mLocationService = new LocationService(getApplicationContext());
                mServiceIntent = new Intent(getApplicationContext(), mLocationService.getClass());

                if (MyApplication.getInstance().getAppPreferences().isLogin()) {

                    if (!isMyServiceRunning(mLocationService.getClass())) {
                        mServiceIntent.setAction(AppConstant.ACTION.STARTFOREGROUND_ACTION);
                        startService(mServiceIntent);
                        LocationService.IS_SERVICE_RUNNING = true;
                    }

                }

            }


        } else {

            if (timeManager != null) {
                timeManager.pause();
            }

            AppGlobal.openLocationDialog(MainActivity.this);


        }

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));


        setNotificationCounter();
        manageFragments(0);

        if (appPreferences.isAssigned()) {
            binding.layoutAppbar.tablayout.getTabAt(1).select();
        } else {
            binding.layoutAppbar.tablayout.getTabAt(0).select();
        }

        if (appPreferences.isOnline()) {
            goOnline();
        } else {
            goOffline();
        }

        checkVersionAPI();
    }

    private void reloadOrders() {
        NewOrderFragment newOrderFragment = (NewOrderFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPager + ":" + binding.layoutAppbar.tablayout.getSelectedTabPosition());
        if (newOrderFragment != null) {
            newOrderFragment.getOrderApi();
        }
    }

    private void goOnline() {
        binding.layoutAppbar.appBarLayout.setBackgroundResource(R.drawable.bg_blue_round_bottom_small);
        binding.layoutAppbar.ivToggleOnline.setImageResource(R.drawable.ic_toogle_on_01);
        binding.layoutAppbar.tvOnlineStatus.setText(R.string.is_online);
        navHeaderMainBinding.setAppPreference(appPreferences);
        appPreferences.setOnline(true);
        callAPIForChangeStatus(true);
        TextViewRegular tvEmptyOrder = binding.layoutAppbar.viewPager.findViewById(R.id.tvEmptyOrder);
        if (tvEmptyOrder != null)
            tvEmptyOrder.setText(R.string.no_order);
        binding.layoutAppbar.tvOnlineString.setText(R.string.open);
        ImageView ivEmpty = binding.layoutAppbar.viewPager.findViewById(R.id.ivEmpty);
        if (ivEmpty != null) {
            ivEmpty.setImageResource(R.drawable.idea);
        }
        reloadOrders();
    }

    private void goOffline() {
        navHeaderMainBinding.setAppPreference(appPreferences);
        binding.layoutAppbar.appBarLayout.setBackgroundResource(R.drawable.bg_red_round_bottom_small);
        binding.layoutAppbar.ivToggleOnline.setImageResource(R.drawable.ic_toogle_off_01);
        binding.layoutAppbar.tvOnlineStatus.setText(R.string.is_offline);
        binding.layoutAppbar.tvOnlineString.setText(R.string.off);
        appPreferences.setOnline(false);
        callAPIForChangeStatus(false);
        TextViewRegular tvEmptyOrder = binding.layoutAppbar.viewPager.findViewById(R.id.tvEmptyOrder);
        if (tvEmptyOrder != null) {
            if (appPreferences.isAssigned()) {
                tvEmptyOrder.setText(R.string.no_order_is_assigned);
            } else {
                tvEmptyOrder.setText(R.string.is_offline_message);
            }
        }
        ImageView ivEmpty = binding.layoutAppbar.viewPager.findViewById(R.id.ivEmpty);
        if (ivEmpty != null) {
            ivEmpty.setImageResource(R.drawable.eraser);
        }
        reloadOrders();
    }

    private void showIsAssignedDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_is_assigned);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        TextView tvClose = (TextView) dialog.findViewById(R.id.tvClose);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void kickoutAPI() {


        appPreferences = MyApplication.getInstance().getAppPreferences();

        if (appPreferences.getUserInfo() != null) {

            if (AppGlobal.isNetwork(getApplicationContext())) {

                if (appPreferences.isLogin()) {

                    HashMap<String, Object> request = new HashMap<>();

                    request.put("secret_log_id", appPreferences.getSecretLogId());

                    HashMap<String, Object> auth = new HashMap<>();
                    auth.put("id", appPreferences.getUserInfo().userid);
                    auth.put("token", appPreferences.getToken());

                    HashMap<String, Object> map = new HashMap<>();
                    map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_logout));
                    map.put(getResources().getString(R.string.request), request);
                    map.put("auth", auth);

                    Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

                    presenter.changeStatus(requestService, new CommonResp(), REQUEST_CODE_7);
                    appPreferences = MyApplication.getInstance().getAppPreferences();
                    appPreferences.setLogin(false);
                    appPreferences.setUserInfo(null);
                    appPreferences.setToken("");
                    appPreferences.setSecretLogId("");

                    mLocationService = new LocationService(getApplicationContext());
                    mServiceIntent = new Intent(getApplicationContext(), mLocationService.getClass());

                    if (isMyServiceRunning(mLocationService.getClass())) {
                        mServiceIntent.setAction(AppConstant.ACTION.STOPFOREGROUND_ACTION);
                        startService(mServiceIntent);
                        LocationService.IS_SERVICE_RUNNING = false;
                    }

                    Bundle bundle = new Bundle();
                    bundle.putString(LoginActivity.EXTRA_IS_KICKED_OUT, "1");

                    LoginActivity.startActivity(MainActivity.this, bundle);
                    finish();
                }
            } else
                AppGlobal.showSnakeBar(getApplicationContext(), binding.drawerLayout, getString(R.string.msg_no_internet));

        }


    }
}



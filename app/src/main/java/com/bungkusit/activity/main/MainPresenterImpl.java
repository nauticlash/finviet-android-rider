package com.bungkusit.activity.main;

import android.app.Activity;

import com.bungkusit.webservices.interfaces.AppInteractor;
import com.bungkusit.webservices.interfaces.AppInteractorImpl;
import com.bungkusit.webservices.interfaces.OnApiCompleteListener;

import retrofit2.Call;

public class MainPresenterImpl implements MainPresenter {

    private OnApiCompleteListener onApiCompleteListener;
    private AppInteractor appInteractor;
    private Activity mActivity;

    public MainPresenterImpl(Activity mActivity) {

        this.mActivity = mActivity;
        this.onApiCompleteListener = (OnApiCompleteListener) mActivity;
        this.appInteractor = new AppInteractorImpl();
    }

    @Override
    public void updateLocation(Call<String> requestService, Object respObject, int requestCode) {
        appInteractor.doCallAPI(mActivity, requestService, respObject, requestCode, onApiCompleteListener);
    }

    @Override
    public void changeStatus(Call<String> requestService, Object respObject, int requestCode) {
        appInteractor.doCallAPI(mActivity, requestService, respObject, requestCode, onApiCompleteListener);
    }

    @Override
    public void checkVersion(Call<String> requestService, Object respObject, int requestCode) {
        appInteractor.doCallAPI(mActivity, requestService, respObject, requestCode, onApiCompleteListener);
    }

    @Override
    public void getNotification(Call<String> requestService, Object respObject, int requestCode) {
        appInteractor.doCallAPI(mActivity, requestService, respObject, requestCode, onApiCompleteListener);
    }

    @Override
    public void editProfile(Call<String> requestService, Object respObject, int requestCode) {
        appInteractor.doCallAPI(mActivity, requestService, respObject, requestCode, onApiCompleteListener);
    }
}
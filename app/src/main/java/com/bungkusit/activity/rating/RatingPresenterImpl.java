package com.bungkusit.activity.rating;

import android.app.Activity;
import android.content.Context;

import com.bungkusit.webservices.interfaces.AppInteractor;
import com.bungkusit.webservices.interfaces.AppInteractorImpl;
import com.bungkusit.webservices.interfaces.OnApiCompleteListener;

import retrofit2.Call;

public class RatingPresenterImpl implements RatingPresenter {

    private OnApiCompleteListener onApiCompleteListener;
    private AppInteractor appInteractor;
    private Context mContext;

    public RatingPresenterImpl(Activity activity) {

        this.onApiCompleteListener = (OnApiCompleteListener) activity;
        this.appInteractor = new AppInteractorImpl();
    }

    @Override
    public void uploadImages(Call<String> requestService, Object respObject, int requestCode) {
        appInteractor.doCallAPI(mContext, requestService, respObject, requestCode, onApiCompleteListener);
    }

    @Override
    public void giveRating(Call<String> requestService, Object respObject, int requestCode) {
        appInteractor.doCallAPI(mContext, requestService, respObject, requestCode, onApiCompleteListener);
    }

}

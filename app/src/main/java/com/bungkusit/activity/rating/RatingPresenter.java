package com.bungkusit.activity.rating;

import retrofit2.Call;

public interface RatingPresenter {

    void giveRating(Call<String> requestService, Object respObject, int requestCode);
    void uploadImages(Call<String> requestService, Object respObject, int requestCode);

}

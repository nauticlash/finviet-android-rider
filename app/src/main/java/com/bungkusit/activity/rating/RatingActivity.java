package com.bungkusit.activity.rating;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.bungkusit.R;
import com.bungkusit.model.CommonResp;
import com.bungkusit.utils.AppConstant;
import com.bungkusit.utils.AppGlobal;
import com.bungkusit.utils.AppPreferences;
import com.bungkusit.utils.Debug;
import com.bungkusit.utils.MyApplication;
import com.bungkusit.webservices.interfaces.OnApiCompleteListener;

import java.io.File;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

import static com.bungkusit.utils.AppConstant.REQUEST_CODE_1;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_2;

public class RatingActivity extends AppCompatActivity implements View.OnClickListener, OnApiCompleteListener {

    private RatingPresenter presenter;
    private AppPreferences appPreferences;
    private String orderId, riderId, userId, profilePath, name, signaturePath, signatureUrl;

    public static void startActivity(Activity mContext, Bundle mBundle) {
        Intent intent = new Intent(mContext, RatingActivity.class);
        intent.putExtras(mBundle);
        mContext.startActivityForResult(intent, AppConstant.REQUEST_CODE_6);
        mContext.overridePendingTransition(R.anim.right2middle, R.anim.middle2left);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appPreferences = MyApplication.getInstance().getAppPreferences();

        Bundle extras = getIntent().getExtras();
        orderId = extras.getString("orderId");
        riderId = extras.getString("riderId");
        userId = extras.getString("userId");
        profilePath = extras.getString("profilePath");

        name = extras.getString("name");
        signaturePath = extras.getString("signaturePath");

        init();
    }


    private void init() {
    }

    @Override
    public void onClick(View view) {
        if (isValid()) {
            uploadSignature();
        }
    }

    private boolean isValid() {
        return true;
    }

    private void uploadSignature() {
        if (AppGlobal.isNetwork(getApplicationContext())) {
            AppGlobal.hideKeyboard(this);

            if (presenter == null)
                presenter = new RatingPresenterImpl(this);

            MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[1];


            if (!signaturePath.contains("http")) {
                File file = new File(signaturePath);
                RequestBody surveyBody = RequestBody.create(MediaType.parse("image/*"), file);
                surveyImagesParts[0] = MultipartBody.Part.createFormData("image", file.getName(), surveyBody);
            }


            Call<String> requestService = MyApplication.getInstance().getAPIListService().callUploadProfileAPI(surveyImagesParts[0]);

            presenter.uploadImages(requestService, new CommonResp(), REQUEST_CODE_1);

        }
    }


    private void callGiveRatingAPI() {

        if (AppGlobal.isNetwork(this)) {

            AppGlobal.hideKeyboard(this);
            AppPreferences appPreferences = MyApplication.getInstance().getAppPreferences();
            if (presenter == null)
                presenter = new RatingPresenterImpl(this);

            HashMap<String, Object> request = new HashMap<>();
            HashMap<String, Object> data = new HashMap<>();

            data.put("orderid", orderId);
            data.put("riderid", riderId);
            data.put("userid", userId);
            data.put("signature_image", signatureUrl);
            data.put("comments", "");

            request.put("data", data);

            HashMap<String, Object> auth = new HashMap<>();
            auth.put("id", appPreferences.getUserInfo().userid);
            auth.put("token", appPreferences.getToken());

            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.give_rating));
            map.put(getResources().getString(R.string.request), request);
            map.put("auth", auth);

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);
            presenter.giveRating(requestService, new CommonResp(), REQUEST_CODE_2);
        }
    }

    @Override
    public void onRequestCall() {
    }

    @Override
    public void onResponseSuccess(Object success, int requestCode) {

        if (requestCode == REQUEST_CODE_1 && success instanceof CommonResp) {
            signatureUrl = ((CommonResp) success).data;
            callGiveRatingAPI();
        }else if (requestCode == REQUEST_CODE_2 && success instanceof CommonResp) {
            appPreferences.setAssigned(false);
            Bundle param = new Bundle();
            finish();
        }
    }

    @Override
    public void onResponseFailure(Object error, int requestCode) {
    }

    @Override
    public void onRequestCallBack(boolean isAuthenticationFailed) {
    }

}

package com.bungkusit.activity.notification;

import retrofit2.Call;

public interface NotificationPresenter {

    void getNotification(Call<String> requestService, Object respObject, int requestCode);
    void readNotification(Call<String> requestService, Object respObject, int requestCode);

}
package com.bungkusit.activity.notification;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.bungkusit.R;
import com.bungkusit.activity.orderDetail.OrderDetailActivity;
import com.bungkusit.adapter.NotificationAdapter;
import com.bungkusit.databinding.ActivityNotificationBinding;
import com.bungkusit.model.CommonResp;
import com.bungkusit.model.NotificationInfo;
import com.bungkusit.model.NotificationResp;
import com.bungkusit.utils.AppGlobal;
import com.bungkusit.utils.AppPreferences;
import com.bungkusit.utils.Debug;
import com.bungkusit.utils.MyApplication;
import com.bungkusit.webservices.interfaces.OnApiCompleteListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;

import static com.bungkusit.utils.AppConstant.REQUEST_CODE_1;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_2;
import static com.bungkusit.utils.AppConstant.REQUEST_CODE_3;

public class NotificationActivity extends AppCompatActivity implements OnApiCompleteListener, View.OnClickListener {

    private ActivityNotificationBinding binding;

    private NotificationPresenter presenter;
    private NotificationResp notificationResp;
    private Boolean isEdit = false;
    NotificationAdapter notificationAdapter;
    boolean isLoading = false;
    Integer page = 0;
    boolean end = false;
    List<NotificationInfo> rowsArrayList = new ArrayList<NotificationInfo>();

    public static void startActivity(Activity mContext, Bundle mBundle) {
        Intent intent = new Intent(mContext, NotificationActivity.class);
        intent.putExtras(mBundle);

        mContext.startActivity(intent);
        mContext.overridePendingTransition(R.anim.right2middle, R.anim.middle2left);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification);

        init();
    }

    private void init() {
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        binding.setAppPreference(MyApplication.getInstance().getAppPreferences());

        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getApplicationContext());

        binding.rvList.setLayoutManager(mLinearLayoutManager);

        binding.swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        Debug.w("onRefresh()", " called from SwipeRefreshLayout");
                        page = 0;
                        rowsArrayList = new ArrayList<NotificationInfo>();
                        callNotificationAPI();
                    }
                }
        );
        binding.ivMark.setOnClickListener(this);
        binding.ivEdit.setOnClickListener(this);

        binding.rvList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading && !end) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == notificationAdapter.getItemCount() - 1) {
                        page += 1;
                        callNotificationAPI();
                        isLoading = true;
                    }
                }
            }
        });


    }

    @Override
    public void onClick(View v) {
        TextView tvClose;
        TextView tvSubmit;
        WindowManager.LayoutParams lp;
        Window window;
        final NotificationInfo notificationItem;
        switch (v.getId()) {
            case R.id.ivEdit:
                if (!isEdit) {
                    binding.ivEdit.setImageDrawable(getResources().getDrawable(R.drawable.ic_notif_edit_on));
                    isEdit = true;
//                    setAdapter();
                } else {
                    binding.ivEdit.setImageDrawable(getResources().getDrawable(R.drawable.ic_notif_edit));
                    isEdit = false;
//                    setAdapter();
                }
                notificationAdapter = new NotificationAdapter(rowsArrayList, this, isEdit);
                binding.rvList.setAdapter(notificationAdapter);
                break;
            case R.id.ivNotificationDelete:
                notificationItem = notificationAdapter.getItem(Integer.parseInt(v.getTag().toString()));
                final Dialog deleteDialog = new Dialog(this);
                deleteDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                deleteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                deleteDialog.setContentView(R.layout.dialog_delete_notification);
                deleteDialog.setCanceledOnTouchOutside(false);
                tvClose = (TextView) deleteDialog.findViewById(R.id.tvClose);
                tvSubmit = (TextView) deleteDialog.findViewById(R.id.tvSubmit);
                lp = new WindowManager.LayoutParams();
                window = deleteDialog.getWindow();
                lp.copyFrom(window.getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
                tvClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        deleteDialog.dismiss();
                    }
                });
                tvSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        deleteDialog.dismiss();
                        deleteNotification(notificationItem);
                    }
                });
                deleteDialog.show();
                break;
            case R.id.llMain:
                notificationItem = notificationAdapter.getItem(Integer.parseInt(v.getTag().toString()));
                readNotificationAPI(notificationItem.id);
                notificationItem.isview = "1";
                notificationAdapter.notifyItemChanged(Integer.parseInt(v.getTag().toString()));
                if (!notificationItem.order_id.equals("0") && notificationItem.order_id != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString(OrderDetailActivity.EXTRA_ORDER_ID, notificationItem.order_id);
                    OrderDetailActivity.startActivity(this, bundle);
//                    finish();
                } else {
                }
                break;
            case R.id.ivMark:
                final Dialog dialog = new Dialog(this);
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.alert_mark_notification);
                dialog.setCanceledOnTouchOutside(false);
                tvClose = (TextView) dialog.findViewById(R.id.tvClose);
                tvSubmit = (TextView) dialog.findViewById(R.id.tvSubmit);
                lp = new WindowManager.LayoutParams();
                window = dialog.getWindow();
                lp.copyFrom(window.getAttributes());
                lp.width = WindowManager.LayoutParams.MATCH_PARENT;
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                window.setAttributes(lp);
                tvClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                tvSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        readNotificationAPI("0");
                        finish();
                        startActivity(getIntent());
                    }
                });
                dialog.show();
                break;
        }
    }

    private void callNotificationAPI() {


        AppPreferences appPreferences = MyApplication.getInstance().getAppPreferences();

        if (appPreferences.getUserInfo() != null) {

            if (AppGlobal.isNetwork(getApplicationContext())) {
                appPreferences.setNotificationBedgeDec();

                if (presenter == null)
                    presenter = new NotificationPresenterImpl(this);

                HashMap<String, Object> request = new HashMap<>();
                request.put("page", page);

                HashMap<String, Object> auth = new HashMap<>();
                auth.put("id", appPreferences.getUserInfo().userid);
                auth.put("token", appPreferences.getToken());

                HashMap<String, Object> map = new HashMap<>();
                map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_get_notification));
                map.put(getResources().getString(R.string.request), request);
                map.put("auth", auth);

                Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

                presenter.getNotification(requestService, new NotificationResp(), REQUEST_CODE_1);
            } else
                AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.msg_no_internet));

        }

    }


    private void readNotificationAPI(String notificationId) {
        if (AppGlobal.isNetwork(getApplicationContext())) {

            AppPreferences appPreferences = MyApplication.getInstance().getAppPreferences();
            appPreferences.setNotificationBedgeDec();

            if (presenter == null)
                presenter = new NotificationPresenterImpl(this);

            HashMap<String, Object> request = new HashMap<>();
            request.put("notificationId", notificationId);


            HashMap<String, Object> auth = new HashMap<>();
            auth.put("id", appPreferences.getUserInfo().userid);
            auth.put("token", appPreferences.getToken());

            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_update_read_notification));
            map.put(getResources().getString(R.string.request), request);
            map.put("auth", auth);

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

            presenter.readNotification(requestService, new CommonResp(), REQUEST_CODE_2);
        } else
            AppGlobal.showSnakeBar(getApplicationContext(), binding.rlMain, getString(R.string.msg_no_internet));
    }

    private void deleteNotification(NotificationInfo notification) {
        if (AppGlobal.isNetwork(getApplicationContext())) {

            AppPreferences appPreferences = MyApplication.getInstance().getAppPreferences();

            if (presenter == null)
                presenter = new NotificationPresenterImpl(this);

            HashMap<String, Object> request = new HashMap<>();
            request.put("notification_id", notification.id);


            HashMap<String, Object> auth = new HashMap<>();
            auth.put("id", appPreferences.getUserInfo().userid);
            auth.put("token", appPreferences.getToken());

            HashMap<String, Object> map = new HashMap<>();
            map.put(getResources().getString(R.string.service), getResources().getString(R.string.service_delete_notification));
            map.put(getResources().getString(R.string.request), request);
            map.put("auth", auth);

            Call<String> requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);

            presenter.readNotification(requestService, new CommonResp(), REQUEST_CODE_3);
        }
    }


    @Override
    public void onRequestCall() {
        binding.avi.smoothToShow();
    }

    @Override
    public void onResponseSuccess(Object success, int requestCode) {
        if (requestCode == REQUEST_CODE_1 && success instanceof NotificationResp) {
            binding.swipeRefreshLayout.setRefreshing(false);

            notificationResp = (NotificationResp) success;
            AppPreferences appPreferences = MyApplication.getInstance().getAppPreferences();
            appPreferences.setNotificationBedgeInc(Integer.parseInt(notificationResp.count));

            if (notificationResp != null && notificationResp.data != null && notificationResp.data.size() != 0) {
                if (rowsArrayList.size() > 0) {
                    rowsArrayList.addAll(notificationResp.data);
                    notificationAdapter.notifyItemInserted(rowsArrayList.size() - 1);
                } else {
                    rowsArrayList.addAll(notificationResp.data);
                    notificationAdapter = new NotificationAdapter(rowsArrayList, this, isEdit);
                    binding.rvList.setAdapter(notificationAdapter);
                }
                binding.tvNoData.setVisibility(View.GONE);
            } else {
                if (rowsArrayList.size() == 0) {
                    binding.tvNoData.setVisibility(View.VISIBLE);
                    binding.ivEdit.setEnabled(false);
                    binding.ivEdit.setAlpha(50);
                    binding.ivMark.setEnabled(false);
                    binding.ivMark.setAlpha(50);

                } else {
                    page = page - 1;
                }
                end = true;
            }
            isLoading = false;
        }

        if (requestCode == REQUEST_CODE_2 && success instanceof CommonResp) {
            CommonResp commonResp = (CommonResp) success;
            AppPreferences appPreferences = MyApplication.getInstance().getAppPreferences();
            appPreferences.setNotificationBedgeDec();
//            finish();
//            startActivity(getIntent());
        } else if (requestCode == REQUEST_CODE_3) {
            AppPreferences appPreferences = MyApplication.getInstance().getAppPreferences();
            appPreferences.setNotificationBedgeDec();
            finish();
            startActivity(getIntent());
        }
    }

    @Override
    public void onResponseFailure(Object error, int requestCode) {
        if (requestCode == REQUEST_CODE_1) {
            isLoading = false;
        }
    }

    @Override
    public void onRequestCallBack(boolean isAuthenticationFailed) {
        binding.avi.smoothToHide();
        if (isAuthenticationFailed)
            finish();

    }

    private void setAdapter() {
        if (notificationResp != null && notificationResp.data != null && notificationResp.data.size() != 0) {
            notificationAdapter = new NotificationAdapter(notificationResp.data, this, isEdit);
            binding.rvList.setAdapter(notificationAdapter);
            binding.tvNoData.setVisibility(View.GONE);
        } else
            binding.tvNoData.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        rowsArrayList.clear();
        callNotificationAPI();
    }

    private void loadMore() {
        notificationAdapter.notifyItemInserted(rowsArrayList.size() - 1);
    }

}
package com.bungkusit.activity;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.bungkusit.R;
import com.bungkusit.activity.main.MainActivity;
import com.bungkusit.activity.my_profile.MyProfilePresenterImpl;
import com.bungkusit.databinding.ActivityChangePasswordBinding;
import com.bungkusit.model.CommonResp;
import com.bungkusit.utils.AppGlobal;
import com.bungkusit.utils.AppPreferences;
import com.bungkusit.utils.MyApplication;
import com.bungkusit.webservices.interfaces.AppInteractor;
import com.bungkusit.webservices.interfaces.AppInteractorImpl;
import com.bungkusit.webservices.interfaces.OnApiCompleteListener;

import java.util.HashMap;

import retrofit2.Call;

import static com.bungkusit.utils.AppConstant.REQUEST_CODE_3;

public class ChangePasswordActivity extends AppCompatActivity implements OnApiCompleteListener, View.OnClickListener {

    public static final String EXTRA_TITLE = "title";
    private ActivityChangePasswordBinding binding;
    private int oldPassChar = 0, newPassChar = 0, confirmPassChar;
    String oldPassword, newPassword, confirmPassword;

    private AppInteractor appInteractor;

    public static void startActivity(Activity mContext, Bundle mBundle) {
        Intent intent = new Intent(mContext, ChangePasswordActivity.class);
        intent.putExtras(mBundle);

        mContext.startActivity(intent);
        mContext.overridePendingTransition(R.anim.right2middle, R.anim.middle2left);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_password);
        init();
    }

    private void init() {
        this.appInteractor = new AppInteractorImpl();
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        binding.editTextOldPassword.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                oldPassChar = s.length();
                if (oldPassChar > 0 && newPassChar > 0 && confirmPassChar > 0) {
                    enableSubmitButton();
                } else {
                    disableSubmitButton();
                }
            }

        });
        binding.editTextNewPassword.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                newPassChar = s.length();
                if (oldPassChar > 0 && newPassChar > 0 && confirmPassChar > 0) {
                    enableSubmitButton();
                } else {
                    disableSubmitButton();
                }
            }
        });
        binding.editTextConfirmPassword.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                confirmPassChar = s.length();
                if (oldPassChar > 0 && newPassChar > 0 && confirmPassChar > 0) {
                    enableSubmitButton();
                } else {
                    disableSubmitButton();
                }
            }
        });
        binding.tvSubmit.setOnClickListener(this);
        disableSubmitButton();
    }

    public void enableSubmitButton() {
        binding.tvSubmit.getBackground().setAlpha(255);
        binding.tvSubmit.setEnabled(true);
    }

    public void disableSubmitButton() {
        binding.tvSubmit.getBackground().setAlpha(128);
        binding.tvSubmit.setEnabled(false);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tvSubmit) {
            oldPassword = binding.editTextOldPassword.getText().toString().trim();
            newPassword = binding.editTextNewPassword.getText().toString().trim();
            confirmPassword = binding.editTextConfirmPassword.getText().toString().trim();
            if (oldPassword.equals("")) {
                binding.ilError.setError(getString(R.string.error_oldPassword));
            } else if (newPassword.equals("")) {
                binding.ilError.setError(getString(R.string.error_newPassword));
            } else if (confirmPassword.equals("")) {
                binding.ilError.setError(getString(R.string.error_retypePassword));
            } else if (!confirmPassword.equals(newPassword)) {
                binding.ilError.setError(getString(R.string.error_password_not_match));
            } else {
                callUpdateChangePasswordAPI();
            }
        }
    }

    private void callUpdateChangePasswordAPI() {

        AppPreferences appPreferences = MyApplication.getInstance().getAppPreferences();

        if (appPreferences != null && appPreferences.getUserInfo() != null) {

            if (AppGlobal.isNetwork(getApplicationContext())) {

                AppGlobal.hideKeyboard(this);

                HashMap request = new HashMap();

                request.put("oldpassword", oldPassword);
                request.put("newpassword", newPassword);
                request.put("confirmpassword", confirmPassword);

                HashMap auth = new HashMap();
                auth.put("id", appPreferences.getUserInfo().userid);
                auth.put("token", appPreferences.getToken());

                HashMap map = new HashMap();
                map.put(getResources().getString(R.string.service), getResources().getString(R.string.update_change_password));
                map.put(getResources().getString(R.string.request), request);
                map.put("auth", auth);

                Call requestService = MyApplication.getInstance().getAPIListService().callPostAPI(map);
                appInteractor.doCallAPI(this, requestService, requestService, REQUEST_CODE_3, (OnApiCompleteListener) this);
//                presenter.changePassword(requestService, new CommonResp(), REQUEST_CODE_3);
            } else
                AppGlobal.showSnakeBar(getApplicationContext(), binding.llMain, getString(R.string.msg_no_internet));


        }


    }

    @Override
    public void onResponseFailure(Object error, int requestCode) {
        if (requestCode == REQUEST_CODE_3) {
            binding.ilError.setError(getString(R.string.error_oldPassword_invalid));
//            System.out.println(error);
//            Toast.makeText(getApplicationContext(), ((CommonResp) error).message, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResponseSuccess(Object success, int requestCode) {
        if (requestCode == REQUEST_CODE_3) {
            Bundle bundle = new Bundle();
            bundle.putString(MainActivity.EXTRA_ACTIVITY_ACTION, "editPasswordSuccess");
            MainActivity.startActivity(this, bundle);
        }
    }

    @Override
    public void onRequestCall() {
        binding.avi.smoothToShow();
    }

    @Override
    public void onRequestCallBack(boolean isAuthenticationFailed) {
        binding.avi.smoothToHide();
    }
}

package com.bungkusit.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bungkusit.BR;
import com.bungkusit.R;
import com.bungkusit.databinding.ListNotificationBinding;
import com.bungkusit.model.NotificationInfo;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.BindingHolder> {


    private List<NotificationInfo> notificationInfos;
    private View.OnClickListener onClickListener;
    private Boolean isEdit;

    public NotificationAdapter(List<NotificationInfo> notificationInfos, View.OnClickListener onClickListener, Boolean isEdit) {
        this.notificationInfos = notificationInfos;
        this.onClickListener = onClickListener;
        this.isEdit = isEdit;
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_notification, parent, false);

        BindingHolder holder = new BindingHolder(v);

        return holder;

    }

    @Override
    public void onBindViewHolder(NotificationAdapter.BindingHolder holder, int position) {
        NotificationInfo notificationInfo = notificationInfos.get(position);

        holder.getBinding().setVariable(BR.notificationInfo, notificationInfo);
        holder.getBinding().setVariable(BR.isEdit, isEdit);

        holder.getBinding().executePendingBindings();

        holder.getBinding().llMain.setTag(position);
        holder.getBinding().ivNotificationDelete.setTag(position);
        holder.getBinding().llMain.setOnClickListener(onClickListener);
        holder.getBinding().ivNotificationDelete.setOnClickListener(onClickListener);

    }

    public class BindingHolder extends RecyclerView.ViewHolder {

        private ListNotificationBinding binding;

        public BindingHolder(View rowView) {
            super(rowView);
            binding = DataBindingUtil.bind(rowView);
        }

        public ListNotificationBinding getBinding() {
            return binding;
        }
    }

    @Override
    public int getItemCount() {
        return notificationInfos.size();
    }

    public NotificationInfo getItem(int position){
        return  notificationInfos.get(position);
    }
}

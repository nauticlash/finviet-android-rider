package com.bungkusit.adapter;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bungkusit.BR;
import com.bungkusit.R;
import com.bungkusit.databinding.ListOrderBinding;
import com.bungkusit.databinding.ListOrderItemBinding;
import com.bungkusit.model.OrderItem;
import com.bungkusit.utils.AppPreferences;
import com.bungkusit.utils.MyApplication;

import java.util.ArrayList;

public class OrderItemAdapter extends RecyclerView.Adapter<OrderItemAdapter.BindingHolder> {

    private Activity activity;
    private ArrayList<OrderItem> orderItems;
    private View.OnClickListener onClickListener;
    private AppPreferences appPreferences;

    public OrderItemAdapter(ArrayList<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_order_item, parent, false);

        BindingHolder holder = new BindingHolder(v);

        return holder;

    }

    @Override
    public void onBindViewHolder(OrderItemAdapter.BindingHolder holder, int position) {
        OrderItem orderItem = orderItems.get(position);

        appPreferences = MyApplication.getInstance().getAppPreferences();

        holder.getBinding().setVariable(BR.orderItem, orderItem);

        holder.getBinding().executePendingBindings();
    }

    public class BindingHolder extends RecyclerView.ViewHolder {

        private ListOrderItemBinding binding;

        public BindingHolder(View rowView) {
            super(rowView);
            binding = DataBindingUtil.bind(rowView);
        }

        public ListOrderItemBinding getBinding() {
            return binding;
        }
    }

    @Override
    public int getItemCount() {
        return orderItems.size();
    }

    public OrderItem getItem(int position) {
        return orderItems.get(position);
    }

}

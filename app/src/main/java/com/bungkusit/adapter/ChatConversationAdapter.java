package com.bungkusit.adapter;

import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bungkusit.R;
import com.bungkusit.BR;
import com.bungkusit.databinding.ListChatDetailsBinding;
import com.bungkusit.model.ChatConversationInfo;
import com.bungkusit.utils.AppPreferences;
import com.bungkusit.utils.MyApplication;

import java.util.ArrayList;
import java.util.List;


public class ChatConversationAdapter extends RecyclerView.Adapter<ChatConversationAdapter.BindingHolder> {


    private List<ChatConversationInfo> chatConversationInfo = new ArrayList<>();
    private View.OnClickListener onClickListener;

    public ChatConversationAdapter(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_chat_details, parent, false);

        BindingHolder holder = new BindingHolder(v);


        return holder;

    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        ChatConversationInfo chatInfo1 = chatConversationInfo.get(position);
        AppPreferences appPreferences = MyApplication.getInstance().getAppPreferences();


        String userid = appPreferences.getUserInfo().userid;

        if (userid.equals(chatInfo1.fromId)) {

            holder.getBinding().llfrom.setVisibility(View.VISIBLE);
            holder.getBinding().llto.setVisibility(View.GONE);

        } else if (userid.equals(chatInfo1.toId)) {
            holder.getBinding().llfrom.setVisibility(View.GONE);
            holder.getBinding().llto.setVisibility(View.VISIBLE);
        }

        holder.getBinding().setVariable(BR.chatConversationInfo, chatInfo1);
        holder.getBinding().executePendingBindings();
    }


    public class BindingHolder extends RecyclerView.ViewHolder {

        private ListChatDetailsBinding binding;

        public BindingHolder(View rowView) {
            super(rowView);
            binding = DataBindingUtil.bind(rowView);
        }

        public ListChatDetailsBinding getBinding() {
            return binding;
        }
    }

    @Override
    public int getItemCount() {
        return chatConversationInfo.size();
    }

    public ChatConversationInfo getItem(int pos) {
        return chatConversationInfo.get(pos);
    }

    public void addAll(List<ChatConversationInfo> data) {
        if (data != null) {
            chatConversationInfo.clear();
            chatConversationInfo.addAll(data);
            notifyDataSetChanged();

        }
    }

    public void appendAll(List<ChatConversationInfo> data) {
        if (data != null) {
            chatConversationInfo.addAll(data);
            notifyDataSetChanged();

        }
    }

    public void add(ChatConversationInfo data) {
        if (data != null) {
            chatConversationInfo.add(data);
            notifyDataSetChanged();
        }
    }

}

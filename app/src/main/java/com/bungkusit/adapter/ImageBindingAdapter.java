package com.bungkusit.adapter;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.Keep;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bungkusit.R;

@Keep
public class ImageBindingAdapter {

    @BindingAdapter(value={"imageUrl", "placeholder"}, requireAll=false)
    public static void loadImage(ImageView imageView, String url, Drawable placeholder) {
//        if (!url.equals("")) {
        if (url.startsWith("http")) {
            Glide
                    .with(imageView.getContext())
                    .load(url)
//                    .placeholder(R.drawable.default_tig_pic)
                    .error(R.mipmap.ic_launcher)
                    .into(imageView);
        }else{
            imageView.setImageDrawable(placeholder);
        }
//        }
    }


    @BindingAdapter("android:src")
    public static void setImageUri(ImageView view, String imageUri) {
        if (imageUri == null) {
            view.setImageURI(null);
        } else {
            view.setImageURI(Uri.parse(imageUri));
        }
    }

    @BindingAdapter("android:src")
    public static void setImageUri(ImageView view, Uri imageUri) {
        view.setImageURI(imageUri);
    }

    @BindingAdapter("android:src")
    public static void setImageDrawable(ImageView view, Drawable drawable) {
        view.setImageDrawable(drawable);
    }

    @BindingAdapter("android:src")
    public static void setImageResource(ImageView imageView, int resource){
        imageView.setImageResource(resource);
    }
}
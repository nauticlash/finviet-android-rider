package com.bungkusit.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bungkusit.BR;
import com.bungkusit.R;
import com.bungkusit.databinding.ListChatNameDetailsBinding;
import com.bungkusit.model.ChatInfo;

import java.util.ArrayList;
import java.util.List;


public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.BindingHolder> {


    private List<ChatInfo> chatInfo;
    private View.OnClickListener onClickListener;

    public ChatAdapter(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
        chatInfo = new ArrayList<>();
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_chat_name_details, parent, false);

        BindingHolder holder = new BindingHolder(v);

        return holder;

    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        ChatInfo chatInfo1 = chatInfo.get(position);

        holder.getBinding().setChatInfo(chatInfo1);

        holder.getBinding().executePendingBindings();

        holder.getBinding().llMain.setTag(position);
        holder.getBinding().llMain.setOnClickListener(onClickListener);

    }

    public class BindingHolder extends RecyclerView.ViewHolder {

        private ListChatNameDetailsBinding binding;

        public BindingHolder(View rowView) {
            super(rowView);
            binding = DataBindingUtil.bind(rowView);
        }

        public ListChatNameDetailsBinding getBinding() {
            return binding;
        }
    }

    @Override
    public int getItemCount() {
        return chatInfo.size();
    }

    public ChatInfo getItem(int pos) {
        return chatInfo.get(pos);
    }


    public void update(int pos, ChatInfo data) {
        if (data != null) {
            chatInfo.set(pos, data);
            notifyDataSetChanged();
        }
    }

    public void appendAll(List<ChatInfo> data) {
        if (data != null) {
            chatInfo.addAll(data);
            notifyDataSetChanged();
        }
    }
}

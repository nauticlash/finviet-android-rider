package com.bungkusit.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bungkusit.BR;
import com.bungkusit.R;
import com.bungkusit.databinding.ListOrderBinding;
import com.bungkusit.model.NewOrderInfo;
import com.bungkusit.model.OrderItem;
import com.bungkusit.utils.AppPreferences;
import com.bungkusit.utils.MyApplication;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NewOrderAdapter extends RecyclerView.Adapter<NewOrderAdapter.BindingHolder> {


    private List<NewOrderInfo> newOrderInfos;
    private View.OnClickListener onClickListener;
    private String type;
    private AppPreferences appPreferences;
    private RecyclerView.RecycledViewPool viewPool = new RecyclerView.RecycledViewPool();
    private Context context;

    public NewOrderAdapter(List<NewOrderInfo> newOrderInfos, View.OnClickListener onClickListener, String type) {
        this.newOrderInfos = newOrderInfos;
        this.onClickListener = onClickListener;
        this.type = type;
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int i) {
        context = parent.getContext();
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_order, parent, false);

        BindingHolder holder = new BindingHolder(v);

        return holder;

    }

    @Override
    public void onBindViewHolder(NewOrderAdapter.BindingHolder holder, int position) {
        NewOrderInfo newOrderInfo = newOrderInfos.get(position);

        appPreferences = MyApplication.getInstance().getAppPreferences();

        holder.getBinding().setVariable(BR.orderInfo, newOrderInfo);
        holder.getBinding().setVariable(BR.type, type);

        holder.getBinding().executePendingBindings();

        holder.getBinding().tvViewDetail.setTag(position);
        holder.getBinding().tvPlaceBid.setTag(position);
        holder.getBinding().tvStartOrder.setTag(position);
        holder.getBinding().tvRemoveOrderDetails.setTag(position);
        if (!appPreferences.isOnline()) {
            holder.getBinding().tvPlaceBid.setVisibility(View.GONE);
            holder.getBinding().tvStartOrder.setVisibility(View.GONE);
        }


        holder.getBinding().tvPlaceBid.setOnClickListener(onClickListener);
        holder.getBinding().tvViewDetail.setOnClickListener(onClickListener);
        holder.getBinding().tvRemoveOrderDetails.setOnClickListener(onClickListener);
        holder.getBinding().tvStartOrder.setOnClickListener(onClickListener);
        if (newOrderInfo.ispickup.equals("0") && !newOrderInfo.riderId.equals("0") && newOrderInfo.riderstatus.equalsIgnoreCase("Pending")) {
            holder.getBinding().tvStartOrder.setVisibility(View.VISIBLE);
        } else {
            holder.getBinding().tvStartOrder.setVisibility(View.GONE);
        }
        if (!newOrderInfo.ispickup.equals("0") && !newOrderInfo.riderId.equals("0")) {
            holder.getBinding().tvViewDetail.setText(R.string.view_details);
        }
        if (newOrderInfo.order_items != null && !newOrderInfo.order_items.equalsIgnoreCase("")) {
//            holder.getBinding().setLayoutManager(new LinearLayoutManager(this));
//            LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            holder.getBinding().rvOrderItems.setLayoutManager(new LinearLayoutManager(context));
//            childLayoutManager = LinearLayoutManager(holder.recyclerView.context, LinearLayout.HORIZONTAL, false);
            try {
                JSONArray orderItems = new JSONArray(newOrderInfo.order_items);
                ArrayList<OrderItem> items = new ArrayList<OrderItem>();
                for (int i = 0; i < orderItems.length(); i++) {
                    JSONObject jo = (JSONObject) orderItems.get(i);
                    OrderItem item = new OrderItem();
                    item.name = jo.getString("name");
                    item.amount = jo.getInt("amount");
                    item.quantity = jo.getInt("quantity");
                    item.position = i + 1;
                    items.add(item);
                }
                OrderItemAdapter mAdapter = new OrderItemAdapter(items);
                holder.getBinding().rvOrderItems.setAdapter(mAdapter);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        holder.getBinding().tvDescription.setOnClickListener(onClickListener);
        if (holder.getBinding().tvDescription.getText().length() > 30) {
            holder.getBinding().tvDescription.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_expanse_small, 0);
            holder.getBinding().tvDescription.setSingleLine(true);
        } else {
            holder.getBinding().tvDescription.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }

    public class BindingHolder extends RecyclerView.ViewHolder {

        private ListOrderBinding binding;

        public BindingHolder(View rowView) {
            super(rowView);
            binding = DataBindingUtil.bind(rowView);
        }

        public ListOrderBinding getBinding() {
            return binding;
        }
    }

    @Override
    public int getItemCount() {
        return newOrderInfos.size();
    }

    public NewOrderInfo getItem(int position) {
        return newOrderInfos.get(position);
    }

}

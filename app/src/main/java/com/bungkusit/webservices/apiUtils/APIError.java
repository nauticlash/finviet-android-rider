package com.bungkusit.webservices.apiUtils;

public class APIError {

    private String message;

    public APIError() {
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String message() {
        return message;
    }
}
package com.bungkusit.webservices.api;

import com.bungkusit.activity.main.MainActivity;
import com.bungkusit.utils.AppConstant;
import com.bungkusit.utils.Debug;
import com.bungkusit.webservices.apiUtils.APIError;
import com.bungkusit.webservices.apiUtils.ErrorUtils;
import com.bungkusit.webservices.apiUtils.HttpLoggingInterceptor;
import com.bungkusit.webservices.apiUtils.ToStringConverterFactory;
import com.bungkusit.webservices.interfaces.APIListService;
import com.bungkusit.webservices.interfaces.OnApiCompleteListener;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.bungkusit.utils.AppConstant.TIME_OUT;


public class RestClient {

    public static retrofit2.Retrofit retrofit;
    private static APIListService service;

    public static String error = "Đường truyền có vấn đề. Vui lòng thử lại!";

    public RestClient( ) {

        if (retrofit == null) {

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.connectTimeout(TIME_OUT, TimeUnit.SECONDS);
            httpClient.readTimeout(TIME_OUT, TimeUnit.SECONDS);
            httpClient.writeTimeout(TIME_OUT, TimeUnit.SECONDS);

            if (Debug.DEBUG) {
                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);

                httpClient.addInterceptor(logging)
                        .addInterceptor(new Interceptor() {
                            @Override
                            public okhttp3.Response intercept(Chain chain) throws IOException {
                                Request original = chain.request();

                                Request request = original.newBuilder()
                                        .method(original.method(), original.body())
                                        .build();

                                return chain.proceed(request);
                            }
                        });
            }

            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(AppConstant.BASE_URL)
//                    .baseUrl(URL)
                    .client(httpClient.build())
                    .addConverterFactory(new ToStringConverterFactory())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            service = retrofit.create(APIListService.class);
        }
    }

    public APIListService getAPIListService() {
        service = retrofit.create(APIListService.class);
        return service;
    }

    public void callAPI(Call<String> objectCall, final Object clz, final int requestCode, final OnApiCompleteListener onLoadCallback) {
        if (service != null) {

            objectCall.enqueue(new Callback<String>() {

                @Override
                public void onResponse(Call<String> call, Response<String> response) {

                    if (response.code() == 400) {

                        APIError error = ErrorUtils.parseError(response);
                        onLoadCallback.onRequestCallBack(true);
                        onLoadCallback.onResponseFailure(error.message(), requestCode);

                    } else if (response.code() == 200) {
                        onLoadCallback.onRequestCallBack(false);

                        if (response.isSuccessful()) {


                            try {
                                Object clz1 = new Gson().fromJson(response.body(), clz.getClass());

                                if (response.body().contains("\"success\":1"))
                                    onLoadCallback.onResponseSuccess(clz1, requestCode);
                                else
                                    onLoadCallback.onResponseFailure(clz1, requestCode);

                            } catch (Exception e) {
                               onLoadCallback.onResponseFailure(e.getMessage(), requestCode);
                            }
                        } else {
                            APIError error = ErrorUtils.parseError(response);
                            onLoadCallback.onResponseFailure(error.message(), requestCode);
                        }
                    } else {
                        onLoadCallback.onRequestCallBack(false);
                        APIError error = ErrorUtils.parseError(response);
                        onLoadCallback.onResponseFailure(error.message(), requestCode);
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable throwable) {
                    onLoadCallback.onRequestCallBack(false);

                    if (!call.isCanceled()) {
                        call.cancel();
                        if (throwable.getMessage() != null && throwable.getMessage().length() != 0)
                            onLoadCallback.onResponseFailure(throwable.getMessage(), requestCode);
                        else
                            onLoadCallback.onResponseFailure(error, requestCode);
                    }
                }
            });
        }
    }
}

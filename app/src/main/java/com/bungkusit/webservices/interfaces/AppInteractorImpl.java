package com.bungkusit.webservices.interfaces;

import android.content.Context;

import com.bungkusit.utils.MyApplication;

import retrofit2.Call;

public class AppInteractorImpl implements AppInteractor {

    @Override
    public void doCallAPI(Context mContext, Call<String> requestService, final Object respObject, final int requestCode, OnApiCompleteListener onApiCompleteListener) {

        onApiCompleteListener.onRequestCall();

        MyApplication.getInstance().getRestClient().callAPI(requestService
                , respObject, requestCode, onApiCompleteListener);

    }
}
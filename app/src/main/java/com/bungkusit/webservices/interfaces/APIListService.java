package com.bungkusit.webservices.interfaces;

import com.bungkusit.model.UploadImageResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface APIListService {

    @POST("webService/service")
    Call<String> callPostAPI(
            @Body Map<String, Object> method);

    @Multipart
    @POST("upload/uploadcourierImage")
    Call<String> callUploadOfferAPI(
            @Part MultipartBody.Part[] data);

    @GET
    Call<String> callMapAPI(
            @Url String url);

    @Multipart
    @POST("upload/uploadprofilepic")
    Call<UploadImageResponse> UploadProfilepic(@Part MultipartBody.Part image);


    @Multipart
    @POST("upload/uploadprofilepic")
    Call<String> callUploadProfileAPI(
            @Part MultipartBody.Part data);

    @Multipart
    @POST("upload/uploadsubscriptionImage")
    Call<UploadImageResponse> UploadSubScriptionImage(@Part MultipartBody.Part image);

    @Multipart
    @POST("upload/uploadmessageattachment")
    Call<String> UploadMessageAttachment(@Part MultipartBody.Part file, @Part("file") RequestBody name);

    @POST("rider/{service}")
    Call<String> callRiderAPI(
            @Body Map<String, Object> method, @Path(value = "service", encoded = true) String service);
}
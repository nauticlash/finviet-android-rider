package com.bungkusit.webservices.interfaces;

public interface OnApiCompleteListener {

    void onRequestCall();

    void onResponseSuccess(Object success, int requestCode);

    void onResponseFailure(Object error, int requestCode);

    void onRequestCallBack(boolean isAuthenticationFailed);

}
package com.bungkusit.webservices.interfaces;

import android.content.Context;

import retrofit2.Call;


public interface AppInteractor {
    void doCallAPI(Context mContext, Call<String> requestService, Object responseObject, int listener, OnApiCompleteListener
            onApiCompleteListener);
}
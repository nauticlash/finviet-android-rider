# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/sudhir/Android/Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile





#
#-keep class * extends java.util.ListResourceBundle {
#    protected Object[][] getContents();
#}
#
##-----------For RecyclerView----------------
#-keep public class * extends android.support.v7.widget.RecyclerView$LayoutManager {
#    public <init>(...);
#}
#
##-----------For CardView----------------
#-keep class android.support.v7.widget.RoundRectDrawable { *; }
#
##-----------For Gson----------------
## Gson specific classes
#-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }
#
## Application classes that will be serialized/deserialized over Gson
#-keep class com.google.gson.examples.android.model.** { *; }
#
## Prevent proguard from stripping interface information from TypeAdapterFactory,
## JsonSerializer, JsonDeserializer instances (so they can be used in @JsonAdapter)
#-keep class * implements com.google.gson.TypeAdapterFactory
#-keep class * implements com.google.gson.JsonSerializer
#-keep class * implements com.google.gson.JsonDeserializer
#
##-----------For rey----------------
#-keep class com.rey.material.** { *; }
#-keep class com.rey.material.$ { *; }
#
#
## for DexGuard only
##-keepresourcexmlelements manifest/application/meta-data@value=GlideModule
#
#
##-----------For Retrofit----------
#-dontnote retrofit2.Platform
#-dontwarn retrofit2.Platform$Java8
#-keepattributes Signature
#-keepattributes Exceptions
#
#-dontwarn javax.annotation.**
#
##-keepclasseswithmembers class * {
##    @retrofit2.http.* <methods>;
##}
##-keep class com.example.app.json.** { *; }
##-keep class com.example.app.json.SpecificClass { *; }
#
##-----------For okio----------------
#-dontwarn com.squareup.**
#-dontwarn okio.**
#
#-dontwarn java.nio.file.Files
#-dontwarn java.nio.file.Path
#-dontwarn java.nio.file.OpenOption
#-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
#
##-----------For okhttp3----------------
##-keepattributes Signature
#-keepattributes Annotation
#-keep class okhttp3.** { *; }
#-keep interface okhttp3.** { *; }
#-dontwarn okhttp3.**
#
##-----------For otto----------------
#-keepclassmembers class ** {
#    @com.squareup.otto.Subscribe public *;
#    @com.squareup.otto.Produce public *;
#}
#
##---------Glide-----------
#-keep public class * implements com.bumptech.glide.module.GlideModule
#-keep public class * extends com.bumptech.glide.GeneratedAppGlideModule
#-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
#    **[] $VALUES;
#    public *;
#}
#
#
##---------Facebook-----------
#-keepclassmembers class * implements java.io.Serializable {
#    private static final java.io.ObjectStreamField[] serialPersistentFields;
#    private void writeObject(java.io.ObjectOutputStream);
#    private void readObject(java.io.ObjectInputStream);
#    java.lang.Object writeReplace();
#    java.lang.Object readResolve();
#}
#
#-keepnames class com.facebook.FacebookActivity
#-keepnames class com.facebook.CustomTabActivity
#
#-keep class com.facebook.all.All
#
#-keep public class com.android.vending.billing.IInAppBillingService {
#    public static com.android.vending.billing.IInAppBillingService asInterface(android.os.IBinder);
#    public android.os.Bundle getSkuDetails(int, java.lang.String, java.lang.String, android.os.Bundle);
#}
#
##---------Firebase-----------
#-keepattributes EnclosingMethod
#-keepattributes InnerClasses
#
#-keep class com.bungkusit.views.** { *; }
#
##---------prettytime-----------
#-keep class org.ocpsoft.prettytime.i18n.**